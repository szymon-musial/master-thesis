using System.Text;
using CloudNative.CloudEvents;
using Confluent.Kafka;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace Controllers;

public class CloudEventsController : ControllerBase
{
    private readonly ILogger _logger;
    private readonly IKafkaProducer<string> _kafkaProducer;
    public CloudEventsController(ILogger<CloudEventsController> logger, IKafkaProducer<string> kafkaProducer)
    {
        _logger = logger;
        _kafkaProducer = kafkaProducer;
    }


    [HttpPost("/")]
    public async Task<ActionResult> CeEventEndpointAsync([FromBody] CloudEvent cloudEvent)
    {
        _logger.LogInformation($"Recived CE {cloudEvent.Type} {cloudEvent.Id}");

        try
        {
            Headers kafkaHeaders = new();
            foreach (var header in Request.Headers.Where(h => !h.Key.StartsWith("ce", StringComparison.OrdinalIgnoreCase)))
            {
                string value = "";
                if (!string.IsNullOrEmpty(header.Value))
                {
                    value = header.Value!;
                }
                kafkaHeaders.Add(header.Key, Encoding.UTF8.GetBytes(value));
            }

            // nagłówki z przefixem ce muszą być:
            // oddzielone `_` a nie `-`
            // z małych liter
            // todo: jak z traceparent ?

            foreach (var header in Request.Headers.Where(h => h.Key.StartsWith("ce", StringComparison.OrdinalIgnoreCase)))
            {
                string value = "";
                if (!string.IsNullOrEmpty(header.Value))
                {
                    value = header.Value!;
                }
                kafkaHeaders.Add(header.Key.ToLower().Replace("-", "_"), Encoding.UTF8.GetBytes(value));
            }

            var msg = new Message<Null, string>
            {
                Value = cloudEvent.Data?.ToString() ?? "empty",
                Headers = kafkaHeaders,
            };

            var dr = await _kafkaProducer.ProduceAsync(msg);
            _logger.LogInformation($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");
        }
        catch (ProduceException<Null, string> e)
        {
            _logger.LogError($"Delivery failed: {e.Error.Reason}");
        }

        return Ok();
    }
}
