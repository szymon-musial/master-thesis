﻿using CloudEvents;
using CloudNative.CloudEvents.SystemTextJson;
using Microsoft.AspNetCore.HttpLogging;
using Configuration;

var builder = WebApplication.CreateSlimBuilder();

builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix);

var headersToLog = new List<string> { "Ce-Id", "Ce-Source", "Ce-Specversion", "Ce-Time", "Ce-Type", "X-B3-Parentspanid", "X-B3-Sampled", "X-B3-Spanid", "X-B3-Traceid" };

builder.Services.AddHttpLogging(o =>
{
    o.LoggingFields = HttpLoggingFields.All;
    o.CombineLogs = true;
    foreach (var header in headersToLog)
    {
        o.RequestHeaders.Add(header);
    }
});

var appConfiguration = builder.Configuration.Get<AppConfiguration>()
    ?? throw new ArgumentNullException(nameof(AppConfiguration));
builder.Services.AddSingleton(appConfiguration);

//builder.Services.AddHostedService<BasicConsumer>();
//builder.Services.AddHostedService<BasicProducer>();
builder.Services.AddTransient<IKafkaProducer<string>, BasicProducer>();

builder.Services.AddControllers(opts => opts.InputFormatters.Insert(0, new CloudEventJsonInputFormatter(new JsonEventFormatter())));

var app = builder.Build();

app.Use(async (context, next) =>
{
    // POST request comes from kn kafka brokers are without content type
    // https://github.com/knative/eventing-contrib/issues/1206
    if(string.IsNullOrEmpty(context.Request.Headers.ContentType))
    {
        context.Request.Headers.ContentType = "application/json";
    }
    await next(context);
});

app.UseHttpLogging();

app.MapControllers();

app.Run();
