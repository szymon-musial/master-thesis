using System.Text;
using Confluent.Kafka;

public sealed record KnativeKafkaCloudEvents()
{
    public readonly Headers HeadersCopy = new()
    {
        { "ce_type",  Encoding.UTF8.GetBytes("dev.knative.sources.ping") },
        { "ce_source",  Encoding.UTF8.GetBytes("/apis/v1/namespaces/default/pingsources/source") },
        { "ce_specversion",  Encoding.UTF8.GetBytes("1.0") },
        { "ce_time",  Encoding.UTF8.GetBytes("2024-05-05T22:00:00.364705363Z") },
        { "ce_id",  Encoding.UTF8.GetBytes("644bca92-80a6-455a-b297-a5eebe4405ae") },
        { "traceparent",  Encoding.UTF8.GetBytes("00-5d6bfdcc17b447dfa04393f2ce74a082-d8b5b411403c2601-00") },
    };

    public Headers Headers => _headers;

    private readonly Headers _headers = new()
    {
        { "ce_type",  Encoding.UTF8.GetBytes("dev.knative.sources.ping") },
        { "ce_source",  Encoding.UTF8.GetBytes("/apis/v1/namespaces/default/pingsources/source") },
        { "ce_specversion",  Encoding.UTF8.GetBytes("1.0") },
        { "ce_time",  Encoding.UTF8.GetBytes(DateTime.UtcNow.ToString("o")) },

        { "ce_id",  Encoding.UTF8.GetBytes("644bca92-80a6-455a-b297-a5eebe4405ae") },
        
        //{ "traceparent",  Encoding.UTF8.GetBytes("00-5d6bfdcc17b447dfa04393f2ce74a082-d8b5b411403c2601-00") },
    };

    public Dictionary<string, string> Extensions
    {
        set
        {
            foreach (var i in value)
            {
                _headers.Add($"ce_{i.Key}", Encoding.UTF8.GetBytes(DateTime.UtcNow.ToString(i.Value)));
            }
        }
    }
}
