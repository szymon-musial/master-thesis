resource "libvirt_volume" "volume_worker" {
  count   = var.worker_node_count
  name    = format("%s%s_worker_%d.qcow2", var.base_name, var.worker_suffix_name, count.index)
  base_volume_id = libvirt_volume.worker_os_base_image[count.index].id
  pool    =  var.libvirt_pool_name
  format  = "qcow2" 
  size    = var.disk_size_gb * 1024 * 1024 * 1024
}

data "template_file" "worker_user_data" {
  template = file("${path.module}/cloudInit/cloud_init.yml")
  vars = {
    new_user = "${var.worker_node_user}"
    host_ssh_pub_key = file(var.ssh_host_key_path)
  }
}

data "template_file" "worker_network_config" {
  count = var.worker_node_count
  template = file("${path.module}/cloudInit/network_config.yml")
  vars = {
    host_address_with_cidr = format("%s/%s", cidrhost(local.network_address_with_cidr, var.worker_begin_octet + count.index), var.network_address_cidr)
    gateway_address = cidrhost(local.network_address_with_cidr, 1)
  }
}

resource "libvirt_cloudinit_disk" "cloudinit_worker" {
  count = var.worker_node_count
  name           = format("cloudinit_worker%d.iso", count.index)
  user_data      = data.template_file.worker_user_data.rendered
  network_config = data.template_file.worker_network_config[count.index].rendered
  pool =  var.libvirt_pool_name
}

resource "libvirt_domain" "instance_worker" {

  count = var.worker_node_count
  name  = format("%s%s%d", var.base_name, var.worker_suffix_name, count.index)

  vcpu   = var.worker_node_cpus
  memory = var.worker_node_memory_gbs * 1000 # Mebibyte

  cloudinit = libvirt_cloudinit_disk.cloudinit_worker[count.index].id

  cpu {
    mode = "host-passthrough"
  }  

  network_interface {
    network_name = libvirt_network.network.name
    # addresses = will be overwritten by cloud init
    hostname     = format("%s%s%d", var.base_name, var.worker_suffix_name, count.index)
  }

  disk {
    volume_id = libvirt_volume.volume_worker[count.index].id
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport = true
    # remote cockpit
    listen_address = "0.0.0.0"
  }

  video {
    type = "virtio"
  }
}