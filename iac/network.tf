resource "libvirt_network" "network" {
  name  = format("%s_net", var.base_name)
  mode = "nat"
  autostart = true
  addresses = [ local.network_address_with_cidr ]
}
