variable "instance_image" {
  description = "Ubuntu cloud image"
  default     = "http://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img"
}

variable "libvirt_pool_name" {
  description = "Libvirt pool name"
  default     = "thesis"
}

variable "base_name" {
  default = "szymon"
  description = "Base name used for construct for ex. node name"
}

# Master node config

variable "master_node_count" {
  default = 1
  type = number
  description = "Master node count"
}

variable "master_suffix_name" {
  default = "masternode"
  description = "Master node name appened to base name"
}

variable "master_node_cpus" {
  default = 2
  type = number
  description = "Master node logic cpu count (ocpu)"
}

variable "master_node_memory_gbs" {
  default = 12
  type = number
  description = "Master node memory in GB"
}

variable "master_begin_octet" {
  default = 10
  type = number
  description = "Host address + this var"
}

variable "master_node_user" {
  default = "ubuntu"
  description = "Master node user"
}

# Worker node config

variable "worker_node_count" {
  default = 1
  type = number
  description = "Worker node count"
}

variable "worker_suffix_name" {
  default = "node"
  description = "Worker node name appened to base name"
}

variable "worker_node_cpus" {
  default = 2
  type = number
  description = "Worker node logic cpu count (ocpu)"
}

variable "worker_node_memory_gbs" {
  default = 12
  type = number
  description = "Worker node memory in GB"
}

variable "worker_begin_octet" {
  default = 20
  type = number
  description = "Host address + this var"
}

variable "worker_node_user" {
  default = "ubuntu"
  description = "Worker node user"
}

# Network

variable "network_address" {
  default = "10.250.0.0"
  description = "CIDR block for new created subnet"
}

variable "network_address_cidr" {
  default = 24
  type = number
  description = "CIDR block for new created subnet"
}

locals {
  network_address_with_cidr = format("%s/%s", var.network_address, var.network_address_cidr)
}

variable "ssh_host_key_path" {
  description = "Host SSH public adding to all instances to remote connect"
  default = "~/.ssh/id_rsa.pub"
}

# disc
variable "disk_size_gb" {
  default = 40
  type = number
  description = "Disk size in [Gb]"
}
