resource "libvirt_volume" "master_os_base_image" {
  count   = var.master_node_count
  name  = format("%s_master_base_image_%d", var.base_name, count.index)

  pool =  var.libvirt_pool_name
  source = var.instance_image
}

resource "libvirt_volume" "worker_os_base_image" {
  count   = var.worker_node_count
  name  = format("%s_worker_base_image_%d", var.base_name, count.index)

  pool =  var.libvirt_pool_name
  source = var.instance_image
}