resource "libvirt_volume" "volume_master" {
  count   = var.master_node_count
  name    = format("%s%s_master_%d.qcow2", var.base_name, var.master_suffix_name, count.index)
  base_volume_id = libvirt_volume.master_os_base_image[count.index].id
  pool    =  var.libvirt_pool_name
  format  = "qcow2" 
  size    = var.disk_size_gb * 1024 * 1024 * 1024
}

data "template_file" "master_user_data" {
  template = file("${path.module}/cloudInit/cloud_init.yml")
  vars = {
    new_user = "${var.master_node_user}"
    host_ssh_pub_key = file(var.ssh_host_key_path)
  }
}

data "template_file" "master_network_config" {
  count = var.master_node_count
  template = file("${path.module}/cloudInit/network_config.yml")
  vars = {
    host_address_with_cidr = format("%s/%s", cidrhost(local.network_address_with_cidr, var.master_begin_octet + count.index), var.network_address_cidr)
    gateway_address = cidrhost(local.network_address_with_cidr, 1)
  }
}

resource "libvirt_cloudinit_disk" "cloudinit_master" {
  count = var.master_node_count
  name           = format("cloudinit_master%d.iso", count.index)
  user_data      = data.template_file.master_user_data.rendered
  network_config = data.template_file.master_network_config[count.index].rendered
  pool =  var.libvirt_pool_name
}

resource "libvirt_domain" "instance_master" {

  count = var.master_node_count
  name  = format("%s%s%d", var.base_name, var.master_suffix_name, count.index)

  vcpu   = var.master_node_cpus
  memory = var.master_node_memory_gbs * 1000 # Mebibyte

  cloudinit = libvirt_cloudinit_disk.cloudinit_master[count.index].id

  cpu {
    mode = "host-passthrough"
  }

  network_interface {
    network_name = libvirt_network.network.name
    # addresses = will be overwritten by cloud init
    hostname     = format("%s%s%d", var.base_name, var.master_suffix_name, count.index)
  }

  disk {
    volume_id = libvirt_volume.volume_master[count.index].id
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport = true
    # remote cockpit
    listen_address = "0.0.0.0"
  }

  video {
    type = "virtio"
  }
}