# Installation

```sh
kubectl apply -f https://raw.githubusercontent.com/istio/istio/1.18.2/samples/httpbin/httpbin.yaml
```
# Ingress

```sh
kubectl apply -f httpbin-istio-api.yaml
```

# Query

```sh
time curl -o /dev/null http://httpbin.127.0.0.1.sslip.io/status/10
curl http://httpbin.127.0.0.1.sslip.io/status/418
```