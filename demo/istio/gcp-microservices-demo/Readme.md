# Install
```sh
kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/v0.8.0/release/kubernetes-manifests.yaml
```

# istio ingress

```sh
kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/microservices-demo/v0.8.0/istio-manifests/frontend-gateway.yaml
```

# Delete LB svc

Conflict with 80 port of Istio LB service
```sh
kubectl delete service frontend-external -n default
```

Hosts edit
```sh
kubectl edit VirtualService/frontend-ingress
kubectl edit Gateway/frontend-gateway
```
