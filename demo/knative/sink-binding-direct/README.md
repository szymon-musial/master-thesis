
```sh
kubectl apply -f .\kn_resources.yaml
```

```sh
$display_pod=kubectl get pods -l serving.knative.dev/configuration=event-display-direct-binding -o name
kubectl logs $display_pod

☁️  cloudevents.Event
Validation: valid
Context Attributes,
  specversion: 1.0
  type: szymon.example
  source: urn:event:from:szymon/example
  id: 0d62d699-21d4-423b-858c-9846694d501e
  time: 2023-08-31T11:17:27.669Z
  datacontenttype: application/json; charset=utf-8
Data,
  {
    "userAgent": "Mozilla/5.0 (Windows NT; Windows NT 10.0; pl-PL) WindowsPowerShell/5.1.22621.1778"
  }
```
