[###](https://knative.dev/docs/eventing/brokers/broker-types/kafka-broker/)


Missing kn boker components

```sh
kind: KnativeEventing
metadata:
  name: knative-eventing
  namespace: knative-eventing
spec:
  source:
    kafka:
      enabled: true
```

```sh
kubectl get deployments.apps -n knative-eventing | grep kafka
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
kafka-controller              1/1     1            1           22d
kafka-webhook-eventing        1/1     1            1           22d
```

```sh
kubectl apply --filename https://github.com/knative-sandbox/eventing-kafka-broker/releases/download/knative-v1.11.1/eventing-kafka-broker.yaml
```
Now controller and broker controller

```sh
kubectl get deployments.apps -n knative-eventing | grep kafka
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
kafka-broker-dispatcher       1/1     1            1           41h
kafka-broker-receiver         1/1     1            1           41h
kafka-controller              1/1     1            1           22d
kafka-webhook-eventing        1/1     1            1           22d
```


https://strimzi.io/quickstarts/

```sh
kubectl create namespace kafka
kubectl create -f 'https://strimzi.io/install/latest?namespace=kafka' -n kafka

kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-persistent.yaml -n kafka 
# not kafka-persistent-single.yaml
```

```sh
kn service create kafka-ui --image provectuslabs/kafka-ui --env KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS="my-cluster-kafka-bootstrap.kafka:9092" --env KAFKA_CLUSTERS_0_NAME="My kafka cluster" -n kafka --security-context none
```

```sh
kubectl apply -f .\kafka-broker.yaml                                                                                                                                   
kn broker list
NAME              URL                                                                                      AGE   CONDITIONS   READY   REASON
my-kafka-broker   http://kafka-broker-ingress.knative-eventing.svc.cluster.local/default/my-kafka-broker   11s   7 OK / 7     True
```

```sh
kubectl apply -f .\kn_resources.yaml
```


```sh
$display_pod=kubectl get pods -l serving.knative.dev/configuration=event-display-kafka -o name
kubectl logs $display_pod

☁️  cloudevents.Event
Validation: valid
Context Attributes,
  specversion: 1.0
  type: szymon.example
  source: urn:event:from:szymon/example
  id: e1565b0d-f59d-4c80-8247-8a584c08a8f6
  time: 2023-08-31T12:08:47.54Z
  datacontenttype: application/json; charset=utf-8
Data,
  {
    "userAgent": "Mozilla/5.0 (Windows NT; Windows NT 10.0; pl-PL) WindowsPowerShell/5.1.22621.1778"
  }
```

