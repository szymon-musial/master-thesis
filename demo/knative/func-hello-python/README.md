# DEMO
```sh
kn func create --language python
kn func run --registry=registrykn.szymonmusial.eu.org 

   Building function image
   Still building
   Still building
   Yes, still building
   Don't give up on me
   Function image built: registrykn.szymonmusial.eu.org/func-hello-python:latest
Function already built.  Use --build to force a rebuild.
Function started on port 8080
❯ kn func deploy

   Pushing function image to the registry "registrykn.szymonmusial.eu.org" using the "" user credentials
   ⬆️  Deploying function to the cluster
   ✅ Function deployed in namespace "default" and exposed at URL: 
   http://func-hello-python.default.127.0.0.1.sslip.io
```


```sh
❯ kn func invoke
Received response
{"message":"Hello World"}
```

```sh
❯ kn service list
NAME                URL                                                   LATEST                    AGE    CONDITIONS   READY   REASON
func-hello-python   http://func-hello-python.default.127.0.0.1.sslip.io   func-hello-python-00001   3m8s   3 OK / 3     True
```

# Python HTTP Function

Welcome to your new Python function project! The boilerplate function
code can be found in [`func.py`](./func.py). This function will respond
to incoming HTTP GET and POST requests.

## Endpoints

Running this function will expose three endpoints.

  * `/` The endpoint for your function.
  * `/health/readiness` The endpoint for a readiness health check
  * `/health/liveness` The endpoint for a liveness health check

The health checks can be accessed in your browser at
[http://localhost:8080/health/readiness]() and
[http://localhost:8080/health/liveness]().

You can use `func invoke` to send an HTTP request to the function endpoint.


## Testing

This function project includes a [unit test](./test_func.py). Update this
as you add business logic to your function in order to test its behavior.

```console
python test_func.py
```
