```sh
kn service create my-nginx --image nginx --port 80 --namespace default
```

```sh
❯ kn service create my-nginx --image nginx --port 80 --namespace default
Creating service 'my-nginx' in namespace 'default':

  5.679s Configuration "my-nginx" is waiting for a Revision to become ready.
  5.716s Ingress has not yet been reconciled.
  5.757s Waiting for load balancer to be ready
  5.951s Ready to serve.

Service 'my-nginx' created to latest revision 'my-nginx-00001' is available at URL:
http://my-nginx.default.127.0.0.1.sslip.io
```

```sh
❯ kn service list -n default
NAME       URL                                          LATEST           AGE    CONDITIONS   READY   REASON
my-nginx   http://my-nginx.default.127.0.0.1.sslip.io   my-nginx-00001   8m7s   3 OK / 3     True 
```

```sh
kubectl get ksvc -n default
```