const { CloudEvent, Emitter, emitterFor, httpTransport } = require('cloudevents');

const K_SINK = process.env['K_SINK'];
K_SINK || (process.stdout.write('Error: K_SINK Environment variable is not defined') && process.exit())
console.log(`Sink URL is ${K_SINK}`);

const source = 'urn:event:from:szymon/example';
const type = 'szymon.example';

function emitMyEvent(data)
{
  let eventIndex = 0;

  console.log(`Emitting event # ${++eventIndex}`);

  // Create a new CloudEvent
  const event = new CloudEvent({ source, type, data });

  // Emits the 'cloudevent' Node.js event application-wide
  event.emit();
}

// Create a function that can post an event
const emit = emitterFor(httpTransport(K_SINK));

// Send the CloudEvent any time a Node.js 'cloudevent' event is emitted
Emitter.on('cloudevent', emit);


/**
 * Your HTTP handling function, invoked with each request. This is an example
 * function that echoes its input to the caller, and returns an error if
 * the incoming request is something other than an HTTP POST or GET.
 *
 * In can be invoked with 'func invoke'
 * It can be tested with 'npm test'
 *
 * @param {Context} context a context object.
 * @param {object} context.body the request body if any
 * @param {object} context.query the query string deserialized as an object, if any
 * @param {object} context.log logging object with methods for 'info', 'warn', 'error', etc.
 * @param {object} context.headers the HTTP request headers
 * @param {string} context.method the HTTP request method
 * @param {string} context.httpVersion the HTTP protocol version
 * See: https://github.com/knative/func/blob/main/docs/function-developers/nodejs.md#the-context-object
 */
const handle = async (context, body) => {
  // YOUR CODE HERE
  context.log.info("query", context.query);
  context.log.info("body", body);

  // If the request is an HTTP POST, the context will contain the request body
  if (context.method === 'POST') {
    return { body };
  } else if (context.method === 'GET') {
    // If the request is an HTTP GET, the context will include a query string, if it exists
    let returnObj = {userAgent: context?.headers["user-agent"] ?? 'empty user agent'}
    emitMyEvent(returnObj)
    return returnObj
  } else {
    return { statusCode: 405, statusMessage: 'Method not allowed' };
  }
}

// Export the function
module.exports = { handle };
