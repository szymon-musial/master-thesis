## packages

```sh
kn func create --language node
```

```bash
npm install
```

## updated func

```yaml
specVersion: 0.35.0
name: my-custom-event-source
runtime: node
registry: registrykn.szymonmusial.eu.org
image: registrykn.szymonmusial.eu.org/my-custom-event-source:latest
imageDigest: sha256:457de2b17f09f413aea0bad9e3c394a2b266fe5b9c4ff2c13430cef0520daea5
created: 2023-08-29T23:49:25.3027502+02:00
build:
  builder: pack
  pvcSize: 256Mi
deploy:
  namespace: default
```

```sh
kn func deploy
```
