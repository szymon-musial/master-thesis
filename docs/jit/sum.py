import pandas as pd
import os


folder_path = '/home/szymon/repo/master-thesis/docs/jit/all-in-one'


sum_df = []


files = os.listdir(folder_path)

sum_df = pd.read_csv(os.path.join(folder_path, files[0]))


for filename in files[1:]:
    if filename.endswith('.csv'):
        file_path = os.path.join(folder_path, filename)        
        df = pd.read_csv(file_path)
        
        for column in df.columns:
            if column != 'n':
                if column in sum_df:
                    sum_df[column] = sum_df[column].add(df[column], fill_value=0)
      
sum_df.to_csv(os.path.join(folder_path, 'sum.csv'), index=False)
print('Sumy zapisano w pliku sum.csv')
