import pandas as pd
import os

# Ustaw ścieżkę do folderu z plikami CSV
folder_path = './separated'

# Iteruj przez wszystkie pliki w folderze
for filename in os.listdir(folder_path):
    # Sprawdź czy plik to plik CSV
    if filename.endswith('.csv'):
        # Utwórz pełną ścieżkę do pliku
        file_path = os.path.join(folder_path, filename)
        
        # Wczytaj plik CSV do DataFrame
        df = pd.read_csv(file_path)
        
        # Dodaj kolumnę 'n' na początku z wartością równą indeksowi wiersza + 1
        df.insert(0, 'n', range(1, len(df) + 1))
        
        # Zapisz zmodyfikowany DataFrame do tego samego pliku CSV
        df.to_csv(file_path, index=False)
        print(f'Zmodyfikowano plik: {file_path}')
