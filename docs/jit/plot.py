import os
import pandas as pd
import matplotlib.pyplot as plt


main_directory = '/home/szymon/repo/master-thesis/docs/jit'
dataframes = []

for root, dirs, files in os.walk(main_directory):
    for file in files:
        if file.endswith('.csv'):
            file_path = os.path.join(root, file)
            df = pd.read_csv(file_path)

         #   df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
            df.set_index('n', inplace=True)
            
            
            # if "db-saver" in file_path:            
            #     cutoff_datetime = pd.Timestamp('2024-07-25 20:30:23')
            #     df.loc[df.index > cutoff_datetime, 'rx_bytes'] -= 15000
                
            #     df.loc[df.index > cutoff_datetime, 'rx_packets'] -= 220
            #     df.loc[df.index > cutoff_datetime, 'tx_packets'] -= 220

                
            # df = df.apply(lambda x: x - x.min())
            # df.index = df.index - df.index.min()
            
            dataframes.append((f"{os.path.basename(root)}/{file}", df))


series = [
    ['dispatcher', 'local_tasks_gw_api'],
    ['policy-enforcement', 'sync'],
    ['measurements'],
    ['sum']
]


for plot in series:

    plt.figure(figsize=(8,4))

    for file_name, df in dataframes:
        if any(ax in file_name for ax in plot):
            label = file_name.replace('/', ': ').rstrip('.csv')
            if 'sum' in file_name:
                label = file_name.split('/')[0]
            plt.plot(df.index[:280], df[' jit_method_count'][:280], label=label)
        # if "sync" in file_name:
        #     plt.plot(df.index, df['tx_bytes'], label="Sync transmitted in " + ("binary" if "binary" in file_name else "structured") + " mode")
        # if "db-saver" in file_name:
        #     plt.plot(df.index, df['rx_bytes'], label="Db saver received in " + ("binary" if "binary" in file_name else "structured") + " mode")

    # plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/1000:.0f}'))
    # plt.gca().xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/1000_000_000:.0f}'))

    plt.xticks([x for x in range(0, 271, 30)])

    
    #plt.title(f'Compiled method count via runtime for all in one and separated c# applications')
    plt.xlabel('Time [s]')
    plt.ylabel("Compiled method count")
    plt.legend(loc='lower right')
    plt.grid(True)
    plt.savefig(os.path.join(os.path.dirname(__file__), f"jit_{''.join(plot)}.svg"))
    plt.show()
