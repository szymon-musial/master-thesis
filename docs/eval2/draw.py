import os
import pandas as pd
import matplotlib.pyplot as plt

plt.figure(figsize=(8,4))

main_directory = '/home/szymon/repo/master-thesis/docs/eval2'
dataframes = []

for root, dirs, files in os.walk(main_directory):
    for file in files:
        if file.endswith('.csv'):
            file_path = os.path.join(root, file)
            df = pd.read_csv(file_path)

            df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
            df.set_index('timestamp', inplace=True)
            
            
            if "db-saver" in file_path:            
                cutoff_datetime = pd.Timestamp('2024-07-25 20:30:23')
                df.loc[df.index > cutoff_datetime, 'rx_bytes'] -= 15000
                
                df.loc[df.index > cutoff_datetime, 'rx_packets'] -= 220
                df.loc[df.index > cutoff_datetime, 'tx_packets'] -= 220

                
            df = df.apply(lambda x: x - x.min())
            df.index = df.index - df.index.min()
            
            dataframes.append((f"{os.path.basename(root)}/{file}", df))


for file_name, df in dataframes:
    if "sync" in file_name:
        plt.plot(df.index, df['tx_bytes'], label="Sync transmitted in " + ("binary" if "binary" in file_name else "structured") + " mode")
    if "db-saver" in file_name:
        plt.plot(df.index, df['rx_bytes'], label="Db saver received in " + ("binary" if "binary" in file_name else "structured") + " mode")

plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/1000:.0f}'))
plt.gca().xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/1000_000_000:.0f}'))

plt.xticks([x * 1000_000_000 for x in range(0, 320, 30)])


#plt.title(f'Packet size over time for each cloud event message mode')
plt.xlabel('Time [s]')
plt.ylabel("Cumulative data transfer [kB]")
plt.legend()
plt.grid(True)
plt.savefig(os.path.join(os.path.dirname(__file__), "packet size over time for both ce mode.svg"))
plt.show()


exit()




for y in ['rx_bytes', 'tx_bytes']: # , 'rx_packets', 'tx_packets']:

    for file_name, df in dataframes:
        plt.plot(df.index, df[y], label=f"{y}_{file_name}")

    plt.title(f'{y} over Time for Each File')
    plt.xlabel('Timestamp')
    plt.ylabel(y)
    plt.legend()
    plt.grid(True)
    plt.show()
