# Tests

```sh
mkdir ./test_reports
```

# Iter8 

https://knative.dev/blog/articles/performance-test-with-slos/

## install on wsl

```sh
wget https://github.com/iter8-tools/iter8/releases/download/v0.15.8/iter8-linux-amd64.tar.gz -O /tmp/iter8-linux-amd64.tar.gz
tar -xvf /tmp/iter8-linux-amd64.tar.gz linux-amd64/iter8
sudo mv ./linux-amd64/iter8 /usr/bin/
```

## sample nginx

```sh

iter8 k launch \
--set "tasks={ready,http,assess}" \
--set ready.ksvc=my-nginx \
--set http.url=http://my-nginx.default.svc.cluster.local:80 \
--set http.numRequests=100 \
--set http.connections=10 \
--set http.qps=20 \
--set assess.SLOs.upper.http/latency-mean=200 \
--set assess.SLOs.upper.http/error-count=0 \
--set runner=job
```

## grpc service

```sh
 kn service create hello-grpc \
--image docker.io/grpc/java-example-hostname:latest \
--port h2c:50051 \
--revision-name=grpc \
--namespace default

```

## grpc tests

```sh

iter8 k launch \
--set "tasks={ready,grpc,assess}" \
--set ready.ksvc=hello-grpc \
--set grpc.host="hello-grpc.default.svc.cluster.local:80" \
--set grpc.call="helloworld.Greeter.SayHello" \
--set grpc.total=100 \
--set grpc.concurrency=10 \
--set grpc.rps=20 \
--set grpc.protoURL="https://raw.githubusercontent.com/grpc/grpc-java/master/examples/example-hostname/src/main/proto/helloworld/helloworld.proto" \
--set grpc.data.name="frodo" \
--set assess.SLOs.upper.grpc/error-rate=0 \
--set assess.SLOs.upper.grpc/latency/mean=400 \
--set assess.SLOs.upper.grpc/latency/p90=500 \
--set runner=job  \
--set logLevel=debug 
```

## show reports

```sh
iter8 k report
iter8 k report -o html > ./test_reports/report.html
```

# kperf

## install go lang using [apt](https://github.com/golang/go/wiki/Ubuntu)
```sh
sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt update
sudo apt install -y golang
go version
go version go1.20.7 linux/amd64
```


```sh
cd
git clone https://github.com/knative-extensions/kperf.git ./kperf
cd ./kperf
git checkout 42375508684647c5f7f77d7dd2fba3c158836cf3
```



## install

```sh
./hack/build.sh
mv kperf /usr/local/bin/
kperf service --help
```


## Run

```sh
kubectl create ns kperf-test
kubectl label ns kperf-test istio-injection=enabled 
```

## Kperf config

https://github.com/knative/infra

`~/.config/kperf/config.yaml`

```yml
# create config file
service:
  svc-prefix: ktest
  namespace: kperf-test
  range: 0,19 
  generate:
    number: 20
    interval: 10
    batch: 10
    concurrency: 5
    namespace-prefix:
    namespace-range:
    min-scale: 0
    max-scale: 0
  load:
    load-concurrency: 30
    load-duration: 60s
```

## Run tests

### Create testing svc

```sh
kperf service generate
```
### Clean testing svc
```sh
kperf service clean --namespace kperf-test --svc-prefix ktest
```
### Run test

Clean and generate svc before run tests

```sh
kperf service measure --namespace kperf-test --svc-prefix ktest --range 0,19 --verbose --output ./test_reports/

kperf service scale --namespace kperf-test --svc-prefix ktest --range 0,9 --iterations 5 --verbose --output ./test_reports/

kperf service load --range 0,9 --verbose --output ./test_reports/
```

### Knative eventing e2e tests


## Ko dependency

```sh
cd
VERSION=0.14.1
OS=Linux
ARCH=x86_64
curl -sSfL "https://github.com/ko-build/ko/releases/download/v${VERSION}/ko_${VERSION}_${OS}_${ARCH}.tar.gz" > ko.tar.gz
tar xzf ko.tar.gz ko
chmod +x ./ko
mv ./ko /usr/bin/
ko
```
### Clone in wsl file system, not windows mount due to end line issues at .sh files

```sh
cd
git clone https://github.com/knative/eventing.git ./eventing
cd eventing
git checkout knative-v1.11.1
```
Upload images

```sh
export PLATFORM=linux/arm64
export KO_DOCKER_REPO=registrykn.szymonmusial.eu.org
./test/upload-test-images.sh
```
Run tests

```sh
export SYSTEM_NAMESPACE=knative-eventing
go test -v -tags=e2e -count=1 ./test/e2e
```

## Output with regex: (---.*)
```sh
--- PASS: TestBrokerChannelFlowTriggerV1BrokerV1 (1.05s)
--- PASS: TestBrokerChannelFlowTriggerV1BrokerV1 (1.05s)
--- PASS: TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (44.98s)
--- PASS: TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (44.98s)
--- PASS: TestEventTransformationForTriggerV1BrokerV1 (0.05s)
--- PASS: TestEventTransformationForTriggerV1BrokerV1 (0.05s)
--- PASS: TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (36.52s)
--- PASS: TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (36.52s)
--- PASS: TestBrokerRedelivery (0.00s)
--- PASS: TestBrokerRedelivery (0.00s)
--- PASS: TestChannelClusterDefaulter (35.61s)
--- PASS: TestChannelClusterDefaulter (35.61s)
--- PASS: TestChannelClusterDefaulter/InMemoryChannel-messaging.knative.dev/v1 (35.61s)
--- PASS: TestChannelClusterDefaulter/InMemoryChannel-messaging.knative.dev/v1 (35.61s)
--- PASS: TestChannelNamespaceDefaulter (36.24s)
--- PASS: TestChannelNamespaceDefaulter (36.24s)
--- PASS: TestChannelNamespaceDefaulter/InMemoryChannel-messaging.knative.dev/v1 (36.24s)
--- PASS: TestChannelNamespaceDefaulter/InMemoryChannel-messaging.knative.dev/v1 (36.24s)
--- PASS: TestParallel (0.04s)
--- PASS: TestParallel (0.04s)
--- PASS: TestParallel/InMemoryChannel-messaging.knative.dev/v1 (46.93s)
--- PASS: TestParallel/InMemoryChannel-messaging.knative.dev/v1 (46.93s)
--- PASS: TestParallelV1 (1.04s)
--- PASS: TestParallelV1 (1.04s)
--- PASS: TestParallelV1/InMemoryChannel-messaging.knative.dev/v1 (49.55s)
--- PASS: TestParallelV1/InMemoryChannel-messaging.knative.dev/v1 (49.55s)
--- PASS: TestSequence (1.04s)
--- PASS: TestSequence (1.04s)
--- PASS: TestSequence/InMemoryChannel-messaging.knative.dev/v1 (44.68s)
--- PASS: TestSequence/InMemoryChannel-messaging.knative.dev/v1 (44.68s)
--- PASS: TestSequenceV1 (1.04s)
--- PASS: TestSequenceV1 (1.04s)
--- PASS: TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1 (45.96s)
--- PASS: TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1 (45.96s)
--- PASS: TestChannelNamespaceDefaulting (3.68s)
--- PASS: TestChannelNamespaceDefaulting (3.68s)
--- PASS: TestBrokerNamespaceDefaulting (3.72s)
--- PASS: TestBrokerNamespaceDefaulting (3.72s)
```

## Performance tests

```sh

export PLATFORM=linux/arm64
export KO_DOCKER_REPO=registrykn.szymonmusial.eu.org

kubectl create ns perf-eventing

# in eventing repo

kubectl create configmap -n perf-eventing config-mako --from-file=test/performance/benchmarks/broker-imc/dev.config
ko apply -f test/performance/benchmarks/broker-imc/100-broker-perf-setup.yaml

# Build, push, and run benchmark job
ko apply -f test/performance/benchmarks/broker-imc/300-broker-imc-increasing-load-setup.yaml

export pod_name=$(kubectl get pod -n perf-eventing -l role=broker-perf-aggregator  -o custom-columns=":metadata.name" | tr -d '\n')
export output_file="$(pwd)/data.csv"

echo $pod_name
echo $output_file
```

### Get linux path from windows side. Type `explorer.exe .`

```sh
# get csv results
bash "vendor/knative.dev/pkg/test/mako/stub-sidecar/read_results.sh" "$pod_name" perf-eventing ${mako_port:-10001} ${timeout:-120} ${retries:-100} ${retries_interval:-10} "$output_file"


gnuplot -c {...test\performance\latency-and-thpt-plot.plg} {...data.csv} 0.5 0 1100
gnuplot -c \\wsl.localhost\Ubuntu-20.04\root\eventing\test\performance\latency-and-thpt-plot.plg \\wsl.localhost\Ubuntu-20.04\root\eventing\data.csv 0.5 0 1100
```

### Benchmark logs

```sh

15:04:48 Creating a receiver
15:04:48 Creating a sender
15:04:48 Waiting for all Pods to be ready in namespace perf-eventing
15:04:49 Starting 2 executors
15:04:49 --- BEGIN WARMUP ---
15:04:49 Starting warmup
15:04:49 Started receiver timeout timer of duration 8m6s
15:04:49 CloudEvents receiver started
15:05:04 ---- END WARMUP ----
15:05:04 --- BEGIN BENCHMARK ---
15:05:04 Starting events processor
15:05:04 Starting benchmark
15:05:04 Starting pace 1° at 500 rps for 30s seconds
15:05:39 Triggering GC
15:05:42 Starting pace 2° at 600 rps for 30s seconds
15:06:17 Triggering GC
15:06:20 Starting pace 3° at 700 rps for 30s seconds
15:06:55 Triggering GC
15:06:58 Starting pace 4° at 800 rps for 30s seconds
15:07:47 Triggering GC
15:07:50 Starting pace 5° at 900 rps for 30s seconds
15:08:25 Triggering GC
15:08:28 Starting pace 6° at 1000 rps for 30s seconds
15:09:04 Triggering GC
15:09:07 Benchmark completed in 4m2.828118103s
15:09:07 All requests sent
15:09:07 All channels closed
15:09:07 ---- END BENCHMARK ----
15:09:07 Sending collected data to the aggregator
15:09:07 Sent count     : 135000
15:09:07 Accepted count : 134999
15:09:07 End message received correctly
15:09:12 Receiver closed
15:09:12 Received count : 135000
15:09:12 Performance image completed

```



### All e2e output

```sh
root@Szymon-Desktop:~/eventing# go test -v -tags=e2e -count=1 ./test/e2e
=== RUN   TestBrokerChannelFlowTriggerV1BrokerV1
=== RUN   TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e0  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e0      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e0  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e0,Namespace:eventing-e2e0,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e0,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e0-eventwatcher  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e0-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e0-eventwatcher  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e0-eventwatcher,Namespace:eventing-e2e0,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e0-eventwatcher,},}
=== PAUSE TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:202: Creating v1 broker e2e-brokerchannel-broker
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-brokerchannel-trans-pod  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:be47d3e9-52a5-41e7-ab96-2ecf2ec5cddf] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_TYPE,Value:type2,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_SOURCE,Value:http://source2.com,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_DATA,Value:{"msg":"transformed body"},ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e0,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-brokerchannel-trans-pod
    creation.go:241: Creating v1 trigger e2e-brokerchannel-trigger1
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-brokerchannel-logger-pod1  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:52363cdc-44cb-4774-8b8e-4fdde7bbb655] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e0,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-brokerchannel-logger-pod1
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 12 events
    creation.go:241: Creating v1 trigger e2e-brokerchannel-trigger2
    creation.go:61: Creating channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-brokerchannel-channel
    creation.go:241: Creating v1 trigger e2e-brokerchannel-trigger3
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-brokerchannel-logger-pod2  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:1bf0256c-593b-434c-a379-50202444804f] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e0,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-brokerchannel-logger-pod2
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 22 events
    creation.go:115: Creating v1 subscription e2e-brokerchannel-subscription for channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-brokerchannel-channel
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-brokerchannel-sender  eventing-e2e0    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://broker-ingress.knative-eventing.svc.cluster.local/eventing-e2e0/e2e-brokerchannel-broker -event {"specversion":"1.0","id":"b95b2c5e-0d98-46ef-89dc-ce165b86cb6d","source":"http://source1.com","type":"type1","datacontenttype":"application/json","data_base64":"eyJtc2ciOiJlMmUtYnJva2VyY2hhbm5lbC1ib2R5In0="}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    broker_channel_flow_helper.go:181: Assert passed
    broker_channel_flow_helper.go:182: Assert passed
    broker_channel_flow_helper.go:184: Assert passed
    broker_channel_flow_helper.go:185: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.177d14698814373c,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:34277ac7-530a-4f41-8b24-5e2c19fe16c7,ResourceVersion:7788036,Generation:0,CreationTimestamp:2023-08-20 13:37:58 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:37:58 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788034,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e0/e2e-brokerchannel-trans-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:37:58 +0200 CEST
        LastTimestamp:2023-08-20 13:37:58 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.177d1469ae0f3192,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:3ef2859a-6907-421f-88da-c9cd2e8a751d,ResourceVersion:7788040,Generation:0,CreationTimestamp:2023-08-20 13:37:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:37:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788035,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:37:59 +0200 CEST
        LastTimestamp:2023-08-20 13:37:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.177d1469b0c28ffe,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:f632c0e6-640d-4310-9c39-7dc668ab57cc,ResourceVersion:7788041,Generation:0,CreationTimestamp:2023-08-20 13:37:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:37:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788035,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:37:59 +0200 CEST
        LastTimestamp:2023-08-20 13:37:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.177d1469b94b32f4,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:8712b4a2-2a21-4ea3-bdd3-cd7285be917f,ResourceVersion:7788042,Generation:0,CreationTimestamp:2023-08-20 13:37:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:37:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788035,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:37:59 +0200 CEST
        LastTimestamp:2023-08-20 13:37:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.177d1469dd41c73d,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:b1e925b3-a431-4be7-a08c-206933163638,ResourceVersion:7788065,Generation:0,CreationTimestamp:2023-08-20 13:37:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:37:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788035,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.32:8080/healthz": dial tcp 10.1.34.32:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:37:59 +0200 CEST
        LastTimestamp:2023-08-20 13:37:59 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08.177d146a3da76118,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:e83a417d-b3af-44da-98da-8798203a32c4,ResourceVersion:7788096,Generation:0,CreationTimestamp:2023-08-20 13:38:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08,UID:05705cc5-4d48-47af-9ec4-10855b272abd,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788094,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:01 +0200 CEST
        LastTimestamp:2023-08-20 13:38:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08.177d146a402dc292,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:64690b74-f028-4b50-b5a4-675d0b0149d8,ResourceVersion:7788103,Generation:0,CreationTimestamp:2023-08-20 13:38:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08,UID:05705cc5-4d48-47af-9ec4-10855b272abd,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788094,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-brokerchannel-broker-kne-trigger"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:01 +0200 CEST
        LastTimestamp:2023-08-20 13:38:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08.177d146a42248061,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:c68bf825-bcd9-4f12-a63b-e0c5478e0ae9,ResourceVersion:7788110,Generation:0,CreationTimestamp:2023-08-20 13:38:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08,UID:05705cc5-4d48-47af-9ec4-10855b272abd,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788105,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-brokerchannel-broker-e2e-br32a364f9e450860856789acb42609e08" not present in channel "e2e-brokerchannel-broker-kne-trigger" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:01 +0200 CEST
        LastTimestamp:2023-08-20 13:38:01 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.177d146a3e502061,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:780ff151-3f98-431d-a557-d232efcfc708,ResourceVersion:7788099,Generation:0,CreationTimestamp:2023-08-20 13:38:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788095,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e0/e2e-brokerchannel-logger-pod1 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:01 +0200 CEST
        LastTimestamp:2023-08-20 13:38:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.177d146a6822782a,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:f9c5b7e6-d62e-4dbd-ba7a-d9eb1bdcc9c4,ResourceVersion:7788124,Generation:0,CreationTimestamp:2023-08-20 13:38:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788097,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:02 +0200 CEST
        LastTimestamp:2023-08-20 13:38:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.177d146a6aeca45e,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:66eb3f35-8321-4b8a-8824-ae93d24fab60,ResourceVersion:7788125,Generation:0,CreationTimestamp:2023-08-20 13:38:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788097,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:02 +0200 CEST
        LastTimestamp:2023-08-20 13:38:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.177d146a71902086,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:4507d91a-78d7-471d-8c47-39ef25cbbfc9,ResourceVersion:7788126,Generation:0,CreationTimestamp:2023-08-20 13:38:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788097,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:02 +0200 CEST
        LastTimestamp:2023-08-20 13:38:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec.177d146ab734b6c1,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:293828c1-40f9-4e87-beb0-5db5de2d2f10,ResourceVersion:7788155,Generation:0,CreationTimestamp:2023-08-20 13:38:03 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:03 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec,UID:9ad89a8c-91f8-4fa1-98e5-bbf48f6023ad,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788153,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:03 +0200 CEST
        LastTimestamp:2023-08-20 13:38:03 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec.177d146aba0d7c62,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:f9551584-b78a-4724-90f9-73d33516395a,ResourceVersion:7788159,Generation:0,CreationTimestamp:2023-08-20 13:38:03 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:03 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec,UID:9ad89a8c-91f8-4fa1-98e5-bbf48f6023ad,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788153,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-brokerchannel-broker-kne-trigger"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:03 +0200 CEST
        LastTimestamp:2023-08-20 13:38:03 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec.177d146abc99a42f,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:ecd028ca-7bca-4af0-aa60-7871e50c9fae,ResourceVersion:7788166,Generation:0,CreationTimestamp:2023-08-20 13:38:03 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:03 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec,UID:9ad89a8c-91f8-4fa1-98e5-bbf48f6023ad,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788160,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-brokerchannel-broker-e2e-br49960d93353dccbe1130763f150e62ec" not present in channel "e2e-brokerchannel-broker-kne-trigger" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:03 +0200 CEST
        LastTimestamp:2023-08-20 13:38:03 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc.177d146af42dcf0d,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:5946ce01-6e14-4878-af85-a37fa1fecfe6,ResourceVersion:7788179,Generation:0,CreationTimestamp:2023-08-20 13:38:04 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:04 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc,UID:06b859a5-bb71-4ee8-ac73-57299e253dae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788177,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:04 +0200 CEST
        LastTimestamp:2023-08-20 13:38:04 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc.177d146af78e10f1,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:fa7d2cab-4bb4-4c5f-b591-8e88e3663e9d,ResourceVersion:7788186,Generation:0,CreationTimestamp:2023-08-20 13:38:04 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:04 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc,UID:06b859a5-bb71-4ee8-ac73-57299e253dae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788177,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-brokerchannel-broker-kne-trigger"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:04 +0200 CEST
        LastTimestamp:2023-08-20 13:38:04 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc.177d146af9cc44a6,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:0dbe91b2-1adb-41fb-8b69-cad93f935c01,ResourceVersion:7788194,Generation:0,CreationTimestamp:2023-08-20 13:38:04 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:04 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc,UID:06b859a5-bb71-4ee8-ac73-57299e253dae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788188,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-brokerchannel-broker-e2e-brfd991f9355cf6a97a73578d37e58aafc" not present in channel "e2e-brokerchannel-broker-kne-trigger" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:04 +0200 CEST
        LastTimestamp:2023-08-20 13:38:04 +0200 CEST
        Count:2
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod2.177d146af52e8f3c,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:68b33ef3-2b52-44de-a7a6-7f862c86264a,ResourceVersion:7788182,Generation:0,CreationTimestamp:2023-08-20 13:38:04 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:04 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod2,UID:dee17789-36b0-4bc6-ae5d-decd8315f661,APIVersion:v1,ResourceVersion:7788178,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e0/e2e-brokerchannel-logger-pod2 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:04 +0200 CEST
        LastTimestamp:2023-08-20 13:38:04 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod2.177d146b1c19d657,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:4749cdbf-2345-479c-8bdd-684e7dda20bd,ResourceVersion:7788198,Generation:0,CreationTimestamp:2023-08-20 13:38:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod2,UID:dee17789-36b0-4bc6-ae5d-decd8315f661,APIVersion:v1,ResourceVersion:7788181,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:05 +0200 CEST
        LastTimestamp:2023-08-20 13:38:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod2.177d146b1e29f6e6,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:da56b5ab-1887-412f-a3a1-3e058684a90c,ResourceVersion:7788199,Generation:0,CreationTimestamp:2023-08-20 13:38:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod2,UID:dee17789-36b0-4bc6-ae5d-decd8315f661,APIVersion:v1,ResourceVersion:7788181,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:05 +0200 CEST
        LastTimestamp:2023-08-20 13:38:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod2.177d146b25511d3e,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:1e551477-963c-4efd-860d-9ed12f231dac,ResourceVersion:7788200,Generation:0,CreationTimestamp:2023-08-20 13:38:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod2,UID:dee17789-36b0-4bc6-ae5d-decd8315f661,APIVersion:v1,ResourceVersion:7788181,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:05 +0200 CEST
        LastTimestamp:2023-08-20 13:38:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-subscription.177d146b6d0bd120,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:4485ecd5-9b0b-4e90-834c-c3967b972934,ResourceVersion:7788258,Generation:0,CreationTimestamp:2023-08-20 13:38:06 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:06 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-subscription,UID:0bbc85e2-8004-4939-8fd9-43605af9b958,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788257,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-brokerchannel-subscription" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:06 +0200 CEST
        LastTimestamp:2023-08-20 13:38:06 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-subscription.177d146b6ed8c931,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:48cf51a7-ec75-4ee2-8101-ad3f31f51810,ResourceVersion:7788262,Generation:0,CreationTimestamp:2023-08-20 13:38:06 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:06 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-subscription,UID:0bbc85e2-8004-4939-8fd9-43605af9b958,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788257,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-brokerchannel-channel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:06 +0200 CEST
        LastTimestamp:2023-08-20 13:38:06 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-subscription.177d146b70657a49,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:fd1d9fd7-81ae-4817-874e-f353441489d6,ResourceVersion:7788267,Generation:0,CreationTimestamp:2023-08-20 13:38:06 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:06 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e0,Name:e2e-brokerchannel-subscription,UID:0bbc85e2-8004-4939-8fd9-43605af9b958,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788263,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-brokerchannel-subscription" not present in channel "e2e-brokerchannel-channel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:06 +0200 CEST
        LastTimestamp:2023-08-20 13:38:06 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-sender.177d146e0037f6c9,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:26c20cc9-c057-4304-a698-ab58356749ec,ResourceVersion:7788437,Generation:0,CreationTimestamp:2023-08-20 13:38:17 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:17 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-sender,UID:c8b870c3-4e9e-4ebd-87d6-858cb283103b,APIVersion:v1,ResourceVersion:7788435,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e0/e2e-brokerchannel-sender to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:17 +0200 CEST
        LastTimestamp:2023-08-20 13:38:17 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-sender.177d146e2cf423a7,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:291f9086-2100-4e5a-85f9-72419dc54600,ResourceVersion:7788440,Generation:0,CreationTimestamp:2023-08-20 13:38:18 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:18 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-sender,UID:c8b870c3-4e9e-4ebd-87d6-858cb283103b,APIVersion:v1,ResourceVersion:7788436,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:18 +0200 CEST
        LastTimestamp:2023-08-20 13:38:18 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-sender.177d146e2f9bce47,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:b3b8d9b2-d0d0-4d44-bb9e-c38a97fe17c2,ResourceVersion:7788441,Generation:0,CreationTimestamp:2023-08-20 13:38:18 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:18 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-sender,UID:c8b870c3-4e9e-4ebd-87d6-858cb283103b,APIVersion:v1,ResourceVersion:7788436,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:18 +0200 CEST
        LastTimestamp:2023-08-20 13:38:18 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-sender.177d146e37fb12c9,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:149c1cc9-6714-4703-bbd8-01f5d422637d,ResourceVersion:7788442,Generation:0,CreationTimestamp:2023-08-20 13:38:18 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:18 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-sender,UID:c8b870c3-4e9e-4ebd-87d6-858cb283103b,APIVersion:v1,ResourceVersion:7788436,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:18 +0200 CEST
        LastTimestamp:2023-08-20 13:38:18 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.Received.1,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:8bce5f00-4f2b-4ba8-80c7-9d3788681f16,ResourceVersion:7788512,Generation:0,CreationTimestamp:2023-08-20 13:38:23 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:23 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788100,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"b95b2c5e-0d98-46ef-89dc-ce165b86cb6d","source":"http://source1.com","type":"type1","datacontenttype":"application/json","data":{"msg":"e2e-brokerchannel-body"},"knativearrivaltime":"2023-08-20T11:38:23.960407608Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["32"],"Content-Type":["application/json"],"Host":["e2e-brokerchannel-logger-pod1.eventing-e2e0.svc.cluster.local"],"Kn-Namespace":["eventing-e2e0"],"Prefer":["reply"],"Traceparent":["00-af0bcb1058e3ba2d3170d57db0bb316c-b97afd584184c727-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.235:40800","observer":"e2e-brokerchannel-logger-pod1","time":"2023-08-20T11:38:23.982714644Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:38:23 +0200 CEST
        LastTimestamp:2023-08-20 13:38:23 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-trans-pod.Received.1,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:25aa73e4-adaf-4748-88a2-5e7d6127f785,ResourceVersion:7788511,Generation:0,CreationTimestamp:2023-08-20 13:38:23 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:23 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-trans-pod,UID:5118bd68-310b-4049-8832-b1532822294c,APIVersion:v1,ResourceVersion:7788066,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"b95b2c5e-0d98-46ef-89dc-ce165b86cb6d","source":"http://source1.com","type":"type1","datacontenttype":"application/json","data":{"msg":"e2e-brokerchannel-body"},"knativearrivaltime":"2023-08-20T11:38:23.960407608Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["32"],"Content-Type":["application/json"],"Host":["e2e-brokerchannel-trans-pod.eventing-e2e0.svc.cluster.local"],"Kn-Namespace":["eventing-e2e0"],"Prefer":["reply"],"Traceparent":["00-af0bcb1058e3ba2d3170d57db0bb316c-d8e678edf4cd0e31-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.235:59494","observer":"e2e-brokerchannel-trans-pod","time":"2023-08-20T11:38:23.98211408Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:38:23 +0200 CEST
        LastTimestamp:2023-08-20 13:38:23 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod1.Received.2,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:45d5881b-c972-4c17-8fd1-154894e8f78f,ResourceVersion:7788513,Generation:0,CreationTimestamp:2023-08-20 13:38:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod1,UID:9d73278b-fe8e-4c0f-bc96-df46a119facf,APIVersion:v1,ResourceVersion:7788100,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"b95b2c5e-0d98-46ef-89dc-ce165b86cb6d","source":"http://source2.com","type":"type2","datacontenttype":"application/json","data":{"msg":"transformed body"},"knativearrivaltime":"2023-08-20T11:38:24.009932988Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["26"],"Content-Type":["application/json"],"Host":["e2e-brokerchannel-logger-pod1.eventing-e2e0.svc.cluster.local"],"Kn-Namespace":["eventing-e2e0"],"Prefer":["reply"],"Traceparent":["00-af0bcb1058e3ba2d3170d57db0bb316c-7ba20630daf03815-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.235:40800","observer":"e2e-brokerchannel-logger-pod1","time":"2023-08-20T11:38:24.012303381Z","sequence":2}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:38:24 +0200 CEST
        LastTimestamp:2023-08-20 13:38:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-brokerchannel-logger-pod2.Received.1,GenerateName:,Namespace:eventing-e2e0,SelfLink:,UID:7e0ce48f-3ba8-44c1-9bbf-bf29f6eee84c,ResourceVersion:7788514,Generation:0,CreationTimestamp:2023-08-20 13:38:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e0,Name:e2e-brokerchannel-logger-pod2,UID:dee17789-36b0-4bc6-ae5d-decd8315f661,APIVersion:v1,ResourceVersion:7788184,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"b95b2c5e-0d98-46ef-89dc-ce165b86cb6d","source":"http://source2.com","type":"type2","datacontenttype":"application/json","data":{"msg":"transformed body"},"knativearrivaltime":"2023-08-20T11:38:24.009932988Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["26"],"Content-Type":["application/json"],"Host":["e2e-brokerchannel-logger-pod2.eventing-e2e0.svc.cluster.local"],"Kn-Namespace":["eventing-e2e0"],"Prefer":["reply"],"Traceparent":["00-af0bcb1058e3ba2d3170d57db0bb316c-731cb94544881b9b-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:34908","observer":"e2e-brokerchannel-logger-pod2","time":"2023-08-20T11:38:24.020928517Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:38:24 +0200 CEST
        LastTimestamp:2023-08-20 13:38:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 33 events seen
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-sender"
    tracker.go:139: Waiting for e2e-brokerchannel-sender to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-subscription"
    tracker.go:139: Waiting for e2e-brokerchannel-subscription to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-logger-pod2"
    tracker.go:139: Waiting for e2e-brokerchannel-logger-pod2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-logger-pod2"
    tracker.go:139: Waiting for e2e-brokerchannel-logger-pod2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-trigger3"
    tracker.go:139: Waiting for e2e-brokerchannel-trigger3 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-channel"
    tracker.go:139: Waiting for e2e-brokerchannel-channel to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-trigger2"
    tracker.go:139: Waiting for e2e-brokerchannel-trigger2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-logger-pod1"
    tracker.go:139: Waiting for e2e-brokerchannel-logger-pod1 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-logger-pod1"
    tracker.go:139: Waiting for e2e-brokerchannel-logger-pod1 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-trigger1"
    tracker.go:139: Waiting for e2e-brokerchannel-trigger1 to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-trans-pod"
    tracker.go:139: Waiting for e2e-brokerchannel-trans-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-trans-pod"
    tracker.go:139: Waiting for e2e-brokerchannel-trans-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-brokerchannel-broker"
    tracker.go:139: Waiting for e2e-brokerchannel-broker to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e0-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e0-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e0-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0"
    tracker.go:139: Waiting for eventing-e2e0 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0"
    tracker.go:139: Waiting for eventing-e2e0 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e0"
    tracker.go:139: Waiting for eventing-e2e0 to be deleted
--- PASS: TestBrokerChannelFlowTriggerV1BrokerV1 (1.05s)
    --- PASS: TestBrokerChannelFlowTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (44.98s)
=== RUN   TestBrokerNamespaceDefaulting
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e1  eventing-e2e1    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e1      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e1  eventing-e2e1    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e1,Namespace:eventing-e2e1,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e1,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e1-eventwatcher  eventing-e2e1    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e1-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e1-eventwatcher  eventing-e2e1    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e1-eventwatcher,Namespace:eventing-e2e1,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e1-eventwatcher,},}
=== PAUSE TestBrokerNamespaceDefaulting
=== RUN   TestEventTransformationForTriggerV1BrokerV1
=== RUN   TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e2  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e2      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e2  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e2,Namespace:eventing-e2e2,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e2,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e2-eventwatcher  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e2-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e2-eventwatcher  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e2-eventwatcher,Namespace:eventing-e2e2,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e2-eventwatcher,},}
=== PAUSE TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:202: Creating v1 broker inmemorychannel
    creation.go:441: Creating pod &Pod{ObjectMeta:{trans-pod  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:f0378139-c2e2-41c9-9946-661a7c4e06c1] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_TYPE,Value:type2,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_SOURCE,Value:source2,ValueFrom:nil,},EnvVar{Name:REPLY_EVENT_DATA,Value:{"msg":"transformed body"},ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e2,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service trans-pod
    creation.go:241: Creating v1 trigger trigger1
    creation.go:441: Creating pod &Pod{ObjectMeta:{recordevents-pod  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:8615a892-0a99-4a42-ab54-263bd47d5119] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e2,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service recordevents-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 11 events
    creation.go:241: Creating v1 trigger trigger2
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-eventtransformation-sender  eventing-e2e2    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://broker-ingress.knative-eventing.svc.cluster.local/eventing-e2e2/inmemorychannel -event {"specversion":"1.0","id":"a85b0283-3849-4ce6-a124-120f63b44675","source":"source1","type":"type1","datacontenttype":"application/json","data_base64":"eyJtc2ciOiJlMmUtZXZlbnR0cmFuc2Zvcm1hdGlvbi1ib2R5In0="}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    broker_event_transformation_test_helper.go:124: Assert passed
    broker_event_transformation_test_helper.go:130: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:trans-pod.177d14740f474a9c,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:2aab8585-f6e8-490d-9905-a8b9fbdedc5b,ResourceVersion:7788913,Generation:0,CreationTimestamp:2023-08-20 13:38:43 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:43 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:trans-pod,UID:538fbc73-0d4a-459f-848a-63ec8f1d0778,APIVersion:v1,ResourceVersion:7788909,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e2/trans-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:43 +0200 CEST
        LastTimestamp:2023-08-20 13:38:43 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:trans-pod.177d147441e2c9b4,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:8932825b-1cbe-4864-9ac3-f31878e4a4ef,ResourceVersion:7788931,Generation:0,CreationTimestamp:2023-08-20 13:38:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:trans-pod,UID:538fbc73-0d4a-459f-848a-63ec8f1d0778,APIVersion:v1,ResourceVersion:7788911,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:44 +0200 CEST
        LastTimestamp:2023-08-20 13:38:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:trans-pod.177d147446441f09,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:b70804aa-fea9-46cb-9ce4-7359c7debe99,ResourceVersion:7788932,Generation:0,CreationTimestamp:2023-08-20 13:38:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:trans-pod,UID:538fbc73-0d4a-459f-848a-63ec8f1d0778,APIVersion:v1,ResourceVersion:7788911,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:44 +0200 CEST
        LastTimestamp:2023-08-20 13:38:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:trans-pod.177d14744dfb1ac5,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:60e3102b-d1f7-487b-b7a8-b7599b43946a,ResourceVersion:7788933,Generation:0,CreationTimestamp:2023-08-20 13:38:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:trans-pod,UID:538fbc73-0d4a-459f-848a-63ec8f1d0778,APIVersion:v1,ResourceVersion:7788911,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:44 +0200 CEST
        LastTimestamp:2023-08-20 13:38:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808.177d14748b796998,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:0724c3e3-1cd9-4ed6-b067-464d0e9d92d1,ResourceVersion:7788973,Generation:0,CreationTimestamp:2023-08-20 13:38:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808,UID:e1c0596d-6500-4670-99cc-297610a3500d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788969,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:45 +0200 CEST
        LastTimestamp:2023-08-20 13:38:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808.177d147490207c3b,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:814485ee-6c1d-4db2-8688-df444dbd8351,ResourceVersion:7788997,Generation:0,CreationTimestamp:2023-08-20 13:38:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808,UID:e1c0596d-6500-4670-99cc-297610a3500d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7788969,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "inmemorychannel-kne-trigger"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:45 +0200 CEST
        LastTimestamp:2023-08-20 13:38:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808.177d147494a3a1b6,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:3ba66adb-d786-4266-817e-8cb9b019a39b,ResourceVersion:7789004,Generation:0,CreationTimestamp:2023-08-20 13:38:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808,UID:e1c0596d-6500-4670-99cc-297610a3500d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789001,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "inmemorychannel-trigger1-5541b3d5-8101-4d90-adba-5a9d94da7808" not present in channel "inmemorychannel-kne-trigger" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:45 +0200 CEST
        LastTimestamp:2023-08-20 13:38:45 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:recordevents-pod.177d14748de42252,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:1c9b1299-0826-42c6-b7b6-08a1605a67a3,ResourceVersion:7788990,Generation:0,CreationTimestamp:2023-08-20 13:38:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:recordevents-pod,UID:ad9a0d60-fe44-4da2-9686-c3f222ce249f,APIVersion:v1,ResourceVersion:7788974,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e2/recordevents-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:45 +0200 CEST
        LastTimestamp:2023-08-20 13:38:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:recordevents-pod.177d1474b7fd438b,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:796b94ac-1d2a-4481-b193-97930958f7a7,ResourceVersion:7789021,Generation:0,CreationTimestamp:2023-08-20 13:38:46 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:46 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:recordevents-pod,UID:ad9a0d60-fe44-4da2-9686-c3f222ce249f,APIVersion:v1,ResourceVersion:7788983,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:46 +0200 CEST
        LastTimestamp:2023-08-20 13:38:46 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:recordevents-pod.177d1474c700be38,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:69abd64f-83e2-4a58-8a87-4ba7fadd61ee,ResourceVersion:7789032,Generation:0,CreationTimestamp:2023-08-20 13:38:46 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:46 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:recordevents-pod,UID:ad9a0d60-fe44-4da2-9686-c3f222ce249f,APIVersion:v1,ResourceVersion:7788983,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:46 +0200 CEST
        LastTimestamp:2023-08-20 13:38:46 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:recordevents-pod.177d1474d9e04383,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:25882a37-eecf-42d0-ae8c-12cf6fc03b0e,ResourceVersion:7789050,Generation:0,CreationTimestamp:2023-08-20 13:38:47 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:47 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:recordevents-pod,UID:ad9a0d60-fe44-4da2-9686-c3f222ce249f,APIVersion:v1,ResourceVersion:7788983,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:38:47 +0200 CEST
        LastTimestamp:2023-08-20 13:38:47 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435.177d1475421086ad,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:c5cecbfc-ca11-4ef6-8d8c-bacd437d7ff3,ResourceVersion:7789100,Generation:0,CreationTimestamp:2023-08-20 13:38:48 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:48 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435,UID:82203cae-9688-419b-a540-0cce72bc02f2,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789098,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:48 +0200 CEST
        LastTimestamp:2023-08-20 13:38:48 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435.177d1475448e0376,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:80e0f359-aa93-4dff-a4f3-318b3d3c01bb,ResourceVersion:7789104,Generation:0,CreationTimestamp:2023-08-20 13:38:48 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:48 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435,UID:82203cae-9688-419b-a540-0cce72bc02f2,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789098,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "inmemorychannel-kne-trigger"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:48 +0200 CEST
        LastTimestamp:2023-08-20 13:38:48 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435.177d1475488a30b5,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:a7222e0b-93e3-4182-b732-026dac21878d,ResourceVersion:7789114,Generation:0,CreationTimestamp:2023-08-20 13:38:48 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:48 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e2,Name:inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435,UID:82203cae-9688-419b-a540-0cce72bc02f2,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789106,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "inmemorychannel-trigger2-06c3f576-6ffa-4834-a63d-9391f6c18435" not present in channel "inmemorychannel-kne-trigger" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:38:48 +0200 CEST
        LastTimestamp:2023-08-20 13:38:48 +0200 CEST
        Count:2
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-eventtransformation-sender.177d1477d35d5a28,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:2d20dcf3-4e03-43bf-87a7-99fccf59201c,ResourceVersion:7789289,Generation:0,CreationTimestamp:2023-08-20 13:38:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:38:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:e2e-eventtransformation-sender,UID:75280f14-4774-457d-a5ad-d146c0c3b1ab,APIVersion:v1,ResourceVersion:7789284,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e2/e2e-eventtransformation-sender to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:38:59 +0200 CEST
        LastTimestamp:2023-08-20 13:38:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-eventtransformation-sender.177d1477fe3874e2,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:9920b2d0-940d-4a64-930a-ce9e92ccda2c,ResourceVersion:7789293,Generation:0,CreationTimestamp:2023-08-20 13:39:00 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:00 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:e2e-eventtransformation-sender,UID:75280f14-4774-457d-a5ad-d146c0c3b1ab,APIVersion:v1,ResourceVersion:7789287,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:00 +0200 CEST
        LastTimestamp:2023-08-20 13:39:00 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-eventtransformation-sender.177d14780107f90c,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:0c56abc3-3a24-4d51-b9dd-c1b52ce5d7da,ResourceVersion:7789294,Generation:0,CreationTimestamp:2023-08-20 13:39:00 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:00 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:e2e-eventtransformation-sender,UID:75280f14-4774-457d-a5ad-d146c0c3b1ab,APIVersion:v1,ResourceVersion:7789287,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:00 +0200 CEST
        LastTimestamp:2023-08-20 13:39:00 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-eventtransformation-sender.177d147809efb4da,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:7e8a5ef0-fb0e-47d1-ac47-0cfa9cd359b8,ResourceVersion:7789295,Generation:0,CreationTimestamp:2023-08-20 13:39:00 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:00 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:e2e-eventtransformation-sender,UID:75280f14-4774-457d-a5ad-d146c0c3b1ab,APIVersion:v1,ResourceVersion:7789287,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:00 +0200 CEST
        LastTimestamp:2023-08-20 13:39:00 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:recordevents-pod.Received.1,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:432cb13c-862c-49b8-813e-b28e623fabda,ResourceVersion:7789384,Generation:0,CreationTimestamp:2023-08-20 13:39:06 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:06 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:recordevents-pod,UID:ad9a0d60-fe44-4da2-9686-c3f222ce249f,APIVersion:v1,ResourceVersion:7789073,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"a85b0283-3849-4ce6-a124-120f63b44675","source":"source2","type":"type2","datacontenttype":"application/json","data":{"msg":"transformed body"},"knativearrivaltime":"2023-08-20T11:39:06.148377233Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["26"],"Content-Type":["application/json"],"Host":["recordevents-pod.eventing-e2e2.svc.cluster.local"],"Kn-Namespace":["eventing-e2e2"],"Prefer":["reply"],"Traceparent":["00-724b27b0fde6b41aecea7182930e71fd-78cc2f3711c379c2-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.235:35504","observer":"recordevents-pod","time":"2023-08-20T11:39:06.158984619Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:39:06 +0200 CEST
        LastTimestamp:2023-08-20 13:39:06 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:trans-pod.Received.1,GenerateName:,Namespace:eventing-e2e2,SelfLink:,UID:1b25e368-35f4-4788-9f62-ce92f8713605,ResourceVersion:7789383,Generation:0,CreationTimestamp:2023-08-20 13:39:06 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:06 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e2,Name:trans-pod,UID:538fbc73-0d4a-459f-848a-63ec8f1d0778,APIVersion:v1,ResourceVersion:7788916,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"a85b0283-3849-4ce6-a124-120f63b44675","source":"source1","type":"type1","datacontenttype":"application/json","data":{"msg":"e2e-eventtransformation-body"},"knativearrivaltime":"2023-08-20T11:39:06.10615226Z"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["38"],"Content-Type":["application/json"],"Host":["trans-pod.eventing-e2e2.svc.cluster.local"],"Kn-Namespace":["eventing-e2e2"],"Prefer":["reply"],"Traceparent":["00-724b27b0fde6b41aecea7182930e71fd-1013c224ca051f9e-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.235:34252","observer":"trans-pod","time":"2023-08-20T11:39:06.118891673Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:39:06 +0200 CEST
        LastTimestamp:2023-08-20 13:39:06 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 20 events seen
    tracker.go:133: Cleaning resource: "e2e-eventtransformation-sender"
    tracker.go:139: Waiting for e2e-eventtransformation-sender to be deleted
    tracker.go:133: Cleaning resource: "trigger2"
    tracker.go:139: Waiting for trigger2 to be deleted
    tracker.go:133: Cleaning resource: "recordevents-pod"
    tracker.go:139: Waiting for recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "recordevents-pod"
    tracker.go:139: Waiting for recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "trigger1"
    tracker.go:139: Waiting for trigger1 to be deleted
    tracker.go:133: Cleaning resource: "trans-pod"
    tracker.go:139: Waiting for trans-pod to be deleted
    tracker.go:133: Cleaning resource: "trans-pod"
    tracker.go:139: Waiting for trans-pod to be deleted
    tracker.go:133: Cleaning resource: "inmemorychannel"
    tracker.go:139: Waiting for inmemorychannel to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e2-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e2-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e2-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2"
    tracker.go:139: Waiting for eventing-e2e2 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2"
    tracker.go:139: Waiting for eventing-e2e2 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e2"
    tracker.go:139: Waiting for eventing-e2e2 to be deleted
--- PASS: TestEventTransformationForTriggerV1BrokerV1 (0.05s)
    --- PASS: TestEventTransformationForTriggerV1BrokerV1/InMemoryChannel-messaging.knative.dev/v1 (36.52s)
=== RUN   TestBrokerRedelivery
--- PASS: TestBrokerRedelivery (0.00s)
=== RUN   TestChannelClusterDefaulter
=== RUN   TestChannelClusterDefaulter/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e3  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e3      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e3  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e3,Namespace:eventing-e2e3,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e3,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e3-eventwatcher  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e3-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e3-eventwatcher  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e3-eventwatcher,Namespace:eventing-e2e3,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e3-eventwatcher,},}
    creation.go:89: Creating default v1 channel &{TypeMeta:{Kind:Channel APIVersion:messaging.knative.dev/v1} ObjectMeta:{Name:e2e-defaulter-channel GenerateName: Namespace:eventing-e2e3 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[messaging.knative.dev/subscribable:v1] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{ChannelTemplate:<nil> ChannelableSpec:{SubscribableSpec:{Subscribers:[]} Delivery:<nil>}} Status:{ChannelableStatus:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} AddressStatus:{Address:<nil> Addresses:[]} SubscribableStatus:{Subscribers:[]} DeliveryStatus:{DeadLetterSinkURI: DeadLetterSinkCACerts:<nil>}} Channel:<nil>}}
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-defaulter-recordevents-pod  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:5e6d1f05-ed5b-482b-b83e-03a90af1e8c0] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e3,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-defaulter-recordevents-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 5 events
    creation.go:115: Creating v1 subscription e2e-defaulter-subscription for channel &TypeMeta{Kind:Channel,APIVersion:messaging.knative.dev/v1,}-e2e-defaulter-channel
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-defaulter-sender  eventing-e2e3    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://e2e-defaulter-channel-kn-channel.eventing-e2e3.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-defaulter-sender.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","data_base64":"eyJtc2ciOiJUZXN0U2luZ2xlRXZlbnQgMWQ3NDBiYjgtODZkMS00N2NkLTk4MjUtMTgzMTI0NWQxNDFmIn0="}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    channel_defaulter_test_helper.go:159: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d147d8b555bbb,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:ba85b90e-a4d9-4ab0-9c85-160753948592,ResourceVersion:7789738,Generation:0,CreationTimestamp:2023-08-20 13:39:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789735,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e3/e2e-defaulter-recordevents-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:39:24 +0200 CEST
        LastTimestamp:2023-08-20 13:39:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d147db7e814ae,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:3a81800f-ea55-4ca4-833c-7f6d887fe529,ResourceVersion:7789741,Generation:0,CreationTimestamp:2023-08-20 13:39:25 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:25 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789737,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:25 +0200 CEST
        LastTimestamp:2023-08-20 13:39:25 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d147dbd498266,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:51bc80a9-ec25-4ae5-9484-407d736ae8eb,ResourceVersion:7789742,Generation:0,CreationTimestamp:2023-08-20 13:39:25 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:25 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789737,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:25 +0200 CEST
        LastTimestamp:2023-08-20 13:39:25 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d147dc61d3cfc,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:96e127ea-b79d-4ff8-a232-027a71654362,ResourceVersion:7789743,Generation:0,CreationTimestamp:2023-08-20 13:39:25 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:25 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789737,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:25 +0200 CEST
        LastTimestamp:2023-08-20 13:39:25 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d147dd010fcf2,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:f425f37d-0154-4495-b83d-e672b09a815b,ResourceVersion:7789744,Generation:0,CreationTimestamp:2023-08-20 13:39:25 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:25 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789737,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.39:8080/healthz": dial tcp 10.1.34.39:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:25 +0200 CEST
        LastTimestamp:2023-08-20 13:39:25 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d147e3ebe3a41,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:e9c96334-e8bb-4429-a67f-14ad9e737fc4,ResourceVersion:7789796,Generation:0,CreationTimestamp:2023-08-20 13:39:27 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:27 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e3,Name:e2e-defaulter-subscription,UID:4059b5e4-25e4-406c-883e-a3b88d3bc94a,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789795,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-defaulter-subscription" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:39:27 +0200 CEST
        LastTimestamp:2023-08-20 13:39:27 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d147e4100c88b,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:7ab16077-c89e-464f-b90d-17421697a480,ResourceVersion:7789806,Generation:0,CreationTimestamp:2023-08-20 13:39:27 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:27 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e3,Name:e2e-defaulter-subscription,UID:4059b5e4-25e4-406c-883e-a3b88d3bc94a,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789795,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-defaulter-channel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:39:27 +0200 CEST
        LastTimestamp:2023-08-20 13:39:27 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d147e4349f9d1,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:264c7b80-c220-4789-b72d-db6ef0891699,ResourceVersion:7789818,Generation:0,CreationTimestamp:2023-08-20 13:39:27 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:27 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e3,Name:e2e-defaulter-subscription,UID:4059b5e4-25e4-406c-883e-a3b88d3bc94a,APIVersion:messaging.knative.dev/v1,ResourceVersion:7789809,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-defaulter-subscription" not present in channel "e2e-defaulter-channel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:39:27 +0200 CEST
        LastTimestamp:2023-08-20 13:39:27 +0200 CEST
        Count:2
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d1480d00b8fbd,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:1d1dd5fb-c97b-4183-b5d8-125fa2b62761,ResourceVersion:7789986,Generation:0,CreationTimestamp:2023-08-20 13:39:38 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:38 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-sender,UID:bde7e3d9-c170-44d9-92c7-865ba21da759,APIVersion:v1,ResourceVersion:7789984,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e3/e2e-defaulter-sender to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:39:38 +0200 CEST
        LastTimestamp:2023-08-20 13:39:38 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d1480fafa840c,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:04d9ea67-b6a6-444c-beee-bc3c7313d490,ResourceVersion:7789990,Generation:0,CreationTimestamp:2023-08-20 13:39:39 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:39 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-sender,UID:bde7e3d9-c170-44d9-92c7-865ba21da759,APIVersion:v1,ResourceVersion:7789985,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:39 +0200 CEST
        LastTimestamp:2023-08-20 13:39:39 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d1480fd7fdeb4,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:80772f22-5175-4bfc-9db1-f67bb9981685,ResourceVersion:7789991,Generation:0,CreationTimestamp:2023-08-20 13:39:39 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:39 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-sender,UID:bde7e3d9-c170-44d9-92c7-865ba21da759,APIVersion:v1,ResourceVersion:7789985,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:39 +0200 CEST
        LastTimestamp:2023-08-20 13:39:39 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d1481075866e6,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:64b8df60-5084-44fe-9ada-8d2995982cfb,ResourceVersion:7789993,Generation:0,CreationTimestamp:2023-08-20 13:39:39 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:39 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-sender,UID:bde7e3d9-c170-44d9-92c7-865ba21da759,APIVersion:v1,ResourceVersion:7789985,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:39:39 +0200 CEST
        LastTimestamp:2023-08-20 13:39:39 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.Received.1,GenerateName:,Namespace:eventing-e2e3,SelfLink:,UID:91c537e3-6c04-4e35-80fa-d52e4c3bb77e,ResourceVersion:7790065,Generation:0,CreationTimestamp:2023-08-20 13:39:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:39:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e3,Name:e2e-defaulter-recordevents-pod,UID:b1801b1a-730e-4741-8f24-f923aaa3b708,APIVersion:v1,ResourceVersion:7789745,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-defaulter-sender.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","data":{"msg":"TestSingleEvent 1d740bb8-86d1-47cd-9825-1831245d141f"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["62"],"Content-Type":["application/json"],"Host":["e2e-defaulter-recordevents-pod.eventing-e2e3.svc.cluster.local"],"Kn-Namespace":["eventing-e2e3"],"Prefer":["reply"],"Traceparent":["00-a130dc7c4096ffa9a2cb7057cbbb7343-50632c87d6161960-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:44176","observer":"e2e-defaulter-recordevents-pod","time":"2023-08-20T11:39:44.747854678Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:39:44 +0200 CEST
        LastTimestamp:2023-08-20 13:39:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 13 events seen
    tracker.go:133: Cleaning resource: "e2e-defaulter-sender"
    tracker.go:139: Waiting for e2e-defaulter-sender to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-subscription"
    tracker.go:139: Waiting for e2e-defaulter-subscription to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-recordevents-pod"
    tracker.go:139: Waiting for e2e-defaulter-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-recordevents-pod"
    tracker.go:139: Waiting for e2e-defaulter-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-channel"
    tracker.go:139: Waiting for e2e-defaulter-channel to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e3-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e3-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e3-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3"
    tracker.go:139: Waiting for eventing-e2e3 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3"
    tracker.go:139: Waiting for eventing-e2e3 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e3"
    tracker.go:139: Waiting for eventing-e2e3 to be deleted
--- PASS: TestChannelClusterDefaulter (35.61s)
    --- PASS: TestChannelClusterDefaulter/InMemoryChannel-messaging.knative.dev/v1 (35.61s)
=== RUN   TestChannelNamespaceDefaulter
=== RUN   TestChannelNamespaceDefaulter/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e4  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e4      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e4  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e4,Namespace:eventing-e2e4,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e4,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e4-eventwatcher  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e4-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e4-eventwatcher  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e4-eventwatcher,Namespace:eventing-e2e4,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e4-eventwatcher,},}
    creation.go:89: Creating default v1 channel &{TypeMeta:{Kind:Channel APIVersion:messaging.knative.dev/v1} ObjectMeta:{Name:e2e-defaulter-channel GenerateName: Namespace:eventing-e2e4 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[messaging.knative.dev/subscribable:v1] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{ChannelTemplate:<nil> ChannelableSpec:{SubscribableSpec:{Subscribers:[]} Delivery:<nil>}} Status:{ChannelableStatus:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} AddressStatus:{Address:<nil> Addresses:[]} SubscribableStatus:{Subscribers:[]} DeliveryStatus:{DeadLetterSinkURI: DeadLetterSinkCACerts:<nil>}} Channel:<nil>}}
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-defaulter-recordevents-pod  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:f10a7cbd-ede1-4080-bafa-490eebac0be0] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e4,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-defaulter-recordevents-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 5 events
    creation.go:115: Creating v1 subscription e2e-defaulter-subscription for channel &TypeMeta{Kind:Channel,APIVersion:messaging.knative.dev/v1,}-e2e-defaulter-channel
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-defaulter-sender  eventing-e2e4    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://e2e-defaulter-channel-kn-channel.eventing-e2e4.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-defaulter-sender.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","data_base64":"eyJtc2ciOiJUZXN0U2luZ2xlRXZlbnQgNmQ2NjU0ZTMtYWVkMi00MmIxLTgwOTAtYmZiNTJlNzhiMzRiIn0="}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    channel_defaulter_test_helper.go:159: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d1485fa6ac2bf,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:100738a8-42a5-4606-9d56-d22bff929b5b,ResourceVersion:7790385,Generation:0,CreationTimestamp:2023-08-20 13:40:00 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:00 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790382,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e4/e2e-defaulter-recordevents-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:00 +0200 CEST
        LastTimestamp:2023-08-20 13:40:00 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d14861f985920,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:69b9d716-52cc-43ec-92dc-332eebf4344c,ResourceVersion:7790388,Generation:0,CreationTimestamp:2023-08-20 13:40:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790384,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:01 +0200 CEST
        LastTimestamp:2023-08-20 13:40:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d1486224c04c4,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:8b131604-731e-4fda-b542-feda5e25d008,ResourceVersion:7790389,Generation:0,CreationTimestamp:2023-08-20 13:40:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790384,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:01 +0200 CEST
        LastTimestamp:2023-08-20 13:40:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d14862b0932f6,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:9a9081ba-0170-4d69-af94-2f59e90b6fbe,ResourceVersion:7790390,Generation:0,CreationTimestamp:2023-08-20 13:40:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790384,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:01 +0200 CEST
        LastTimestamp:2023-08-20 13:40:01 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.177d14862e2bc067,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:9e8cbaad-49bd-4906-9457-f38309daa7fc,ResourceVersion:7790391,Generation:0,CreationTimestamp:2023-08-20 13:40:01 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:01 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790384,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.41:8080/healthz": dial tcp 10.1.34.41:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:01 +0200 CEST
        LastTimestamp:2023-08-20 13:40:01 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d148673047fc1,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:99285a88-d3e8-4aa6-ae15-568641e8d923,ResourceVersion:7790425,Generation:0,CreationTimestamp:2023-08-20 13:40:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e4,Name:e2e-defaulter-subscription,UID:3af11503-81d3-464d-8198-f68a31be9946,APIVersion:messaging.knative.dev/v1,ResourceVersion:7790424,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-defaulter-subscription" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:02 +0200 CEST
        LastTimestamp:2023-08-20 13:40:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d1486776e629d,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:903b4463-cacc-4a4f-a532-67af09aa11d3,ResourceVersion:7790430,Generation:0,CreationTimestamp:2023-08-20 13:40:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e4,Name:e2e-defaulter-subscription,UID:3af11503-81d3-464d-8198-f68a31be9946,APIVersion:messaging.knative.dev/v1,ResourceVersion:7790424,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-defaulter-channel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:02 +0200 CEST
        LastTimestamp:2023-08-20 13:40:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-subscription.177d148678e4f567,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:224b2506-7ef4-4265-bc13-a3b206ba0af8,ResourceVersion:7790436,Generation:0,CreationTimestamp:2023-08-20 13:40:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e4,Name:e2e-defaulter-subscription,UID:3af11503-81d3-464d-8198-f68a31be9946,APIVersion:messaging.knative.dev/v1,ResourceVersion:7790432,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-defaulter-subscription" not present in channel "e2e-defaulter-channel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:02 +0200 CEST
        LastTimestamp:2023-08-20 13:40:02 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d148903bd195b,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:5a010dd2-1438-4637-82e6-aece55f961c9,ResourceVersion:7790582,Generation:0,CreationTimestamp:2023-08-20 13:40:13 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:13 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-sender,UID:6ef6de41-8705-411f-be98-ccd17439c17a,APIVersion:v1,ResourceVersion:7790580,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e4/e2e-defaulter-sender to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:13 +0200 CEST
        LastTimestamp:2023-08-20 13:40:13 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d14892f076ffe,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:6789be95-ab46-45fb-8d12-cce85d1f0e78,ResourceVersion:7790599,Generation:0,CreationTimestamp:2023-08-20 13:40:14 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:14 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-sender,UID:6ef6de41-8705-411f-be98-ccd17439c17a,APIVersion:v1,ResourceVersion:7790581,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:14 +0200 CEST
        LastTimestamp:2023-08-20 13:40:14 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d148931918de3,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:901486fc-0aee-4274-b82b-84535add4c9e,ResourceVersion:7790600,Generation:0,CreationTimestamp:2023-08-20 13:40:14 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:14 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-sender,UID:6ef6de41-8705-411f-be98-ccd17439c17a,APIVersion:v1,ResourceVersion:7790581,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:14 +0200 CEST
        LastTimestamp:2023-08-20 13:40:14 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-sender.177d148938586dcf,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:401cdefb-9842-4f41-ac2b-138ce8fa024c,ResourceVersion:7790601,Generation:0,CreationTimestamp:2023-08-20 13:40:14 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:14 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-sender,UID:6ef6de41-8705-411f-be98-ccd17439c17a,APIVersion:v1,ResourceVersion:7790581,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:14 +0200 CEST
        LastTimestamp:2023-08-20 13:40:14 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-defaulter-recordevents-pod.Received.1,GenerateName:,Namespace:eventing-e2e4,SelfLink:,UID:d65c38ec-6b57-4ed1-8a31-3a681d08d2ff,ResourceVersion:7790679,Generation:0,CreationTimestamp:2023-08-20 13:40:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e4,Name:e2e-defaulter-recordevents-pod,UID:7dfafe49-3cd7-4648-9c51-72e30066be89,APIVersion:v1,ResourceVersion:7790392,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-defaulter-sender.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","data":{"msg":"TestSingleEvent 6d6654e3-aed2-42b1-8090-bfb52e78b34b"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["62"],"Content-Type":["application/json"],"Host":["e2e-defaulter-recordevents-pod.eventing-e2e4.svc.cluster.local"],"Kn-Namespace":["eventing-e2e4"],"Prefer":["reply"],"Traceparent":["00-5e26e12076a6bb54d06bfe6a75476d52-493e10618d66e5ed-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:41888","observer":"e2e-defaulter-recordevents-pod","time":"2023-08-20T11:40:19.923525994Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:40:19 +0200 CEST
        LastTimestamp:2023-08-20 13:40:19 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 13 events seen
    tracker.go:133: Cleaning resource: "e2e-defaulter-sender"
    tracker.go:139: Waiting for e2e-defaulter-sender to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-subscription"
    tracker.go:139: Waiting for e2e-defaulter-subscription to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-recordevents-pod"
    tracker.go:139: Waiting for e2e-defaulter-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-recordevents-pod"
    tracker.go:139: Waiting for e2e-defaulter-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-defaulter-channel"
    tracker.go:139: Waiting for e2e-defaulter-channel to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e4-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e4-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e4-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4"
    tracker.go:139: Waiting for eventing-e2e4 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4"
    tracker.go:139: Waiting for eventing-e2e4 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e4"
    tracker.go:139: Waiting for eventing-e2e4 to be deleted
--- PASS: TestChannelNamespaceDefaulter (36.24s)
    --- PASS: TestChannelNamespaceDefaulter/InMemoryChannel-messaging.knative.dev/v1 (36.24s)
=== RUN   TestChannelNamespaceDefaulting
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e5  eventing-e2e5    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e5      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e5  eventing-e2e5    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e5,Namespace:eventing-e2e5,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e5,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e5-eventwatcher  eventing-e2e5    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e5-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e5-eventwatcher  eventing-e2e5    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e5-eventwatcher,Namespace:eventing-e2e5,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e5-eventwatcher,},}
=== PAUSE TestChannelNamespaceDefaulting
=== RUN   TestParallel
=== RUN   TestParallel/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e6  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e6      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e6  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e6,Namespace:eventing-e2e6,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e6,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e6-eventwatcher  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e6-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e6-eventwatcher  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e6-eventwatcher,Namespace:eventing-e2e6,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e6-eventwatcher,},}
=== PAUSE TestParallel/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestParallel/InMemoryChannel-messaging.knative.dev/v1
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-0-filter  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:0bd0b1c0-119b-48f7-9ac2-3bbd43a85d75] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e6,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-0-filter
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-0-sub  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:891ef652-914b-4971-97a4-5ef404838b08] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:parallel-two-branches-pass-first-branch-only-branch-0-sub,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e6,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-0-sub
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-1-filter  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:5fe5efbc-782f-433b-aac1-64e04620ade7] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e6,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-1-filter
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-1-sub  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:7acc9993-ffc7-4421-9b62-82faf38d7003] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:parallel-two-branches-pass-first-branch-only-branch-1-sub,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e6,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-1-sub
    creation.go:441: Creating pod &Pod{ObjectMeta:{two-branches-pass-first-branch-only-event-record-pod  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:ba430dee-132b-4b33-bfca-f4b277bd1e8c] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e6,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service two-branches-pass-first-branch-only-event-record-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 21 events
    creation.go:61: Creating channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-reply-two-branches-pass-first-branch-only
    creation.go:115: Creating v1 subscription reply-two-branches-pass-first-branch-only for channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-reply-two-branches-pass-first-branch-only
    creation.go:293: Creating flows parallel &{TypeMeta:{Kind: APIVersion:} ObjectMeta:{Name:two-branches-pass-first-branch-only GenerateName: Namespace:eventing-e2e6 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{Branches:[{Filter:0xc000c2d350 Subscriber:{Ref:Kind = Service, Namespace = eventing-e2e6, Name = parallel-two-branches-pass-first-branch-only-branch-0-sub, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Reply:<nil> Delivery:<nil>} {Filter:0xc000664780 Subscriber:{Ref:Kind = Service, Namespace = eventing-e2e6, Name = parallel-two-branches-pass-first-branch-only-branch-1-sub, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Reply:<nil> Delivery:<nil>}] ChannelTemplate:&TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,} Reply:0xc000831dd0} Status:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} IngressChannelStatus:{Channel:{Kind: Namespace: Name: UID: APIVersion: ResourceVersion: FieldPath:} ReadyCondition:{Type: Status: Severity: LastTransitionTime:{Inner:0001-01-01 00:00:00 +0000 UTC} Reason: Message:}} BranchStatuses:[] AddressStatus:{Address:<nil> Addresses:[]}}}
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-parallel  eventing-e2e6    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://two-branches-pass-first-branch-only-kn-parallel-kn-channel.eventing-e2e6.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:40:55.88247873Z","data_base64":"eyJtc2ciOiJUZXN0Rmxvd1BhcmFsbGVsIDhkNzM4MGU1LTZhOWEtNDRiZi1iY2VhLWFmOTc0YWE4MDhlMiJ9"}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    parallel_test_helper.go:151: Assert passed
    parallel_test_helper.go:151: Assert passed
    parallel_test_helper.go:151: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d148d619d582f,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:846b8052-54d8-49ed-b9ca-36250026623f,ResourceVersion:7790931,Generation:0,CreationTimestamp:2023-08-20 13:40:32 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:32 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790929,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/parallel-two-branches-pass-first-branch-only-branch-0-filter to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:32 +0200 CEST
        LastTimestamp:2023-08-20 13:40:32 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d148d8894e28a,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:f46df5fe-9363-4326-97a7-ed63432f9d34,ResourceVersion:7790934,Generation:0,CreationTimestamp:2023-08-20 13:40:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790930,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:33 +0200 CEST
        LastTimestamp:2023-08-20 13:40:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d148d8af94955,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:3948b231-ad26-4091-8c34-e13af2b4ac3b,ResourceVersion:7790935,Generation:0,CreationTimestamp:2023-08-20 13:40:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790930,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:33 +0200 CEST
        LastTimestamp:2023-08-20 13:40:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d148d9265e191,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:096887f9-da1b-4dd8-9747-599b78733345,ResourceVersion:7790936,Generation:0,CreationTimestamp:2023-08-20 13:40:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790930,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:33 +0200 CEST
        LastTimestamp:2023-08-20 13:40:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d148d94122e69,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:46bff959-69fb-43ed-8321-2131d6b57818,ResourceVersion:7790937,Generation:0,CreationTimestamp:2023-08-20 13:40:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790930,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.43:8080/healthz": dial tcp 10.1.34.43:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:33 +0200 CEST
        LastTimestamp:2023-08-20 13:40:33 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d148de95e66f8,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:964a5a1c-4002-41c0-9a64-59c0acd89b75,ResourceVersion:7790966,Generation:0,CreationTimestamp:2023-08-20 13:40:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:28d9dcf8-397b-48e5-90b4-9a7cd9961b44,APIVersion:v1,ResourceVersion:7790964,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/parallel-two-branches-pass-first-branch-only-branch-0-sub to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:34 +0200 CEST
        LastTimestamp:2023-08-20 13:40:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d148e10ebdc2f,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:a6274800-7f27-4c90-bede-44d803552715,ResourceVersion:7790968,Generation:0,CreationTimestamp:2023-08-20 13:40:35 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:35 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:28d9dcf8-397b-48e5-90b4-9a7cd9961b44,APIVersion:v1,ResourceVersion:7790965,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:35 +0200 CEST
        LastTimestamp:2023-08-20 13:40:35 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d148e12f176f5,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:aa80a0dd-5d55-478f-b24d-e57e40e6d321,ResourceVersion:7790969,Generation:0,CreationTimestamp:2023-08-20 13:40:35 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:35 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:28d9dcf8-397b-48e5-90b4-9a7cd9961b44,APIVersion:v1,ResourceVersion:7790965,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:35 +0200 CEST
        LastTimestamp:2023-08-20 13:40:35 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d148e1a40e2b7,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:db35ddbe-36e2-41e2-9d84-5219b1f65e4e,ResourceVersion:7790970,Generation:0,CreationTimestamp:2023-08-20 13:40:35 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:35 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:28d9dcf8-397b-48e5-90b4-9a7cd9961b44,APIVersion:v1,ResourceVersion:7790965,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:35 +0200 CEST
        LastTimestamp:2023-08-20 13:40:35 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d148e623e5ebf,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:571fa1ef-73c8-4f36-b1c3-b88af1a980e0,ResourceVersion:7791052,Generation:0,CreationTimestamp:2023-08-20 13:40:36 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:36 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:abfca65f-300f-4df0-909e-6367dc6c7962,APIVersion:v1,ResourceVersion:7791050,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/parallel-two-branches-pass-first-branch-only-branch-1-filter to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:36 +0200 CEST
        LastTimestamp:2023-08-20 13:40:36 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d148e8903ba8d,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:26518fa0-0d87-4b98-b0ef-5da0653c9ac2,ResourceVersion:7791055,Generation:0,CreationTimestamp:2023-08-20 13:40:37 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:37 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:abfca65f-300f-4df0-909e-6367dc6c7962,APIVersion:v1,ResourceVersion:7791051,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:37 +0200 CEST
        LastTimestamp:2023-08-20 13:40:37 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d148e8bb6c2a8,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:2745075e-bfe8-4168-b897-303041576daa,ResourceVersion:7791056,Generation:0,CreationTimestamp:2023-08-20 13:40:37 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:37 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:abfca65f-300f-4df0-909e-6367dc6c7962,APIVersion:v1,ResourceVersion:7791051,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:37 +0200 CEST
        LastTimestamp:2023-08-20 13:40:37 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d148e941e9a91,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:f6b9614a-9c51-44c4-9828-565ae2ceafcc,ResourceVersion:7791068,Generation:0,CreationTimestamp:2023-08-20 13:40:37 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:37 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:abfca65f-300f-4df0-909e-6367dc6c7962,APIVersion:v1,ResourceVersion:7791051,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:37 +0200 CEST
        LastTimestamp:2023-08-20 13:40:37 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d148f18447471,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:a7db824d-eb16-4427-9b62-3fc3eed2b8d7,ResourceVersion:7791105,Generation:0,CreationTimestamp:2023-08-20 13:40:39 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:39 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:7b5a70cc-2c82-4fce-be7e-4ef51e3d6185,APIVersion:v1,ResourceVersion:7791100,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/parallel-two-branches-pass-first-branch-only-branch-1-sub to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:39 +0200 CEST
        LastTimestamp:2023-08-20 13:40:39 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d148f4279bd90,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:99903210-c519-45f8-9b90-d3498cc98127,ResourceVersion:7791123,Generation:0,CreationTimestamp:2023-08-20 13:40:40 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:40 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:7b5a70cc-2c82-4fce-be7e-4ef51e3d6185,APIVersion:v1,ResourceVersion:7791102,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:40 +0200 CEST
        LastTimestamp:2023-08-20 13:40:40 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d148f44c82b68,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:ce7b77a8-8730-41a0-8083-6d18bee5b65b,ResourceVersion:7791124,Generation:0,CreationTimestamp:2023-08-20 13:40:40 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:40 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:7b5a70cc-2c82-4fce-be7e-4ef51e3d6185,APIVersion:v1,ResourceVersion:7791102,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:40 +0200 CEST
        LastTimestamp:2023-08-20 13:40:40 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d148f4cd2b17b,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:4bf37239-fa2f-461f-9e7a-0f3252e3de83,ResourceVersion:7791125,Generation:0,CreationTimestamp:2023-08-20 13:40:40 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:40 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:7b5a70cc-2c82-4fce-be7e-4ef51e3d6185,APIVersion:v1,ResourceVersion:7791102,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:40 +0200 CEST
        LastTimestamp:2023-08-20 13:40:40 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d148fccb024ad,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:6b235f72-919b-49bd-a681-cdedeff9893e,ResourceVersion:7791165,Generation:0,CreationTimestamp:2023-08-20 13:40:42 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:42 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-event-record-pod,UID:f65d0f2a-6306-4ec8-9684-6bfaa109d07d,APIVersion:v1,ResourceVersion:7791163,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/two-branches-pass-first-branch-only-event-record-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:42 +0200 CEST
        LastTimestamp:2023-08-20 13:40:42 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d148ff381e400,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:59ccc1fa-fb08-4147-a2ab-2f5db6ea0667,ResourceVersion:7791167,Generation:0,CreationTimestamp:2023-08-20 13:40:43 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:43 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-event-record-pod,UID:f65d0f2a-6306-4ec8-9684-6bfaa109d07d,APIVersion:v1,ResourceVersion:7791164,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:43 +0200 CEST
        LastTimestamp:2023-08-20 13:40:43 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d148ff5dd2419,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:ae2e3fa4-e739-48ba-9409-cce287b22744,ResourceVersion:7791168,Generation:0,CreationTimestamp:2023-08-20 13:40:43 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:43 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-event-record-pod,UID:f65d0f2a-6306-4ec8-9684-6bfaa109d07d,APIVersion:v1,ResourceVersion:7791164,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:43 +0200 CEST
        LastTimestamp:2023-08-20 13:40:43 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d148fff83e0d4,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:176ec7ad-1d82-45bf-929c-fcbe54befb04,ResourceVersion:7791173,Generation:0,CreationTimestamp:2023-08-20 13:40:43 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:43 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-event-record-pod,UID:f65d0f2a-6306-4ec8-9684-6bfaa109d07d,APIVersion:v1,ResourceVersion:7791164,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:43 +0200 CEST
        LastTimestamp:2023-08-20 13:40:43 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:reply-two-branches-pass-first-branch-only.177d149045dc17c8,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:7473adbb-c4db-46eb-ac38-5c12a580aea7,ResourceVersion:7791206,Generation:0,CreationTimestamp:2023-08-20 13:40:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:reply-two-branches-pass-first-branch-only,UID:6bd568fc-4ff7-4c16-a1b8-d0e725b9e871,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791205,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "reply-two-branches-pass-first-branch-only" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:44 +0200 CEST
        LastTimestamp:2023-08-20 13:40:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:reply-two-branches-pass-first-branch-only.177d149048abf54a,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:b4bcf637-0628-475f-96a0-75c729aa2310,ResourceVersion:7791220,Generation:0,CreationTimestamp:2023-08-20 13:40:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:reply-two-branches-pass-first-branch-only,UID:6bd568fc-4ff7-4c16-a1b8-d0e725b9e871,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791205,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "reply-two-branches-pass-first-branch-only"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:44 +0200 CEST
        LastTimestamp:2023-08-20 13:40:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-0.177d14904abab01f,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:fbff4030-a78f-4404-92e8-764073d8c60e,ResourceVersion:7791230,Generation:0,CreationTimestamp:2023-08-20 13:40:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-0,UID:f9e9246b-4793-4b25-aa77-2b8391278d6e,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791229,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:44 +0200 CEST
        LastTimestamp:2023-08-20 13:40:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d14904937093b,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:5af48297-6fef-4fd4-a012-38b0dff38134,ResourceVersion:7791224,Generation:0,CreationTimestamp:2023-08-20 13:40:44 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:44 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:8db2255e-aba0-4f73-8c83-c384af1f3dcf,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791219,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-filter-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:44 +0200 CEST
        LastTimestamp:2023-08-20 13:40:44 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-0.177d149053ae5cd4,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:9c43d355-f3d3-482f-a934-0f71a8919671,ResourceVersion:7791241,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-0,UID:f9e9246b-4793-4b25-aa77-2b8391278d6e,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791229,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel-0"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-1.177d1490546db35f,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:d1d951db-5c1a-4744-8619-32056fe61226,ResourceVersion:7791247,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-1,UID:bdbacfb3-e710-4c51-abf8-928c98b697b7,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791242,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-1.177d149057ea8ff1,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:0986027b-faa8-4409-99c4-446365ca43fa,ResourceVersion:7791258,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-1,UID:bdbacfb3-e710-4c51-abf8-928c98b697b7,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791242,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel-1"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d1490536de51a,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:8d3fac40-ce06-4d82-86f9-66401eded018,ResourceVersion:7791239,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:8db2255e-aba0-4f73-8c83-c384af1f3dcf,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791219,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d14905428755a,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:ff687d69-b13c-4814-9481-5a1181b57eec,ResourceVersion:7791244,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:f5b47f3c-645d-4bf7-a68a-bbeaa74006be,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791240,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-filter-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d149057ef1f4c,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:281e9b23-c2c4-4926-86ce-43d7207c1cb0,ResourceVersion:7791260,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:f5b47f3c-645d-4bf7-a68a-bbeaa74006be,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791240,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d14905b9b342a,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:1cf5d774-7e55-4b77-b761-4f7574c1478e,ResourceVersion:7791267,Generation:0,CreationTimestamp:2023-08-20 13:40:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:f5b47f3c-645d-4bf7-a68a-bbeaa74006be,APIVersion:messaging.knative.dev/v1,ResourceVersion:7791257,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "two-branches-pass-first-branch-only-kn-parallel-filter-1" not present in channel "two-branches-pass-first-branch-only-kn-parallel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:40:45 +0200 CEST
        LastTimestamp:2023-08-20 13:40:45 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d1492d963bd83,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:3fa17a10-9ab6-4cda-aa1f-cf9d77575602,ResourceVersion:7791427,Generation:0,CreationTimestamp:2023-08-20 13:40:55 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:55 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:e2e-parallel,UID:2acc0da1-acb8-44f5-bc75-7f10c2b7feb6,APIVersion:v1,ResourceVersion:7791422,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e6/e2e-parallel to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:40:55 +0200 CEST
        LastTimestamp:2023-08-20 13:40:55 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d149307e74ad4,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:2952c546-e141-45c6-9ebd-172558ab263d,ResourceVersion:7791456,Generation:0,CreationTimestamp:2023-08-20 13:40:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:e2e-parallel,UID:2acc0da1-acb8-44f5-bc75-7f10c2b7feb6,APIVersion:v1,ResourceVersion:7791425,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:56 +0200 CEST
        LastTimestamp:2023-08-20 13:40:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d14930a5d10a8,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:6d5e8f81-3392-46e3-b472-f46c15043f1c,ResourceVersion:7791457,Generation:0,CreationTimestamp:2023-08-20 13:40:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:e2e-parallel,UID:2acc0da1-acb8-44f5-bc75-7f10c2b7feb6,APIVersion:v1,ResourceVersion:7791425,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:56 +0200 CEST
        LastTimestamp:2023-08-20 13:40:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d14931111b0c2,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:42ef1a55-a080-4042-ad8a-5deb71430e86,ResourceVersion:7791458,Generation:0,CreationTimestamp:2023-08-20 13:40:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:40:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:e2e-parallel,UID:2acc0da1-acb8-44f5-bc75-7f10c2b7feb6,APIVersion:v1,ResourceVersion:7791425,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:40:56 +0200 CEST
        LastTimestamp:2023-08-20 13:40:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.Received.1,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:65a5c510-6e28-4a80-973d-ad05ec6d2909,ResourceVersion:7791523,Generation:0,CreationTimestamp:2023-08-20 13:41:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:d90be98e-9174-4b0e-936c-c5d43806ec66,APIVersion:v1,ResourceVersion:7790938,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:40:55.88247873Z","data":{"msg":"TestFlowParallel 8d7380e5-6a9a-44bf-bcea-af974aa808e2"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-0-filter.eventing-e2e6.svc.cluster.local"],"Kn-Namespace":["eventing-e2e6"],"Prefer":["reply"],"Traceparent":["00-05ae47c09d1a5ce6c4fd21adca2e4b98-e855e1761396e484-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:49114","observer":"parallel-two-branches-pass-first-branch-only-branch-0-filter","time":"2023-08-20T11:41:02.230313242Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:02 +0200 CEST
        LastTimestamp:2023-08-20 13:41:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.Received.1,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:7108543e-279f-4746-96e4-f78a5d6cfe8c,ResourceVersion:7791525,Generation:0,CreationTimestamp:2023-08-20 13:41:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:28d9dcf8-397b-48e5-90b4-9a7cd9961b44,APIVersion:v1,ResourceVersion:7790967,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:40:55.88247873Z","data":{"msg":"TestFlowParallel 8d7380e5-6a9a-44bf-bcea-af974aa808e2"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-0-sub.eventing-e2e6.svc.cluster.local"],"Kn-Namespace":["eventing-e2e6"],"Prefer":["reply"],"Traceparent":["00-05ae47c09d1a5ce6c4fd21adca2e4b98-876db28c99c5e31b-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:48606","observer":"parallel-two-branches-pass-first-branch-only-branch-0-sub","time":"2023-08-20T11:41:02.267997407Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:02 +0200 CEST
        LastTimestamp:2023-08-20 13:41:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.Received.1,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:c5dd4c35-8aae-495f-8d0d-b3dd8dca62c0,ResourceVersion:7791522,Generation:0,CreationTimestamp:2023-08-20 13:41:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:abfca65f-300f-4df0-909e-6367dc6c7962,APIVersion:v1,ResourceVersion:7791053,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:40:55.88247873Z","data":{"msg":"TestFlowParallel 8d7380e5-6a9a-44bf-bcea-af974aa808e2"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-1-filter.eventing-e2e6.svc.cluster.local"],"Kn-Namespace":["eventing-e2e6"],"Prefer":["reply"],"Traceparent":["00-05ae47c09d1a5ce6c4fd21adca2e4b98-4219f43a44b6b17b-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:58532","observer":"parallel-two-branches-pass-first-branch-only-branch-1-filter","time":"2023-08-20T11:41:02.226185186Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:02 +0200 CEST
        LastTimestamp:2023-08-20 13:41:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.Received.1,GenerateName:,Namespace:eventing-e2e6,SelfLink:,UID:2b9c52e9-244c-4248-9339-f603fda41599,ResourceVersion:7791526,Generation:0,CreationTimestamp:2023-08-20 13:41:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e6,Name:two-branches-pass-first-branch-only-event-record-pod,UID:f65d0f2a-6306-4ec8-9684-6bfaa109d07d,APIVersion:v1,ResourceVersion:7791174,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","time":"2023-08-20T11:40:55.88247873Z","data":"parallel-two-branches-pass-first-branch-only-branch-0-sub"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["57"],"Content-Type":["text/plain"],"Host":["two-branches-pass-first-branch-only-event-record-pod.eventing-e2e6.svc.cluster.local"],"Kn-Namespace":["eventing-e2e6"],"Prefer":["reply"],"Traceparent":["00-05ae47c09d1a5ce6c4fd21adca2e4b98-268583a21ff5e2b2-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:58710","observer":"two-branches-pass-first-branch-only-event-record-pod","time":"2023-08-20T11:41:02.303058992Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:02 +0200 CEST
        LastTimestamp:2023-08-20 13:41:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 40 events seen
    tracker.go:133: Cleaning resource: "e2e-parallel"
    tracker.go:139: Waiting for e2e-parallel to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "reply-two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for reply-two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "reply-two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for reply-two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only-event-record-pod"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only-event-record-pod to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only-event-record-pod"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only-event-record-pod to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-filter to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e6-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e6-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e6-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6"
    tracker.go:139: Waiting for eventing-e2e6 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6"
    tracker.go:139: Waiting for eventing-e2e6 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e6"
    tracker.go:139: Waiting for eventing-e2e6 to be deleted
--- PASS: TestParallel (0.04s)
    --- PASS: TestParallel/InMemoryChannel-messaging.knative.dev/v1 (46.93s)
=== RUN   TestParallelV1
=== RUN   TestParallelV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e7  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e7      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e7  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e7,Namespace:eventing-e2e7,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e7,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e7-eventwatcher  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e7-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e7-eventwatcher  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e7-eventwatcher,Namespace:eventing-e2e7,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e7-eventwatcher,},}
=== PAUSE TestParallelV1/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestParallelV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-0-filter  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:e10584b8-d1e3-4e3b-9169-31c67e4c55b5] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e7,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-0-filter
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-0-sub  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:ae25f7b8-aa45-4a39-9e86-ad3681b3f3ce] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:parallel-two-branches-pass-first-branch-only-branch-0-sub,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e7,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-0-sub
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-1-filter  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:ca2fffab-39ef-426c-a9df-6686a084fce2] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e7,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-1-filter
    creation.go:441: Creating pod &Pod{ObjectMeta:{parallel-two-branches-pass-first-branch-only-branch-1-sub  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:bbea7caa-6f6c-4f90-a1f4-1832777c878a] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:parallel-two-branches-pass-first-branch-only-branch-1-sub,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e7,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service parallel-two-branches-pass-first-branch-only-branch-1-sub
    creation.go:441: Creating pod &Pod{ObjectMeta:{two-branches-pass-first-branch-only-event-record-pod  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:7648edec-913e-4391-a16f-fb05a43b5ec1] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e7,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service two-branches-pass-first-branch-only-event-record-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 20 events
    creation.go:61: Creating channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-reply-two-branches-pass-first-branch-only
    creation.go:115: Creating v1 subscription reply-two-branches-pass-first-branch-only for channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-reply-two-branches-pass-first-branch-only
    creation.go:293: Creating flows parallel &{TypeMeta:{Kind: APIVersion:} ObjectMeta:{Name:two-branches-pass-first-branch-only GenerateName: Namespace:eventing-e2e7 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{Branches:[{Filter:0xc000664a98 Subscriber:{Ref:Kind = Service, Namespace = eventing-e2e7, Name = parallel-two-branches-pass-first-branch-only-branch-0-sub, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Reply:<nil> Delivery:<nil>} {Filter:0xc000aee0d8 Subscriber:{Ref:Kind = Service, Namespace = eventing-e2e7, Name = parallel-two-branches-pass-first-branch-only-branch-1-sub, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Reply:<nil> Delivery:<nil>}] ChannelTemplate:&TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,} Reply:0xc000c2d500} Status:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} IngressChannelStatus:{Channel:{Kind: Namespace: Name: UID: APIVersion: ResourceVersion: FieldPath:} ReadyCondition:{Type: Status: Severity: LastTransitionTime:{Inner:0001-01-01 00:00:00 +0000 UTC} Reason: Message:}} BranchStatuses:[] AddressStatus:{Address:<nil> Addresses:[]}}}
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-parallel  eventing-e2e7    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://two-branches-pass-first-branch-only-kn-parallel-kn-channel.eventing-e2e7.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:41:45.132356158Z","data_base64":"eyJtc2ciOiJUZXN0Rmxvd1BhcmFsbGVsIGI5YTZlZjhjLTU3MmEtNGE0ZS04YjhkLWJjOGM4Yjg3M2I0NCJ9"}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    parallel_test_helper.go:267: Assert passed
    parallel_test_helper.go:267: Assert passed
    parallel_test_helper.go:267: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d14986a89c369,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:fd25d17c-237f-4c97-b8cf-cd0cda2303ed,ResourceVersion:7791905,Generation:0,CreationTimestamp:2023-08-20 13:41:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:1bbfd494-f32b-4251-a286-0d75a0f001ce,APIVersion:v1,ResourceVersion:7791901,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/parallel-two-branches-pass-first-branch-only-branch-0-filter to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:19 +0200 CEST
        LastTimestamp:2023-08-20 13:41:19 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d149891a46e41,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:3d739f87-909e-4b59-a451-001519795149,ResourceVersion:7791917,Generation:0,CreationTimestamp:2023-08-20 13:41:20 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:20 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:1bbfd494-f32b-4251-a286-0d75a0f001ce,APIVersion:v1,ResourceVersion:7791903,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:20 +0200 CEST
        LastTimestamp:2023-08-20 13:41:20 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d149894b7b23d,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:603dbb23-b9ff-42bd-8dc2-4fd021a01ffc,ResourceVersion:7791922,Generation:0,CreationTimestamp:2023-08-20 13:41:20 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:20 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:1bbfd494-f32b-4251-a286-0d75a0f001ce,APIVersion:v1,ResourceVersion:7791903,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:20 +0200 CEST
        LastTimestamp:2023-08-20 13:41:20 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.177d14989cdaeecf,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:cdf314ee-4b20-4a3e-8e1a-722823033f8b,ResourceVersion:7791926,Generation:0,CreationTimestamp:2023-08-20 13:41:20 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:20 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:1bbfd494-f32b-4251-a286-0d75a0f001ce,APIVersion:v1,ResourceVersion:7791903,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:20 +0200 CEST
        LastTimestamp:2023-08-20 13:41:20 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d1498e4d72143,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:3347bb9d-e884-431d-be5b-286ca6abdbeb,ResourceVersion:7791950,Generation:0,CreationTimestamp:2023-08-20 13:41:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:d4b77937-55a9-4d63-a604-8a8a38ebb8a5,APIVersion:v1,ResourceVersion:7791946,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/parallel-two-branches-pass-first-branch-only-branch-0-sub to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:21 +0200 CEST
        LastTimestamp:2023-08-20 13:41:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d14990cacccce,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:9bdafb2c-c8fe-4275-a9dc-f121b29d660d,ResourceVersion:7791968,Generation:0,CreationTimestamp:2023-08-20 13:41:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:d4b77937-55a9-4d63-a604-8a8a38ebb8a5,APIVersion:v1,ResourceVersion:7791949,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:22 +0200 CEST
        LastTimestamp:2023-08-20 13:41:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d14990f0ec13f,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:188315a1-5421-4590-bbc6-b3c00963c591,ResourceVersion:7791972,Generation:0,CreationTimestamp:2023-08-20 13:41:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:d4b77937-55a9-4d63-a604-8a8a38ebb8a5,APIVersion:v1,ResourceVersion:7791949,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:22 +0200 CEST
        LastTimestamp:2023-08-20 13:41:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.177d1499154ec8fb,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:f5e3627c-d122-4ff0-8882-804c9d02fd47,ResourceVersion:7791973,Generation:0,CreationTimestamp:2023-08-20 13:41:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:d4b77937-55a9-4d63-a604-8a8a38ebb8a5,APIVersion:v1,ResourceVersion:7791949,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:22 +0200 CEST
        LastTimestamp:2023-08-20 13:41:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d14995fa2a7f9,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:8162db69-885e-40c0-bb07-0ca75cacfdfb,ResourceVersion:7792013,Generation:0,CreationTimestamp:2023-08-20 13:41:23 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:23 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:96af3406-6412-459d-ab24-3c3eb4715333,APIVersion:v1,ResourceVersion:7792009,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/parallel-two-branches-pass-first-branch-only-branch-1-filter to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:23 +0200 CEST
        LastTimestamp:2023-08-20 13:41:23 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d149986aa2ec8,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:0f9e9baa-16a3-4325-9b22-dcb7cc0bff50,ResourceVersion:7792062,Generation:0,CreationTimestamp:2023-08-20 13:41:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:96af3406-6412-459d-ab24-3c3eb4715333,APIVersion:v1,ResourceVersion:7792011,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:24 +0200 CEST
        LastTimestamp:2023-08-20 13:41:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d14998902fb62,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:1a27b4e9-2036-48a3-922e-49871b487450,ResourceVersion:7792063,Generation:0,CreationTimestamp:2023-08-20 13:41:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:96af3406-6412-459d-ab24-3c3eb4715333,APIVersion:v1,ResourceVersion:7792011,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:24 +0200 CEST
        LastTimestamp:2023-08-20 13:41:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.177d14999300094f,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:bb6e56dc-1fce-4714-9e3d-9eaf60d1c43d,ResourceVersion:7792064,Generation:0,CreationTimestamp:2023-08-20 13:41:24 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:24 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:96af3406-6412-459d-ab24-3c3eb4715333,APIVersion:v1,ResourceVersion:7792011,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:24 +0200 CEST
        LastTimestamp:2023-08-20 13:41:24 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d149a16adf6b5,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:a3c17600-a7f4-4bfa-91a3-f00ee8c07fa9,ResourceVersion:7792119,Generation:0,CreationTimestamp:2023-08-20 13:41:26 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:26 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:f54998c3-db51-4a4d-a38e-62cca394c3e4,APIVersion:v1,ResourceVersion:7792117,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/parallel-two-branches-pass-first-branch-only-branch-1-sub to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:26 +0200 CEST
        LastTimestamp:2023-08-20 13:41:26 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d149a5647f801,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:f47e7f03-96b3-4b62-9df3-1ac7f3df655d,ResourceVersion:7792133,Generation:0,CreationTimestamp:2023-08-20 13:41:28 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:28 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:f54998c3-db51-4a4d-a38e-62cca394c3e4,APIVersion:v1,ResourceVersion:7792118,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:28 +0200 CEST
        LastTimestamp:2023-08-20 13:41:28 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d149a598c4766,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:a0b30914-5292-40fb-adae-c3ef401b5f43,ResourceVersion:7792134,Generation:0,CreationTimestamp:2023-08-20 13:41:28 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:28 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:f54998c3-db51-4a4d-a38e-62cca394c3e4,APIVersion:v1,ResourceVersion:7792118,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:28 +0200 CEST
        LastTimestamp:2023-08-20 13:41:28 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-sub.177d149a6427cf4d,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:533be5d4-c97e-4983-a294-ba4e4b40f234,ResourceVersion:7792135,Generation:0,CreationTimestamp:2023-08-20 13:41:28 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:28 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-sub,UID:f54998c3-db51-4a4d-a38e-62cca394c3e4,APIVersion:v1,ResourceVersion:7792118,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:28 +0200 CEST
        LastTimestamp:2023-08-20 13:41:28 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d149b090e56d3,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:b2b6e711-6287-4177-82c1-7c3066bac73c,ResourceVersion:7792192,Generation:0,CreationTimestamp:2023-08-20 13:41:31 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:31 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-event-record-pod,UID:3e08193f-7068-4d1e-9e86-e8a3a2b296b5,APIVersion:v1,ResourceVersion:7792190,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/two-branches-pass-first-branch-only-event-record-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:31 +0200 CEST
        LastTimestamp:2023-08-20 13:41:31 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d149b396370ce,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:72d23fdd-ae6c-49b6-a85c-93c6ea5082d2,ResourceVersion:7792203,Generation:0,CreationTimestamp:2023-08-20 13:41:31 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:31 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-event-record-pod,UID:3e08193f-7068-4d1e-9e86-e8a3a2b296b5,APIVersion:v1,ResourceVersion:7792191,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:31 +0200 CEST
        LastTimestamp:2023-08-20 13:41:31 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d149b3e54646b,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:e1c5f3ec-9843-4667-8a9f-081d10260861,ResourceVersion:7792204,Generation:0,CreationTimestamp:2023-08-20 13:41:31 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:31 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-event-record-pod,UID:3e08193f-7068-4d1e-9e86-e8a3a2b296b5,APIVersion:v1,ResourceVersion:7792191,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:31 +0200 CEST
        LastTimestamp:2023-08-20 13:41:31 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.177d149b46c9f675,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:6aa212d3-6f5c-4908-a569-ed56220ca264,ResourceVersion:7792205,Generation:0,CreationTimestamp:2023-08-20 13:41:32 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:32 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-event-record-pod,UID:3e08193f-7068-4d1e-9e86-e8a3a2b296b5,APIVersion:v1,ResourceVersion:7792191,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:32 +0200 CEST
        LastTimestamp:2023-08-20 13:41:32 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:reply-two-branches-pass-first-branch-only.177d149bbe8c795b,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:08989c1f-17d3-4218-9a6e-d17884fa0b99,ResourceVersion:7792237,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:reply-two-branches-pass-first-branch-only,UID:0af80a55-75f1-4639-8a48-08bd1484281f,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792236,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "reply-two-branches-pass-first-branch-only" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:reply-two-branches-pass-first-branch-only.177d149bc1972f76,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:21442e46-1c96-4b9d-9862-4af687be2db7,ResourceVersion:7792249,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:reply-two-branches-pass-first-branch-only,UID:0af80a55-75f1-4639-8a48-08bd1484281f,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792236,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "reply-two-branches-pass-first-branch-only"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-0.177d149bc58db9f8,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:f5709150-7276-4fbf-ac22-29c26d74643f,ResourceVersion:7792262,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-0,UID:9e2c455f-c817-4ca1-9d21-0ae023fe282e,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792259,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-0.177d149bc974af12,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:66b75194-345a-4769-b235-ece625dd781f,ResourceVersion:7792273,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-0,UID:9e2c455f-c817-4ca1-9d21-0ae023fe282e,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792259,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel-0"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-1.177d149bca6226fc,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:4311e745-e897-47e3-8fb0-03b56eccbc4a,ResourceVersion:7792277,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-1,UID:a1fa2734-bf36-4182-8fe4-25a447e9069d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792274,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-1.177d149bd34a4378,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:1ac7de97-bd87-4e9f-9b5c-b2372aad3345,ResourceVersion:7792291,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-1,UID:a1fa2734-bf36-4182-8fe4-25a447e9069d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792274,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel-1"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-1.177d149bd8817858,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:f2ecba26-57c2-47c2-8836-d348606f3bf7,ResourceVersion:7792307,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-1,UID:a1fa2734-bf36-4182-8fe4-25a447e9069d,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792289,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "two-branches-pass-first-branch-only-kn-parallel-1" not present in channel "two-branches-pass-first-branch-only-kn-parallel-1" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d149bc2d9907f,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:1ea04326-9a55-4c9f-8de1-76a4094a7314,ResourceVersion:7792254,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:bc59073e-c6ac-40fe-a07b-b9e8926f8128,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792251,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-filter-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d149bc7a78f60,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:57ba4eea-2e0e-4034-ab7a-ac611cd00e6a,ResourceVersion:7792266,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:bc59073e-c6ac-40fe-a07b-b9e8926f8128,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792251,FieldPath:,}
        Reason:ReplyResolveFailed
        Message:Failed to resolve spec.reply: address not set for Kind = InMemoryChannel, Namespace = eventing-e2e7, Name = two-branches-pass-first-branch-only-kn-parallel-0, APIVersion = messaging.knative.dev/v1, Group = , Address =
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d149bd2531538,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:365b4d42-9870-48eb-8349-3e3f000a25a2,ResourceVersion:7792288,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:bc59073e-c6ac-40fe-a07b-b9e8926f8128,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792264,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-0.177d149bd9b4ce27,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:02c42897-daec-4a2c-8834-f513719f3b03,ResourceVersion:7792335,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-0,UID:bc59073e-c6ac-40fe-a07b-b9e8926f8128,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792297,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "two-branches-pass-first-branch-only-kn-parallel-filter-0" not present in channel "two-branches-pass-first-branch-only-kn-parallel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:2
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d149bc8b99e1d,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:d27c9721-aeba-46d5-a915-ec90c9bbcf5f,ResourceVersion:7792270,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:301505ce-c0e9-4b9e-a0df-a3fd54420dac,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792268,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "two-branches-pass-first-branch-only-kn-parallel-filter-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d149bcc564b51,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:7d59c7d2-3918-43f8-a1db-cb7d10cf8884,ResourceVersion:7792282,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:301505ce-c0e9-4b9e-a0df-a3fd54420dac,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792268,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "two-branches-pass-first-branch-only-kn-parallel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-kn-parallel-filter-1.177d149bd923170f,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:b96939c3-d190-45d2-b74b-e0a9d11dbee1,ResourceVersion:7792334,Generation:0,CreationTimestamp:2023-08-20 13:41:34 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:34 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-kn-parallel-filter-1,UID:301505ce-c0e9-4b9e-a0df-a3fd54420dac,APIVersion:messaging.knative.dev/v1,ResourceVersion:7792295,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "two-branches-pass-first-branch-only-kn-parallel-filter-1" not present in channel "two-branches-pass-first-branch-only-kn-parallel" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:41:34 +0200 CEST
        LastTimestamp:2023-08-20 13:41:34 +0200 CEST
        Count:3
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d149e50cbc23c,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:895bb70b-1df9-4a0e-886c-d37e464ab4c2,ResourceVersion:7792485,Generation:0,CreationTimestamp:2023-08-20 13:41:45 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:45 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:e2e-parallel,UID:6ddeffb7-7099-4ff4-a97c-951ac9d5dcd6,APIVersion:v1,ResourceVersion:7792483,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e7/e2e-parallel to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:41:45 +0200 CEST
        LastTimestamp:2023-08-20 13:41:45 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d149e84169477,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:86b9652e-f148-4b77-942c-dac1e2ce629e,ResourceVersion:7792501,Generation:0,CreationTimestamp:2023-08-20 13:41:46 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:46 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:e2e-parallel,UID:6ddeffb7-7099-4ff4-a97c-951ac9d5dcd6,APIVersion:v1,ResourceVersion:7792484,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:46 +0200 CEST
        LastTimestamp:2023-08-20 13:41:46 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d149e87e471ec,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:6b9db9d8-cea1-4c77-999c-a4d08c623e52,ResourceVersion:7792513,Generation:0,CreationTimestamp:2023-08-20 13:41:46 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:46 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:e2e-parallel,UID:6ddeffb7-7099-4ff4-a97c-951ac9d5dcd6,APIVersion:v1,ResourceVersion:7792484,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:46 +0200 CEST
        LastTimestamp:2023-08-20 13:41:46 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-parallel.177d149e8fc66071,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:41e6e6b4-54fd-42b8-a1fe-265631d27b20,ResourceVersion:7792515,Generation:0,CreationTimestamp:2023-08-20 13:41:46 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:46 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:e2e-parallel,UID:6ddeffb7-7099-4ff4-a97c-951ac9d5dcd6,APIVersion:v1,ResourceVersion:7792484,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:41:46 +0200 CEST
        LastTimestamp:2023-08-20 13:41:46 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-filter.Received.1,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:102adec8-1064-4821-804d-0bec8548a252,ResourceVersion:7792587,Generation:0,CreationTimestamp:2023-08-20 13:41:51 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:51 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-filter,UID:1bbfd494-f32b-4251-a286-0d75a0f001ce,APIVersion:v1,ResourceVersion:7791906,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:41:45.132356158Z","data":{"msg":"TestFlowParallel b9a6ef8c-572a-4a4e-8b8d-bc8c8b873b44"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-0-filter.eventing-e2e7.svc.cluster.local"],"Kn-Namespace":["eventing-e2e7"],"Prefer":["reply"],"Traceparent":["00-394de52d2633130383c7a1089f1d2ed7-72fe5d1abeb448c5-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:49240","observer":"parallel-two-branches-pass-first-branch-only-branch-0-filter","time":"2023-08-20T11:41:51.601478236Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:51 +0200 CEST
        LastTimestamp:2023-08-20 13:41:51 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-0-sub.Received.1,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:e9ef9352-39e6-46ee-8eb9-8e50e0368831,ResourceVersion:7792589,Generation:0,CreationTimestamp:2023-08-20 13:41:51 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:51 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-0-sub,UID:d4b77937-55a9-4d63-a604-8a8a38ebb8a5,APIVersion:v1,ResourceVersion:7791974,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:41:45.132356158Z","data":{"msg":"TestFlowParallel b9a6ef8c-572a-4a4e-8b8d-bc8c8b873b44"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-0-sub.eventing-e2e7.svc.cluster.local"],"Kn-Namespace":["eventing-e2e7"],"Prefer":["reply"],"Traceparent":["00-394de52d2633130383c7a1089f1d2ed7-64b425ce2b54e1e0-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:44752","observer":"parallel-two-branches-pass-first-branch-only-branch-0-sub","time":"2023-08-20T11:41:51.639245966Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:51 +0200 CEST
        LastTimestamp:2023-08-20 13:41:51 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:parallel-two-branches-pass-first-branch-only-branch-1-filter.Received.1,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:d2ff5726-b909-41da-ab7d-066eb477105e,ResourceVersion:7792586,Generation:0,CreationTimestamp:2023-08-20 13:41:51 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:51 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:parallel-two-branches-pass-first-branch-only-branch-1-filter,UID:96af3406-6412-459d-ab24-3c3eb4715333,APIVersion:v1,ResourceVersion:7792016,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"application/json","time":"2023-08-20T11:41:45.132356158Z","data":{"msg":"TestFlowParallel b9a6ef8c-572a-4a4e-8b8d-bc8c8b873b44"}},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["63"],"Content-Type":["application/json"],"Host":["parallel-two-branches-pass-first-branch-only-branch-1-filter.eventing-e2e7.svc.cluster.local"],"Kn-Namespace":["eventing-e2e7"],"Prefer":["reply"],"Traceparent":["00-394de52d2633130383c7a1089f1d2ed7-c59c54b8a524e249-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:46694","observer":"parallel-two-branches-pass-first-branch-only-branch-1-filter","time":"2023-08-20T11:41:51.60141346Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:51 +0200 CEST
        LastTimestamp:2023-08-20 13:41:51 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:two-branches-pass-first-branch-only-event-record-pod.Received.1,GenerateName:,Namespace:eventing-e2e7,SelfLink:,UID:e67cb78b-b46d-4f2d-b637-38192c207086,ResourceVersion:7792590,Generation:0,CreationTimestamp:2023-08-20 13:41:51 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:41:51 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e7,Name:two-branches-pass-first-branch-only-event-record-pod,UID:3e08193f-7068-4d1e-9e86-e8a3a2b296b5,APIVersion:v1,ResourceVersion:7792206,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-parallel.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","time":"2023-08-20T11:41:45.132356158Z","data":"parallel-two-branches-pass-first-branch-only-branch-0-sub"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["57"],"Content-Type":["text/plain"],"Host":["two-branches-pass-first-branch-only-event-record-pod.eventing-e2e7.svc.cluster.local"],"Kn-Namespace":["eventing-e2e7"],"Prefer":["reply"],"Traceparent":["00-394de52d2633130383c7a1089f1d2ed7-03ccf6e3b183e077-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:53098","observer":"two-branches-pass-first-branch-only-event-record-pod","time":"2023-08-20T11:41:51.674718432Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:41:51 +0200 CEST
        LastTimestamp:2023-08-20 13:41:51 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 42 events seen
    tracker.go:133: Cleaning resource: "e2e-parallel"
    tracker.go:139: Waiting for e2e-parallel to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "reply-two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for reply-two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "reply-two-branches-pass-first-branch-only"
    tracker.go:139: Waiting for reply-two-branches-pass-first-branch-only to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only-event-record-pod"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only-event-record-pod to be deleted
    tracker.go:133: Cleaning resource: "two-branches-pass-first-branch-only-event-record-pod"
    tracker.go:139: Waiting for two-branches-pass-first-branch-only-event-record-pod to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-1-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-1-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-sub"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-sub to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-filter to be deleted
    tracker.go:133: Cleaning resource: "parallel-two-branches-pass-first-branch-only-branch-0-filter"
    tracker.go:139: Waiting for parallel-two-branches-pass-first-branch-only-branch-0-filter to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e7-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e7-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e7-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7"
    tracker.go:139: Waiting for eventing-e2e7 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7"
    tracker.go:139: Waiting for eventing-e2e7 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e7"
    tracker.go:139: Waiting for eventing-e2e7 to be deleted
--- PASS: TestParallelV1 (1.04s)
    --- PASS: TestParallelV1/InMemoryChannel-messaging.knative.dev/v1 (49.55s)
=== RUN   TestSequence
=== RUN   TestSequence/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e8  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e8      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e8  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e8,Namespace:eventing-e2e8,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e8,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e8-eventwatcher  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e8-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e8-eventwatcher  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e8-eventwatcher,Namespace:eventing-e2e8,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e8-eventwatcher,},}
=== PAUSE TestSequence/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestSequence/InMemoryChannel-messaging.knative.dev/v1
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper1  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:eafc5789-6ffd-4a61-bd5d-37d1b1d46645] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step1,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e8,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper1
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper2  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:db2b3c17-9a05-4089-890a-a0dfb29ed270] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step2,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e8,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper2
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper3  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:29eb884c-0df4-4f21-b48d-aa5df2cfaced] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step3,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e8,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper3
    creation.go:61: Creating channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-sequence-channel
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-sequence-recordevents-pod  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:25ef6513-abf8-4375-85ee-57eab7030469] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e8,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-sequence-recordevents-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 20 events
    creation.go:115: Creating v1 subscription e2e-sequence-subscription for channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-sequence-channel
    creation.go:275: Creating flows sequence &{TypeMeta:{Kind: APIVersion:} ObjectMeta:{Name:e2e-sequence GenerateName: Namespace:eventing-e2e8 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{Steps:[{Destination:{Ref:Kind = Service, Namespace = eventing-e2e8, Name = e2e-stepper1, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>} {Destination:{Ref:Kind = Service, Namespace = eventing-e2e8, Name = e2e-stepper2, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>} {Destination:{Ref:Kind = Service, Namespace = eventing-e2e8, Name = e2e-stepper3, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>}] ChannelTemplate:&TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,} Reply:0xc000aaa348} Status:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} SubscriptionStatuses:[] ChannelStatuses:[] Address:{Name:<nil> URL: CACerts:<nil>}}}
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-sequence-sender-pod  eventing-e2e8    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://e2e-sequence-kn-sequence-0-kn-channel.eventing-e2e8.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 6e809a46-7b8e-43f3-8d88-c7af62872fcc"}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    sequence_test_helper.go:148: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14a3f39c0e67,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:8fc7dfbc-9197-463b-aeb8-092ea6e4788c,ResourceVersion:7792970,Generation:0,CreationTimestamp:2023-08-20 13:42:09 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:09 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792968,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e8/e2e-stepper1 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:09 +0200 CEST
        LastTimestamp:2023-08-20 13:42:09 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14a4191753be,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:39779008-77c2-4236-adad-a75e4c69f2d3,ResourceVersion:7792980,Generation:0,CreationTimestamp:2023-08-20 13:42:09 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:09 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792969,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:09 +0200 CEST
        LastTimestamp:2023-08-20 13:42:09 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14a41b2dec37,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:fbe4a37d-23a3-4071-b706-9ae9b2a99110,ResourceVersion:7792981,Generation:0,CreationTimestamp:2023-08-20 13:42:10 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:10 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792969,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:10 +0200 CEST
        LastTimestamp:2023-08-20 13:42:10 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14a422108d62,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:cf6813e0-1df4-45a5-815f-08dcd2887468,ResourceVersion:7792982,Generation:0,CreationTimestamp:2023-08-20 13:42:10 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:10 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792969,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:10 +0200 CEST
        LastTimestamp:2023-08-20 13:42:10 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14a432b97f49,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:4bfe47f5-8953-47c0-8349-85c0aebcf9fa,ResourceVersion:7792983,Generation:0,CreationTimestamp:2023-08-20 13:42:10 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:10 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792969,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.55:8080/healthz": dial tcp 10.1.34.55:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:10 +0200 CEST
        LastTimestamp:2023-08-20 13:42:10 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14a4a8928aa0,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:1d0713d8-3098-4353-b6ee-5b565b36400e,ResourceVersion:7793021,Generation:0,CreationTimestamp:2023-08-20 13:42:12 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:12 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793019,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e8/e2e-stepper2 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:12 +0200 CEST
        LastTimestamp:2023-08-20 13:42:12 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14a4d7010d5a,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:e90919c7-f2c4-4f58-a3dd-7d892971cd66,ResourceVersion:7793038,Generation:0,CreationTimestamp:2023-08-20 13:42:13 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:13 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793020,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:13 +0200 CEST
        LastTimestamp:2023-08-20 13:42:13 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14a4daf3098a,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:7d852e17-2209-412d-b249-2f993a89a336,ResourceVersion:7793039,Generation:0,CreationTimestamp:2023-08-20 13:42:13 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:13 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793020,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:13 +0200 CEST
        LastTimestamp:2023-08-20 13:42:13 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14a4e38eed6f,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:b520f437-b1bd-4528-8675-da29e02e1daa,ResourceVersion:7793055,Generation:0,CreationTimestamp:2023-08-20 13:42:13 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:13 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793020,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:13 +0200 CEST
        LastTimestamp:2023-08-20 13:42:13 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14a4ea4ec436,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:7ace7abf-6a86-4c35-9b96-1addee239609,ResourceVersion:7793079,Generation:0,CreationTimestamp:2023-08-20 13:42:13 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:13 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793020,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.56:8080/healthz": dial tcp 10.1.34.56:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:13 +0200 CEST
        LastTimestamp:2023-08-20 13:42:13 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14a55d8f42df,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:b8ceace4-a775-4322-a69a-d4e546950c03,ResourceVersion:7793132,Generation:0,CreationTimestamp:2023-08-20 13:42:15 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:15 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793130,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e8/e2e-stepper3 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:15 +0200 CEST
        LastTimestamp:2023-08-20 13:42:15 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14a58a49ebeb,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:a93e5f14-b247-4375-a4a8-ee1890ea898a,ResourceVersion:7793160,Generation:0,CreationTimestamp:2023-08-20 13:42:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793131,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:16 +0200 CEST
        LastTimestamp:2023-08-20 13:42:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14a58e35ac3d,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:9381ce75-309a-4094-89ae-a9bbf8e79c88,ResourceVersion:7793162,Generation:0,CreationTimestamp:2023-08-20 13:42:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793131,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:16 +0200 CEST
        LastTimestamp:2023-08-20 13:42:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14a595d56de6,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:a5504db2-a771-452f-b3c8-0d08f063c773,ResourceVersion:7793163,Generation:0,CreationTimestamp:2023-08-20 13:42:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793131,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:16 +0200 CEST
        LastTimestamp:2023-08-20 13:42:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14a5a2de612e,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:8669d73b-db04-4c22-924f-9aa7a4e4a3ba,ResourceVersion:7793164,Generation:0,CreationTimestamp:2023-08-20 13:42:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793131,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.57:8080/healthz": dial tcp 10.1.34.57:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:16 +0200 CEST
        LastTimestamp:2023-08-20 13:42:16 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14a612f7e0bc,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:66464ee7-fe62-4b82-8859-2e98d8d53516,ResourceVersion:7793209,Generation:0,CreationTimestamp:2023-08-20 13:42:18 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:18 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793207,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e8/e2e-sequence-recordevents-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:18 +0200 CEST
        LastTimestamp:2023-08-20 13:42:18 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14a63f39ac2f,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:21234a98-e4c2-4225-9bb7-43b595daee33,ResourceVersion:7793229,Generation:0,CreationTimestamp:2023-08-20 13:42:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793208,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:19 +0200 CEST
        LastTimestamp:2023-08-20 13:42:19 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14a64386c474,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:e269061c-1ab0-4ef0-a30b-fe48d595a090,ResourceVersion:7793230,Generation:0,CreationTimestamp:2023-08-20 13:42:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793208,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:19 +0200 CEST
        LastTimestamp:2023-08-20 13:42:19 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14a64eab8da0,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:44f89ba9-d39d-47a5-8070-06fd53fd0763,ResourceVersion:7793231,Generation:0,CreationTimestamp:2023-08-20 13:42:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793208,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:19 +0200 CEST
        LastTimestamp:2023-08-20 13:42:19 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14a659ddfcd5,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:38037ca0-c87f-46cc-8ae1-afb2f4fc01c7,ResourceVersion:7793232,Generation:0,CreationTimestamp:2023-08-20 13:42:19 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:19 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793208,FieldPath:spec.containers{recordevents},}
        Reason:Unhealthy
        Message:Readiness probe failed: Get "http://10.1.34.58:8080/healthz": dial tcp 10.1.34.58:8080: connect: connection refused
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:19 +0200 CEST
        LastTimestamp:2023-08-20 13:42:19 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-0.177d14a6cb5ddfb2,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:df4197bf-aef6-4804-b786-bbb594d606e3,ResourceVersion:7793286,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-0,UID:ad6e5f78-1834-4977-9fb9-8dbf5511aa55,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793283,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-0.177d14a6d25457c6,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:b6d57c24-bf3a-400d-8677-a6f57491b5d3,ResourceVersion:7793300,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-0,UID:ad6e5f78-1834-4977-9fb9-8dbf5511aa55,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793283,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-0"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-1.177d14a6cf473ebf,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:435b7a62-2f75-4165-981a-70e0b7b37331,ResourceVersion:7793294,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-1,UID:12c25f75-421d-4aa3-b9cb-a70af85ed4ae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793292,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-1.177d14a6d4f34b9c,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:4735585b-6802-48e9-abdf-f03e115e5319,ResourceVersion:7793308,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-1,UID:12c25f75-421d-4aa3-b9cb-a70af85ed4ae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793292,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-1"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-1.177d14a6d95dc35c,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:b11ca9ba-7526-4985-925b-039e90044c3e,ResourceVersion:7793321,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-1,UID:12c25f75-421d-4aa3-b9cb-a70af85ed4ae,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793307,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-sequence-kn-sequence-1" not present in channel "e2e-sequence-kn-sequence-1" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14a6d40b900e,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:7e3e8ab1-4390-481b-81ee-d44593f5f087,ResourceVersion:7793305,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-2,UID:e07f3cfe-f524-48dd-8ebc-2385a7188ca6,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793302,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-2" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14a6d7b4cd43,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:9c8bdf71-18c7-402b-8992-2337b35348cb,ResourceVersion:7793314,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-2,UID:e07f3cfe-f524-48dd-8ebc-2385a7188ca6,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793302,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-2"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14a6db87f831,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:1f75e704-4ec3-444c-af9d-0ebd12cf37e2,ResourceVersion:7793326,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-kn-sequence-2,UID:e07f3cfe-f524-48dd-8ebc-2385a7188ca6,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793313,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-sequence-kn-sequence-2" not present in channel "e2e-sequence-kn-sequence-2" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-subscription.177d14a6c6eea169,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:5b172181-fbee-43d0-8b90-9d598787e712,ResourceVersion:7793268,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-subscription,UID:7101ae76-90b9-407a-8510-09d3bd42399a,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793267,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-subscription" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-subscription.177d14a6ca1ca397,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:22f2a9f6-f69b-4f50-93f4-c8d8f071be18,ResourceVersion:7793279,Generation:0,CreationTimestamp:2023-08-20 13:42:21 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:21 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e8,Name:e2e-sequence-subscription,UID:7101ae76-90b9-407a-8510-09d3bd42399a,APIVersion:messaging.knative.dev/v1,ResourceVersion:7793267,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-channel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:42:21 +0200 CEST
        LastTimestamp:2023-08-20 13:42:21 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14a958b82924,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:a940be38-2338-4fe5-aeac-411292cee81c,ResourceVersion:7793479,Generation:0,CreationTimestamp:2023-08-20 13:42:32 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:32 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-sender-pod,UID:5c66f094-68f0-4689-8f6c-4c4d47c2b192,APIVersion:v1,ResourceVersion:7793477,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e8/e2e-sequence-sender-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:32 +0200 CEST
        LastTimestamp:2023-08-20 13:42:32 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14a983862420,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:0a95aa7d-c048-4a65-bb2f-04c7eb95fab4,ResourceVersion:7793496,Generation:0,CreationTimestamp:2023-08-20 13:42:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-sender-pod,UID:5c66f094-68f0-4689-8f6c-4c4d47c2b192,APIVersion:v1,ResourceVersion:7793478,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:33 +0200 CEST
        LastTimestamp:2023-08-20 13:42:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14a985cb32b5,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:dbdd91fc-2e0e-4b85-943e-719d32d06c4a,ResourceVersion:7793497,Generation:0,CreationTimestamp:2023-08-20 13:42:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-sender-pod,UID:5c66f094-68f0-4689-8f6c-4c4d47c2b192,APIVersion:v1,ResourceVersion:7793478,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:33 +0200 CEST
        LastTimestamp:2023-08-20 13:42:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14a98c7e4f94,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:561e1abd-7d10-4d14-b3b9-600951c16718,ResourceVersion:7793498,Generation:0,CreationTimestamp:2023-08-20 13:42:33 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:33 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-sender-pod,UID:5c66f094-68f0-4689-8f6c-4c4d47c2b192,APIVersion:v1,ResourceVersion:7793478,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:33 +0200 CEST
        LastTimestamp:2023-08-20 13:42:33 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.Received.1,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:1b874c14-dea3-4bdc-ada5-dcbf8930029d,ResourceVersion:7793582,Generation:0,CreationTimestamp:2023-08-20 13:42:38 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:38 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-sequence-recordevents-pod,UID:fa88669b-24d9-4478-8b44-5991830d26ad,APIVersion:v1,ResourceVersion:7793233,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 6e809a46-7b8e-43f3-8d88-c7af62872fcc-step1-step2-step3"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["67"],"Content-Type":["text/plain"],"Host":["e2e-sequence-recordevents-pod.eventing-e2e8.svc.cluster.local"],"Kn-Namespace":["eventing-e2e8"],"Prefer":["reply"],"Traceparent":["00-a56b8c671abbcb632a6e1e4a4a8557cd-d9ed4dfffa61aaca-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:60482","observer":"e2e-sequence-recordevents-pod","time":"2023-08-20T11:42:38.882229317Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:42:38 +0200 CEST
        LastTimestamp:2023-08-20 13:42:38 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.Received.1,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:de1d844e-e214-4512-818b-b4db18444da9,ResourceVersion:7793576,Generation:0,CreationTimestamp:2023-08-20 13:42:38 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:38 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper1,UID:30ac3ed1-7903-432a-88d4-8b8410f61ea8,APIVersion:v1,ResourceVersion:7792984,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 6e809a46-7b8e-43f3-8d88-c7af62872fcc"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["49"],"Content-Type":["text/plain"],"Host":["e2e-stepper1.eventing-e2e8.svc.cluster.local"],"Kn-Namespace":["eventing-e2e8"],"Prefer":["reply"],"Traceparent":["00-a56b8c671abbcb632a6e1e4a4a8557cd-fca6dabd68d3ac05-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:55572","observer":"e2e-stepper1","time":"2023-08-20T11:42:38.775938035Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:42:38 +0200 CEST
        LastTimestamp:2023-08-20 13:42:38 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.Received.1,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:81a1fe9f-e2fb-4cf1-afee-3ff7c409d6be,ResourceVersion:7793577,Generation:0,CreationTimestamp:2023-08-20 13:42:38 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:38 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper2,UID:82601c99-6110-4a2e-844a-3cbebefd00a8,APIVersion:v1,ResourceVersion:7793081,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 6e809a46-7b8e-43f3-8d88-c7af62872fcc-step1"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["55"],"Content-Type":["text/plain"],"Host":["e2e-stepper2.eventing-e2e8.svc.cluster.local"],"Kn-Namespace":["eventing-e2e8"],"Prefer":["reply"],"Traceparent":["00-a56b8c671abbcb632a6e1e4a4a8557cd-9bbeabd3ee02ac9c-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:51530","observer":"e2e-stepper2","time":"2023-08-20T11:42:38.808144365Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:42:38 +0200 CEST
        LastTimestamp:2023-08-20 13:42:38 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.Received.1,GenerateName:,Namespace:eventing-e2e8,SelfLink:,UID:022e158e-b644-47a8-b494-9f628c5db320,ResourceVersion:7793578,Generation:0,CreationTimestamp:2023-08-20 13:42:38 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:38 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e8,Name:e2e-stepper3,UID:4001e427-f3d6-4c61-8496-3119fbaddbdb,APIVersion:v1,ResourceVersion:7793165,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 6e809a46-7b8e-43f3-8d88-c7af62872fcc-step1-step2"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["61"],"Content-Type":["text/plain"],"Host":["e2e-stepper3.eventing-e2e8.svc.cluster.local"],"Kn-Namespace":["eventing-e2e8"],"Prefer":["reply"],"Traceparent":["00-a56b8c671abbcb632a6e1e4a4a8557cd-3ad67ce97432ab33-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:53720","observer":"e2e-stepper3","time":"2023-08-20T11:42:38.847471715Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:42:38 +0200 CEST
        LastTimestamp:2023-08-20 13:42:38 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 38 events seen
    tracker.go:133: Cleaning resource: "e2e-sequence-sender-pod"
    tracker.go:139: Waiting for e2e-sequence-sender-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence"
    tracker.go:139: Waiting for e2e-sequence to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-subscription"
    tracker.go:139: Waiting for e2e-sequence-subscription to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-recordevents-pod"
    tracker.go:139: Waiting for e2e-sequence-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-recordevents-pod"
    tracker.go:139: Waiting for e2e-sequence-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-channel"
    tracker.go:139: Waiting for e2e-sequence-channel to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper3"
    tracker.go:139: Waiting for e2e-stepper3 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper3"
    tracker.go:139: Waiting for e2e-stepper3 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper2"
    tracker.go:139: Waiting for e2e-stepper2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper2"
    tracker.go:139: Waiting for e2e-stepper2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper1"
    tracker.go:139: Waiting for e2e-stepper1 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper1"
    tracker.go:139: Waiting for e2e-stepper1 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e8-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e8-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e8-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8"
    tracker.go:139: Waiting for eventing-e2e8 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8"
    tracker.go:139: Waiting for eventing-e2e8 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e8"
    tracker.go:139: Waiting for eventing-e2e8 to be deleted
--- PASS: TestSequence (1.04s)
    --- PASS: TestSequence/InMemoryChannel-messaging.knative.dev/v1 (44.68s)
=== RUN   TestSequenceV1
=== RUN   TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e9  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e9      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get],APIGroups:[],Resources:[pods],ResourceNames:[],NonResourceURLs:[],},PolicyRule{Verbs:[*],APIGroups:[],Resources:[events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e9  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e9,Namespace:eventing-e2e9,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e9,},}
    creation.go:504: Creating service account &ServiceAccount{ObjectMeta:{eventing-e2e9-eventwatcher  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Secrets:[]ObjectReference{},ImagePullSecrets:[]LocalObjectReference{},AutomountServiceAccountToken:nil,}
    creation.go:532: Creating role &Role{ObjectMeta:{eventing-e2e9-eventwatcher      0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Rules:[]PolicyRule{PolicyRule{Verbs:[get list watch],APIGroups:[],Resources:[pods events],ResourceNames:[],NonResourceURLs:[],},},}
    creation.go:551: Creating role binding &RoleBinding{ObjectMeta:{eventing-e2e9-eventwatcher  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Subjects:[]Subject{Subject{Kind:ServiceAccount,APIGroup:,Name:eventing-e2e9-eventwatcher,Namespace:eventing-e2e9,},},RoleRef:RoleRef{APIGroup:rbac.authorization.k8s.io,Kind:Role,Name:eventing-e2e9-eventwatcher,},}
=== PAUSE TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1
=== CONT  TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper1  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:d43a8c34-d93b-439d-b7f5-aa8c9bcb6582] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step1,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e9,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper1
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper2  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:63a90f0a-cb7b-499f-89da-620114f10a3d] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step2,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e9,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper2
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-stepper3  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:d4fa3659-ebb1-4d43-8583-759902173674] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:REPLY,Value:true,ValueFrom:nil,},EnvVar{Name:REPLY_APPEND_DATA,Value:-step3,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e9,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-stepper3
    creation.go:61: Creating channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-sequence-channel
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-sequence-recordevents-pod  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[e2etest:f69cd6eb-3125-463f-b4d2-1801e4189ecb] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:recordevents,Image:registrykn.szymonmusial.eu.org/recordevents:latest,Command:[],Args:[],WorkingDir:,Ports:[]ContainerPort{ContainerPort{Name:receive,HostPort:0,ContainerPort:8080,Protocol:,HostIP:,},},Env:[]EnvVar{EnvVar{Name:SYSTEM_NAMESPACE,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.namespace,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:POD_NAME,Value:,ValueFrom:&EnvVarSource{FieldRef:&ObjectFieldSelector{APIVersion:,FieldPath:metadata.name,},ResourceFieldRef:nil,ConfigMapKeyRef:nil,SecretKeyRef:nil,},},EnvVar{Name:EVENT_LOGS,Value:recorder,logger,ValueFrom:nil,},EnvVar{Name:EVENT_GENERATORS,Value:receiver,ValueFrom:nil,},EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:&Probe{ProbeHandler:ProbeHandler{Exec:nil,HTTPGet:&HTTPGetAction{Path:/healthz,Port:{1 0 receive},Host:,Scheme:,HTTPHeaders:[]HTTPHeader{},},TCPSocket:nil,GRPC:nil,},InitialDelaySeconds:0,TimeoutSeconds:0,PeriodSeconds:0,SuccessThreshold:0,FailureThreshold:0,TerminationGracePeriodSeconds:nil,},Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Always,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:eventing-e2e9,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    operation.go:128: Waiting for 1 endpoints in service e2e-sequence-recordevents-pod
    event_info_store.go:80: EventInfoStore added to the EventListener, which has already seen 16 events
    creation.go:115: Creating v1 subscription e2e-sequence-subscription for channel &TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,}-e2e-sequence-channel
    creation.go:275: Creating flows sequence &{TypeMeta:{Kind: APIVersion:} ObjectMeta:{Name:e2e-sequence GenerateName: Namespace:eventing-e2e9 SelfLink: UID: ResourceVersion: Generation:0 CreationTimestamp:0001-01-01 00:00:00 +0000 UTC DeletionTimestamp:<nil> DeletionGracePeriodSeconds:<nil> Labels:map[] Annotations:map[] OwnerReferences:[] Finalizers:[] ManagedFields:[]} Spec:{Steps:[{Destination:{Ref:Kind = Service, Namespace = eventing-e2e9, Name = e2e-stepper1, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>} {Destination:{Ref:Kind = Service, Namespace = eventing-e2e9, Name = e2e-stepper2, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>} {Destination:{Ref:Kind = Service, Namespace = eventing-e2e9, Name = e2e-stepper3, APIVersion = v1, Group = , Address =  URI: CACerts:<nil>} Delivery:<nil>}] ChannelTemplate:&TypeMeta{Kind:InMemoryChannel,APIVersion:messaging.knative.dev/v1,} Reply:0xc00080a1b0} Status:{Status:{ObservedGeneration:0 Conditions:[] Annotations:map[]} SubscriptionStatuses:[] ChannelStatuses:[] Address:{Name:<nil> URL: CACerts:<nil>}}}
    tracker.go:155: Waiting for all KResources to become ready
    creation.go:441: Creating pod &Pod{ObjectMeta:{e2e-sequence-sender-pod  eventing-e2e9    0 0001-01-01 00:00:00 +0000 UTC <nil> <nil> map[] map[] [] [] []},Spec:PodSpec{Volumes:[]Volume{},Containers:[]Container{Container{Name:event-sender,Image:registrykn.szymonmusial.eu.org/event-sender:latest,Command:[],Args:[-sink http://e2e-sequence-kn-sequence-0-kn-channel.eventing-e2e9.svc.cluster.local -event {"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 1e8d17e6-92b7-4cd0-bf45-08a64ef9f622"}],WorkingDir:,Ports:[]ContainerPort{},Env:[]EnvVar{EnvVar{Name:K_CONFIG_TRACING,Value:{"backend":"zipkin","debug":"true","sample-rate":"1","zipkin-endpoint":"http://my-collector-collector.observability.svc:9411/api/v2/spans"},ValueFrom:nil,},EnvVar{Name:K_CONFIG_LOGGING,Value:{"zap-logger-config":"{\n  \"level\": \"info\",\n  \"development\": false,\n  \"outputPaths\": [\"stdout\"],\n  \"errorOutputPaths\": [\"stderr\"],\n  \"encoding\": \"json\",\n  \"encoderConfig\": {\n    \"timeKey\": \"ts\",\n    \"levelKey\": \"level\",\n    \"nameKey\": \"logger\",\n    \"callerKey\": \"caller\",\n    \"messageKey\": \"msg\",\n    \"stacktraceKey\": \"stacktrace\",\n    \"lineEnding\": \"\",\n    \"levelEncoder\": \"\",\n    \"timeEncoder\": \"iso8601\",\n    \"durationEncoder\": \"\",\n    \"callerEncoder\": \"\"\n  }\n}\n"},ValueFrom:nil,},},Resources:ResourceRequirements{Limits:ResourceList{},Requests:ResourceList{},Claims:[]ResourceClaim{},},VolumeMounts:[]VolumeMount{},LivenessProbe:nil,ReadinessProbe:nil,Lifecycle:nil,TerminationMessagePath:,ImagePullPolicy:IfNotPresent,SecurityContext:nil,Stdin:false,StdinOnce:false,TTY:false,EnvFrom:[]EnvFromSource{},TerminationMessagePolicy:,VolumeDevices:[]VolumeDevice{},StartupProbe:nil,},},RestartPolicy:Never,TerminationGracePeriodSeconds:nil,ActiveDeadlineSeconds:nil,DNSPolicy:,NodeSelector:map[string]string{},ServiceAccountName:,DeprecatedServiceAccount:,NodeName:,HostNetwork:false,HostPID:false,HostIPC:false,SecurityContext:nil,ImagePullSecrets:[]LocalObjectReference{},Hostname:,Subdomain:,Affinity:nil,SchedulerName:,InitContainers:[]Container{},AutomountServiceAccountToken:nil,Tolerations:[]Toleration{},HostAliases:[]HostAlias{},PriorityClassName:,Priority:nil,DNSConfig:nil,ShareProcessNamespace:nil,ReadinessGates:[]PodReadinessGate{},RuntimeClassName:nil,EnableServiceLinks:nil,PreemptionPolicy:nil,Overhead:ResourceList{},TopologySpreadConstraints:[]TopologySpreadConstraint{},EphemeralContainers:[]EphemeralContainer{},SetHostnameAsFQDN:nil,OS:nil,HostUsers:nil,SchedulingGates:[]PodSchedulingGate{},ResourceClaims:[]PodResourceClaim{},},Status:PodStatus{Phase:,Conditions:[]PodCondition{},Message:,Reason:,HostIP:,PodIP:,StartTime:<nil>,ContainerStatuses:[]ContainerStatus{},QOSClass:,InitContainerStatuses:[]ContainerStatus{},NominatedNodeName:,PodIPs:[]PodIP{},EphemeralContainerStatuses:[]ContainerStatus{},},}
    sequence_test_helper.go:266: Assert passed
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14ae5a81f2ae,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:b02e007c-782d-4577-a884-5b8751981adc,ResourceVersion:7793898,Generation:0,CreationTimestamp:2023-08-20 13:42:54 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:54 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper1,UID:9c29efef-5428-4ee5-abdc-58213f0371d6,APIVersion:v1,ResourceVersion:7793896,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e9/e2e-stepper1 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:54 +0200 CEST
        LastTimestamp:2023-08-20 13:42:54 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14ae809cdb52,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:4bc6bcae-32df-47a2-b2b8-d2346016fcd0,ResourceVersion:7793901,Generation:0,CreationTimestamp:2023-08-20 13:42:54 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:54 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper1,UID:9c29efef-5428-4ee5-abdc-58213f0371d6,APIVersion:v1,ResourceVersion:7793897,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:54 +0200 CEST
        LastTimestamp:2023-08-20 13:42:54 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14ae82af8bc4,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:d217cd09-874e-45dd-9c7f-987b81b875b0,ResourceVersion:7793902,Generation:0,CreationTimestamp:2023-08-20 13:42:54 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:54 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper1,UID:9c29efef-5428-4ee5-abdc-58213f0371d6,APIVersion:v1,ResourceVersion:7793897,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:54 +0200 CEST
        LastTimestamp:2023-08-20 13:42:54 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.177d14ae897c8a60,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:747ac9b3-1964-4d97-8b59-be45053804d8,ResourceVersion:7793904,Generation:0,CreationTimestamp:2023-08-20 13:42:54 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:54 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper1,UID:9c29efef-5428-4ee5-abdc-58213f0371d6,APIVersion:v1,ResourceVersion:7793897,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:54 +0200 CEST
        LastTimestamp:2023-08-20 13:42:54 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14aed35f02e7,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:de96f119-0847-40f9-8533-52449e3c8a55,ResourceVersion:7793942,Generation:0,CreationTimestamp:2023-08-20 13:42:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper2,UID:157d96fb-c7e5-41d6-a939-d6d42ffa9fdd,APIVersion:v1,ResourceVersion:7793939,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e9/e2e-stepper2 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:56 +0200 CEST
        LastTimestamp:2023-08-20 13:42:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14aef9cd68bf,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:5eb9c9d8-c885-40e3-8619-2223924b31ca,ResourceVersion:7793961,Generation:0,CreationTimestamp:2023-08-20 13:42:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper2,UID:157d96fb-c7e5-41d6-a939-d6d42ffa9fdd,APIVersion:v1,ResourceVersion:7793940,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:56 +0200 CEST
        LastTimestamp:2023-08-20 13:42:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14aefbc4b970,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:5f3f04fc-fe9f-4d01-84d0-6208fcea649b,ResourceVersion:7793962,Generation:0,CreationTimestamp:2023-08-20 13:42:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper2,UID:157d96fb-c7e5-41d6-a939-d6d42ffa9fdd,APIVersion:v1,ResourceVersion:7793940,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:56 +0200 CEST
        LastTimestamp:2023-08-20 13:42:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.177d14af02194440,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:751405bc-47d3-4392-bd34-cc9def89baf3,ResourceVersion:7793964,Generation:0,CreationTimestamp:2023-08-20 13:42:56 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:56 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper2,UID:157d96fb-c7e5-41d6-a939-d6d42ffa9fdd,APIVersion:v1,ResourceVersion:7793940,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:56 +0200 CEST
        LastTimestamp:2023-08-20 13:42:56 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14af88db057f,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:f4ca2253-4fce-4d51-a23e-ac5ea57bef42,ResourceVersion:7794072,Generation:0,CreationTimestamp:2023-08-20 13:42:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper3,UID:7f4f525c-3247-429f-a0eb-7f0df6e5dff5,APIVersion:v1,ResourceVersion:7794067,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e9/e2e-stepper3 to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:42:59 +0200 CEST
        LastTimestamp:2023-08-20 13:42:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14afaeb99028,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:847452ff-eaac-4f33-9fa8-7c4e058cfae8,ResourceVersion:7794081,Generation:0,CreationTimestamp:2023-08-20 13:42:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper3,UID:7f4f525c-3247-429f-a0eb-7f0df6e5dff5,APIVersion:v1,ResourceVersion:7794069,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:59 +0200 CEST
        LastTimestamp:2023-08-20 13:42:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14afb0fc4783,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:1c4314c9-a7f1-4cd9-a189-74987234912a,ResourceVersion:7794082,Generation:0,CreationTimestamp:2023-08-20 13:42:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper3,UID:7f4f525c-3247-429f-a0eb-7f0df6e5dff5,APIVersion:v1,ResourceVersion:7794069,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:59 +0200 CEST
        LastTimestamp:2023-08-20 13:42:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.177d14afb7aeb112,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:011ba134-bfbe-455c-bc98-3aefc2833b1c,ResourceVersion:7794088,Generation:0,CreationTimestamp:2023-08-20 13:42:59 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:42:59 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper3,UID:7f4f525c-3247-429f-a0eb-7f0df6e5dff5,APIVersion:v1,ResourceVersion:7794069,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:42:59 +0200 CEST
        LastTimestamp:2023-08-20 13:42:59 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14b03e88fe4f,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:2f5b1407-3e41-45bd-a758-ee8544796910,ResourceVersion:7794145,Generation:0,CreationTimestamp:2023-08-20 13:43:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-recordevents-pod,UID:949e45dd-edd5-456b-80f7-72581ec13a2f,APIVersion:v1,ResourceVersion:7794143,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e9/e2e-sequence-recordevents-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:43:02 +0200 CEST
        LastTimestamp:2023-08-20 13:43:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14b065e0d87d,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:4db18e4e-00e8-458e-a7f0-b258cbcc0f67,ResourceVersion:7794151,Generation:0,CreationTimestamp:2023-08-20 13:43:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-recordevents-pod,UID:949e45dd-edd5-456b-80f7-72581ec13a2f,APIVersion:v1,ResourceVersion:7794144,FieldPath:spec.containers{recordevents},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/recordevents:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:02 +0200 CEST
        LastTimestamp:2023-08-20 13:43:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14b068a65f57,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:9a32ed73-04fe-4bfc-9f58-764cf86c6b26,ResourceVersion:7794153,Generation:0,CreationTimestamp:2023-08-20 13:43:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-recordevents-pod,UID:949e45dd-edd5-456b-80f7-72581ec13a2f,APIVersion:v1,ResourceVersion:7794144,FieldPath:spec.containers{recordevents},}
        Reason:Created
        Message:Created container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:02 +0200 CEST
        LastTimestamp:2023-08-20 13:43:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.177d14b070b13353,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:f3d7c160-e412-40db-bb3e-d6a0325717bb,ResourceVersion:7794158,Generation:0,CreationTimestamp:2023-08-20 13:43:02 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:02 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-recordevents-pod,UID:949e45dd-edd5-456b-80f7-72581ec13a2f,APIVersion:v1,ResourceVersion:7794144,FieldPath:spec.containers{recordevents},}
        Reason:Started
        Message:Started container recordevents
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:02 +0200 CEST
        LastTimestamp:2023-08-20 13:43:02 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-0.177d14b0f5864ae7,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:2dc3ccfd-ef97-4248-bcf6-2448c5e6c6b2,ResourceVersion:7794208,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-0,UID:25627e7d-9505-4f7c-96ad-5ec82d27409c,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794205,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-0" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-0.177d14b0f94868bb,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:2b8c4bf8-d2c3-471b-94a1-b3e57c923365,ResourceVersion:7794225,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-0,UID:25627e7d-9505-4f7c-96ad-5ec82d27409c,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794205,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-0"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-0.177d14b0fc017cce,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:10b3c177-aa9a-4296-99d3-80e766d62dbf,ResourceVersion:7794237,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-0,UID:25627e7d-9505-4f7c-96ad-5ec82d27409c,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794224,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-sequence-kn-sequence-0" not present in channel "e2e-sequence-kn-sequence-0" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-1.177d14b0f7050760,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:1cd0a054-a80a-42fa-9d67-753a27d5418e,ResourceVersion:7794216,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-1,UID:007f0f80-251d-478b-9a5d-c945ee6106a7,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794214,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-1" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-1.177d14b0fa9692af,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:7fbf59e5-e7ae-4750-838a-dbbff01764d1,ResourceVersion:7794232,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-1,UID:007f0f80-251d-478b-9a5d-c945ee6106a7,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794214,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-1"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14b0fa938359,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:de310f36-4a22-42d1-8d26-fce89fce590e,ResourceVersion:7794230,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-2,UID:3ae3066c-8b89-4980-b7fe-e841ab47d767,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794227,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-kn-sequence-2" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14b0ff2e8e95,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:d362e103-dcd4-4220-82cd-2d97b1cf1070,ResourceVersion:7794244,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-2,UID:3ae3066c-8b89-4980-b7fe-e841ab47d767,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794227,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-kn-sequence-2"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-kn-sequence-2.177d14b101faa9bd,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:4ebd9b35-0a4b-487f-9ea3-90a68a5733eb,ResourceVersion:7794251,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-kn-sequence-2,UID:3ae3066c-8b89-4980-b7fe-e841ab47d767,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794241,FieldPath:,}
        Reason:SubscriptionNotMarkedReadyByChannel
        Message:Failed to get subscription status: subscription "e2e-sequence-kn-sequence-2" not present in channel "e2e-sequence-kn-sequence-2" subscriber's list
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Warning
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-subscription.177d14b0f2760908,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:e927d668-8963-4a61-b8bf-6751a55a06eb,ResourceVersion:7794190,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-subscription,UID:055948fe-827e-4acd-921c-e0added4274f,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794188,FieldPath:,}
        Reason:FinalizerUpdate
        Message:Updated "e2e-sequence-subscription" finalizers
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-subscription.177d14b0f53c9b13,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:16f704a3-2f5a-4919-973e-f9dc320096ac,ResourceVersion:7794206,Generation:0,CreationTimestamp:2023-08-20 13:43:05 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:controller,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:05 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Subscription,Namespace:eventing-e2e9,Name:e2e-sequence-subscription,UID:055948fe-827e-4acd-921c-e0added4274f,APIVersion:messaging.knative.dev/v1,ResourceVersion:7794188,FieldPath:,}
        Reason:SubscriberSync
        Message:Subscription was synchronized to channel "e2e-sequence-channel"
        Source:EventSource{Component:subscription-controller,Host:,}
        FirstTimestamp:2023-08-20 13:43:05 +0200 CEST
        LastTimestamp:2023-08-20 13:43:05 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14b383c4bdac,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:83297549-882f-415e-ac68-8b4b3bf714a0,ResourceVersion:7794458,Generation:0,CreationTimestamp:2023-08-20 13:43:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kube-scheduler,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-sender-pod,UID:8e5b7ac5-4e10-4293-9804-21bcd5a2f045,APIVersion:v1,ResourceVersion:7794456,FieldPath:,}
        Reason:Scheduled
        Message:Successfully assigned eventing-e2e9/e2e-sequence-sender-pod to docker-desktop
        Source:EventSource{Component:default-scheduler,Host:,}
        FirstTimestamp:2023-08-20 13:43:16 +0200 CEST
        LastTimestamp:2023-08-20 13:43:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14b3ab9278c6,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:25685736-a338-4f85-bc3d-e70beb00d058,ResourceVersion:7794464,Generation:0,CreationTimestamp:2023-08-20 13:43:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-sender-pod,UID:8e5b7ac5-4e10-4293-9804-21bcd5a2f045,APIVersion:v1,ResourceVersion:7794457,FieldPath:spec.containers{event-sender},}
        Reason:Pulled
        Message:Container image "registrykn.szymonmusial.eu.org/event-sender:latest" already present on machine
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:16 +0200 CEST
        LastTimestamp:2023-08-20 13:43:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14b3adc401a6,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:16c39198-c7c3-4c69-b362-28fdd5b927ac,ResourceVersion:7794465,Generation:0,CreationTimestamp:2023-08-20 13:43:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-sender-pod,UID:8e5b7ac5-4e10-4293-9804-21bcd5a2f045,APIVersion:v1,ResourceVersion:7794457,FieldPath:spec.containers{event-sender},}
        Reason:Created
        Message:Created container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:16 +0200 CEST
        LastTimestamp:2023-08-20 13:43:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-sender-pod.177d14b3b31e039f,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:9d60ba4d-25d4-4dfd-ab73-7d62b8196abf,ResourceVersion:7794467,Generation:0,CreationTimestamp:2023-08-20 13:43:16 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:kubelet,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:16 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{},"f:host":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-sender-pod,UID:8e5b7ac5-4e10-4293-9804-21bcd5a2f045,APIVersion:v1,ResourceVersion:7794457,FieldPath:spec.containers{event-sender},}
        Reason:Started
        Message:Started container event-sender
        Source:EventSource{Component:kubelet,Host:docker-desktop,}
        FirstTimestamp:2023-08-20 13:43:16 +0200 CEST
        LastTimestamp:2023-08-20 13:43:16 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-sequence-recordevents-pod.Received.1,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:f0192b62-9482-449d-b28e-b930dd2e5169,ResourceVersion:7794562,Generation:0,CreationTimestamp:2023-08-20 13:43:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-sequence-recordevents-pod,UID:949e45dd-edd5-456b-80f7-72581ec13a2f,APIVersion:v1,ResourceVersion:7794146,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 1e8d17e6-92b7-4cd0-bf45-08a64ef9f622-step1-step2-step3"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["67"],"Content-Type":["text/plain"],"Host":["e2e-sequence-recordevents-pod.eventing-e2e9.svc.cluster.local"],"Kn-Namespace":["eventing-e2e9"],"Prefer":["reply"],"Traceparent":["00-79cd9ff03387624e55f762198e971b32-af0fa51a4440741d-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:60510","observer":"e2e-sequence-recordevents-pod","time":"2023-08-20T11:43:22.513169609Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:43:22 +0200 CEST
        LastTimestamp:2023-08-20 13:43:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper1.Received.1,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:b5a34ec3-f105-49a6-88d7-ac6862853109,ResourceVersion:7794559,Generation:0,CreationTimestamp:2023-08-20 13:43:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper1,UID:9c29efef-5428-4ee5-abdc-58213f0371d6,APIVersion:v1,ResourceVersion:7793899,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 1e8d17e6-92b7-4cd0-bf45-08a64ef9f622"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["49"],"Content-Type":["text/plain"],"Host":["e2e-stepper1.eventing-e2e9.svc.cluster.local"],"Kn-Namespace":["eventing-e2e9"],"Prefer":["reply"],"Traceparent":["00-79cd9ff03387624e55f762198e971b32-d2c831d9b1b17658-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:52172","observer":"e2e-stepper1","time":"2023-08-20T11:43:22.383581891Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:43:22 +0200 CEST
        LastTimestamp:2023-08-20 13:43:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper2.Received.1,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:f1ea3e3a-adac-4709-b664-479a0dbb8e70,ResourceVersion:7794560,Generation:0,CreationTimestamp:2023-08-20 13:43:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper2,UID:157d96fb-c7e5-41d6-a939-d6d42ffa9fdd,APIVersion:v1,ResourceVersion:7793943,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 1e8d17e6-92b7-4cd0-bf45-08a64ef9f622-step1"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["55"],"Content-Type":["text/plain"],"Host":["e2e-stepper2.eventing-e2e9.svc.cluster.local"],"Kn-Namespace":["eventing-e2e9"],"Prefer":["reply"],"Traceparent":["00-79cd9ff03387624e55f762198e971b32-71e002ef37e175ef-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:59576","observer":"e2e-stepper2","time":"2023-08-20T11:43:22.444927103Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:43:22 +0200 CEST
        LastTimestamp:2023-08-20 13:43:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    test_runner.go:284: Event{
        ObjectMeta:v1.ObjectMeta{Name:e2e-stepper3.Received.1,GenerateName:,Namespace:eventing-e2e9,SelfLink:,UID:240bc477-79f7-4c29-bc3c-d3cbfe462791,ResourceVersion:7794561,Generation:0,CreationTimestamp:2023-08-20 13:43:22 +0200 CEST,DeletionTimestamp:<nil>,DeletionGracePeriodSeconds:nil,Labels:map[string]string{},Annotations:map[string]string{},OwnerReferences:[]OwnerReference{},Finalizers:[],ManagedFields:[]ManagedFieldsEntry{ManagedFieldsEntry{Manager:recordevents,Operation:Update,APIVersion:v1,Time:2023-08-20 13:43:22 +0200 CEST,FieldsType:FieldsV1,FieldsV1:{"f:count":{},"f:firstTimestamp":{},"f:involvedObject":{},"f:lastTimestamp":{},"f:message":{},"f:reason":{},"f:source":{"f:component":{}},"f:type":{}},Subresource:,},},}
        InvolvedObject:ObjectReference{Kind:Pod,Namespace:eventing-e2e9,Name:e2e-stepper3,UID:7f4f525c-3247-429f-a0eb-7f0df6e5dff5,APIVersion:v1,ResourceVersion:7794075,FieldPath:,}
        Reason:CloudEventObserved
        Message:{"kind":"Received","event":{"specversion":"1.0","id":"test","source":"http://e2e-sequence-sender-pod.svc/","type":"dev.knative.test.event","datacontenttype":"text/plain","data":"TestSequence 1e8d17e6-92b7-4cd0-bf45-08a64ef9f622-step1-step2"},"httpHeaders":{"Accept-Encoding":["gzip"],"Content-Length":["61"],"Content-Type":["text/plain"],"Host":["e2e-stepper3.eventing-e2e9.svc.cluster.local"],"Kn-Namespace":["eventing-e2e9"],"Prefer":["reply"],"Traceparent":["00-79cd9ff03387624e55f762198e971b32-10f8d304be107586-01"],"User-Agent":["Go-http-client/1.1"]},"origin":"10.1.33.212:50470","observer":"e2e-stepper3","time":"2023-08-20T11:43:22.482549662Z","sequence":1}
        Source:EventSource{Component:observer-default,Host:,}
        FirstTimestamp:2023-08-20 13:43:22 +0200 CEST
        LastTimestamp:2023-08-20 13:43:22 +0200 CEST
        Count:1
        Type:Normal
        EventTime:0001-01-01 00:00:00 +0000 UTC
        Series:nil
        Action:
        Related:nil
        ReportingController:
        ReportingInstance:
        }
    k8s_events.go:66: EventListener stopped, 34 events seen
    tracker.go:133: Cleaning resource: "e2e-sequence-sender-pod"
    tracker.go:139: Waiting for e2e-sequence-sender-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence"
    tracker.go:139: Waiting for e2e-sequence to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-subscription"
    tracker.go:139: Waiting for e2e-sequence-subscription to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-recordevents-pod"
    tracker.go:139: Waiting for e2e-sequence-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-recordevents-pod"
    tracker.go:139: Waiting for e2e-sequence-recordevents-pod to be deleted
    tracker.go:133: Cleaning resource: "e2e-sequence-channel"
    tracker.go:139: Waiting for e2e-sequence-channel to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper3"
    tracker.go:139: Waiting for e2e-stepper3 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper3"
    tracker.go:139: Waiting for e2e-stepper3 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper2"
    tracker.go:139: Waiting for e2e-stepper2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper2"
    tracker.go:139: Waiting for e2e-stepper2 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper1"
    tracker.go:139: Waiting for e2e-stepper1 to be deleted
    tracker.go:133: Cleaning resource: "e2e-stepper1"
    tracker.go:139: Waiting for e2e-stepper1 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e9-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e9-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e9-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9"
    tracker.go:139: Waiting for eventing-e2e9 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9"
    tracker.go:139: Waiting for eventing-e2e9 to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e9"
    tracker.go:139: Waiting for eventing-e2e9 to be deleted
--- PASS: TestSequenceV1 (1.04s)
    --- PASS: TestSequenceV1/InMemoryChannel-messaging.knative.dev/v1 (45.96s)
=== CONT  TestBrokerNamespaceDefaulting
    broker_defaults_webhook_test.go:55: Updating defaulting ConfigMap attempt: 0
=== CONT  TestChannelNamespaceDefaulting
    channel_defaults_webhook_test.go:60: Updating defaulting ConfigMap
    channel_defaults_webhook_test.go:96: CM updated - new values: |
          clusterDefault:
            apiVersion: messaging.knative.dev/v1
            kind: InMemoryChannel
          namespaceDefaults:
            eventing-e2e5:
              apiVersion: messaging.knative.dev/v1
              kind: InMemoryChannel
              spec:
                delivery:
                  backoffDelay: PT0.5S
                  backoffPolicy: exponential
                  retry: 5
            some-namespace:
              apiVersion: messaging.knative.dev/v1
              kind: InMemoryChannel

=== NAME  TestBrokerNamespaceDefaulting
    broker_defaults_webhook_test.go:96: CM updated - new values: |
          clusterDefault:
            apiVersion: v1
            brokerClass: MTChannelBasedBroker
            delivery:
              backoffDelay: PT0.2S
              backoffPolicy: exponential
              retry: 10
            kind: ConfigMap
            name: config-br-default-channel
            namespace: knative-eventing
          namespaceDefaults:
            eventing-e2e1:
              apiVersion: v1
              brokerClass: MTChannelBasedBroker
              delivery:
                backoffDelay: PT0.5S
                backoffPolicy: exponential
                retry: 5
              kind: ConfigMap
              name: config-br-default-channel
              namespace: knative-eventing

=== NAME  TestChannelNamespaceDefaulting
    k8s_events.go:66: EventListener stopped, 0 events seen
    tracker.go:133: Cleaning resource: "eventing-e2e5-eventwatcher"
=== NAME  TestBrokerNamespaceDefaulting
    k8s_events.go:66: EventListener stopped, 0 events seen
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e5-eventwatcher to be deleted
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e1-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e1-eventwatcher to be deleted
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e5-eventwatcher"
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e1-eventwatcher"
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e5-eventwatcher to be deleted
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e1-eventwatcher to be deleted
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e5-eventwatcher"
    tracker.go:139: Waiting for eventing-e2e5-eventwatcher to be deleted
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e1-eventwatcher"
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e5"
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e1-eventwatcher to be deleted
    tracker.go:133: Cleaning resource: "eventing-e2e1"
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e5 to be deleted
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e1 to be deleted
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e5"
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e1"
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e5 to be deleted
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e1 to be deleted
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e5"
=== NAME  TestBrokerNamespaceDefaulting
    tracker.go:133: Cleaning resource: "eventing-e2e1"
    tracker.go:139: Waiting for eventing-e2e1 to be deleted
=== NAME  TestChannelNamespaceDefaulting
    tracker.go:139: Waiting for eventing-e2e5 to be deleted
--- PASS: TestChannelNamespaceDefaulting (3.68s)
--- PASS: TestBrokerNamespaceDefaulting (3.72s)
PASS
ok      knative.dev/eventing/test/e2e   345.253s
root@Szymon-Desktop:~/eventing#
```
