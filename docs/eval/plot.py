import os
import pandas as pd
import matplotlib.pyplot as plt

plt.figure(figsize=(8,4))

main_directory = '/home/szymon/repo/master-thesis/docs/eval'

def list_files(directory, prefix):
    return [f for f in os.listdir(directory) if f.startswith(prefix) and f.endswith('.csv') ]


dataframes = []
prefix = "7s_"
shift_seconds = -0.25 # 7s
#shift_seconds = -3.25 # 20s

max_x = 0.0

for file in list_files(main_directory, prefix):

    file_path = os.path.join(main_directory, file)
    df = pd.read_csv(file_path)
    df.set_index('ax', inplace=True)
    
    df.index = df.index + shift_seconds
    
    if df.index.max() > max_x:
        max_x = df.index.max()
    
    dataframes.append((file.split('.')[0].lstrip(prefix), df))


for file_name, df in dataframes:
    #if "sync" in file_name:
    plt.plot(df.index, df['val'], label=f"{file_name} layer".capitalize())



plt.gca().yaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/1000_000_000:.0f}'))
# plt.gca().xaxis.set_major_formatter(plt.FuncFormatter(lambda x, _: f'{x/840_000_000:.0f}'))

plt.xticks(list(range(0, int(max_x)+2, 1)))

#plt.title(f'Sum of used memory in task solver for each layer')
plt.xlabel('Timestamp [min]')
plt.ylabel("Used memory [GB]")
plt.legend()# loc='upper left')
plt.grid(True)
plt.savefig(os.path.join(os.path.dirname(__file__), prefix + " memory in task solver.svg"))
plt.show()


exit()

