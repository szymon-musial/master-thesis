import pandas as pd
import datetime
import os

def list_files(directory, prefix):
    return [f for f in os.listdir(directory) if f.startswith(prefix)]

prefix = '20s_cloud'

# List files in the directory starting with "7_"
files_list = list_files('.', prefix)

# Printing the list of files
print("Files starting with", files_list)

start_time = datetime.datetime(2024, 7, 17, 21, 25)

for f in files_list:



    # Creating a DataFrame
    df = pd.read_csv(f)
    
    df['Time'] = pd.to_numeric(df['Time'])

    # Converting the timestamp to a readable format 
    df['Time'] = pd.to_datetime(df['Time'], unit='ms') # + datetime.timedelta(minutes=9)

    # Converting the Time column to number of minutes from the first timestamp
    df['ax'] = (df['Time'] - start_time).dt.total_seconds() / 60

    # Showing the updated DataFrame

    print(df)

    df.to_csv(f, index=False)
