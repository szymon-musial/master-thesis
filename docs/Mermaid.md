# OpenTelemetry

```mermaid
flowchart LR

Prom[("Prometheus
(metrics)")]

Tempo[("Grafana Tempo
(traces)")]

Loki[("Grafana Loki
(logs)")]

Grafana["Grafana
dashboards
(Web UI)"]

Otlp[Opentelemetry Collector]

Pody_istio["Istio proxy
Istio ingress controller"]

Inne_pody["Pody"]

kn[Komponenty Knative]

Otlp --->|Otlp| Tempo
Otlp -->|Api| Loki

Prom <--> Grafana
Tempo <--> Grafana
Loki <--> Grafana


Pody_istio <-->|"Prometheus scrape (metrics)"| Prom
Pody_istio -->|"Zipkin (traces)"| Otlp
Pody_istio <--->|"Promtail (logs)"| Loki

kn <-->|"OpenCensus (metrics)"| Otlp
kn -->|"Zipkin (traces)"| Otlp
kn <--->|"Promtail (logs)"| Loki

Inne_pody -->|"Promtail (logs)"| Loki

Otlp --->|"Prometheus
remote write"| Prom
```

# Kiali

```mermaid
flowchart LR

kiali[Kiali dashboard]


Prom[("Prometheus
(metrics)")]

Tempo[("Grafana Tempo
(traces)")]

Grafana["Grafana
dashboards
(Web UI)"]

query["Grafana Tempo
Query"]


Prom <--> Grafana
Tempo <--> Grafana

query <--> Tempo

kiali <-->|"Jeager REST (traces)"| query
kiali <-->|"Pometheus protocol (metrics)"| Prom
kiali -->|"Url (href)"| Grafana

```
# Apps dependencies

```mermaid
flowchart BT

    kiali[Kiali dashboard]
    tempo_query[Grafana Tempo Query]

    cert[Cert bot]
    otlp[Open telemetry: Zipkin, OpenCensus reciver]
    tempo[Grafana Tempo]

    istio["Istio"]

    kn[Knative Eventing, Serving]
    prom[Prometheus]

    tempo_query -.->|Jaeger Api| kiali
    tempo -.->|Tempo backend| tempo_query
        
    registry[Docker Registry]

    otlp --> tempo

    otlp --> cert
    otlp --> prom

    istio -->|Traces| otlp
    istio -->|Metrics| prom

    kn -->|istio ingress gateway| istio
    kn --> cert

    kn -->|Traces, Metrics| otlp

    registry -->|"Images (Https)"| kn
```
