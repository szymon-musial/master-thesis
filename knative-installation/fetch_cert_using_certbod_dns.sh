#!/bin/bash
apt install -y certbot

certbot certonly --manual \
  --preferred-challenges=dns \
  --email szymonmusial@student.agh.edu.pl \
  --server https://acme-v02.api.letsencrypt.org/directory \
  --agree-tos \
  --manual-public-ip-logging-ok \
  -d "registrykn.szymonmusial.eu.org"


# Certificate   /etc/letsencrypt/live/registrykn.szymonmusial.eu.org/fullchain.pem
# Private key   /etc/letsencrypt/live/registrykn.szymonmusial.eu.org/privkey.pem