## NS
```sh
kubectl apply -f .\namespaces-knative.yaml
```
## Operator

```sh
wget -O - https://github.com/knative/operator/releases/download/knative-v1.11.1/operator.yaml | \
sed  's/namespace: default/namespace: knative-operator/g' | \
kubectl apply -f -
```

## Serving

```sh
kubectl apply -f .\knative-serving.yaml
```

## Eventing

```sh
kubectl apply -f .\knative-eventing.yaml
```

# Observability

## Tracing

[Blog](https://knative.dev/blog/articles/distributed-tracing/)

```sh
kubectl edit configmap/config-tracing -n knative-serving
kubectl edit configmap/config-tracing -n knative-eventing
```

```yaml
# ...
data:
  backend: zipkin
  debug: "true"
  zipkin-endpoint: http://my-collector-collector.observability.svc:9411/api/v2/spans
# ...
```

## Metrics
```sh
kubectl edit configmap/config-observability -n knative-serving
kubectl edit configmap/config-observability -n knative-eventing
```

```yaml
# ...
data:
  backend: zipkin
  metrics.backend-destination: opencensus
  metrics.opencensus-address: my-collector-collector.observability.svc:55678
  metrics.request-metrics-backend-destination: opencensus
# ...
```

## Logs


```sh
kubectl edit configmap/config-logging -n knative-serving
kubectl edit configmap/config-logging -n knative-eventing
```

```yaml
# ...
data:
  zap-logger-config: |
    {
      "level": "debug",
      "development": false,
      "outputPaths": ["stdout"],
      "errorOutputPaths": ["stderr"],
      "encoding": "json",
      "encoderConfig": {
        "timeKey": "timestamp",
        "levelKey": "severity",
        "nameKey": "logger",
        "callerKey": "caller",
        "messageKey": "message",
        "stacktraceKey": "stacktrace",
        "lineEnding": "",
        "levelEncoder": "",
        "timeEncoder": "iso8601",
        "durationEncoder": "",
        "callerEncoder": ""
      }
    }
# ...
```

## Docker registry in docker desktop host path

https://stackoverflow.com/questions/55378663/equivalent-of-minikube-ssh-with-docker-for-desktop-kubernetes-node/62117039#62117039

## Windows disks 

```sh
❯ docker run -it --rm --privileged --pid=host justincormack/nsenter1 /bin/bash -c 'ls /mnt/host/'
c     d     e     f     wsl   wslg
```
```sh
2023/08/20 12:05:09 Publishing registrykn.szymonmusial.eu.org/recordevents:latest
<3>WSL (11296) ERROR: UtilAcceptVsock:248: accept4 failed 110
Error: error processing import paths in "test/test_images/print/pod.yaml": error resolving image references: writing sbom: POST https://registrykn.szymonmusial.eu.org/v2/print/blobs/uploads/: UNKNOWN: unknown error; map[DriverName:filesystem Enclosed:map[Err:5 Op:mkdir Path:/var/lib/registry/docker/registry/v2/repositories/print/_uploads]] 
```

https://ko.build/features/sboms/

Using windows filesystem is bad idea, / path in wsl is not persistent, so use docker desktop volumes which is mounted in `/var/lib/docker/volumes/`

```sh
docker run -it --rm --privileged --pid=host justincormack/nsenter1 /bin/bash -c 'ls /var/lib/docker/volumes/{volume name}/_data'
```

## Certs 

```sh
./fetch_cert_using_certbod_dns.sh 


kubectl create --namespace istio-system secret tls knregistry-cert \
  --key /etc/letsencrypt/live/registrykn.szymonmusial.eu.org/privkey.pem \
  --cert /etc/letsencrypt/live/registrykn.szymonmusial.eu.org/fullchain.pem
```
## Registry deployment

```sh
kubectl apply -f .\docker-registry-namespace.yaml
kubectl apply -f .\docker-registry.yaml
kubectl apply -f .\docker-registry-joxitui.yaml
```

## Check

```sh
curl https://registrykn.szymonmusial.eu.org/v2/_catalog
{"repositories":[]}
```

### Upload image
```sh
docker pull nginx
docker tag nginx registrykn.szymonmusial.eu.org/nginx
docker push registrykn.szymonmusial.eu.org/nginx
```
### Dns issues

```sh
❯ docker push registrykn.szymonmusial.eu.org/nginx
Using default tag: latest
The push refers to repository [registrykn.szymonmusial.eu.org/nginx]
Get "https://registrykn.szymonmusial.eu.org/v2/": dialing registrykn.szymonmusial.eu.org:443 with direct connection: resolving host registrykn.szymonmusial.eu.org: lookup registrykn.szymonmusial.eu.org: no such host
```

### Updated docker engine json
```json
{
  "builder": {
    "gc": {
      "defaultKeepStorage": "20GB",
      "enabled": true
    }
  },
  "experimental": false,
  "features": {
    "buildkit": true
  },
  "dns" : [ "8.8.8.8", "8.8.4.4" ]
}
```

### Updated dns also in wsl, usefull when pushing in wsl
```sh
echo -e "[network]\ngenerateResolvConf = false\n" | sudo tee /etc/wsl.conf
echo -e "nameserver 8.8.8.8\n" | sudo tee /etc/resolv.conf
```

## k8 

```sh
kubectl edit configmap coredns -n kube-system
```

```sh
kn service update my-nginx --image registrykn.szymonmusial.eu.org/nginx --namespace default

Updating Service 'my-nginx' in namespace 'default':

  0.022s The Configuration is still working to reflect the latest desired specification.
  0.076s Revision "my-nginx-00006" failed with message: Unable to fetch image "registrykn.szymonmusial.eu.org/nginx": failed to resolve image to digest: Get "https://registrykn.szymonmusial.eu.org/v2/": dial tcp: lookup registrykn.szymonmusial.eu.org on 10.96.0.10:53: no such host.
Error: RevisionFailed: Revision "my-nginx-00006" failed with message: Unable to fetch image "registrykn.szymonmusial.eu.org/nginx": failed to resolve image to digest: Get "https://registrykn.szymonmusial.eu.org/v2/": dial tcp: lookup registrykn.szymonmusial.eu.org on 10.96.0.10:53: no such host.
Run 'kn --help' for usage
```

`10.96.0.10` -> core dns ip


## Fix k8

```yml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . /etc/resolv.conf {
           max_concurrent 1000
        }
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  creationTimestamp: "2023-08-08T17:19:58Z"
  name: coredns
  namespace: kube-system
  resourceVersion: "7371316"
  uid: 0a108a0e-14fd-42ff-9a32-58c1eaae4d83
```

Updated to 

```yml
apiVersion: v1
data:
  Corefile: |
    .:53 {
        errors
        health {
           lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
           pods insecure
           fallthrough in-addr.arpa ip6.arpa
           ttl 30
        }
        prometheus :9153
        forward . 8.8.8.8
        cache 30
        loop
        reload
        loadbalance
    }
kind: ConfigMap
metadata:
  creationTimestamp: "2023-08-08T17:19:58Z"
  name: coredns
  namespace: kube-system
  resourceVersion: "7371316"
  uid: 0a108a0e-14fd-42ff-9a32-58c1eaae4d83
```




```sh
kn service create my-nginx --image nginx --port 80 --namespace default  --security-context none

kn service update my-nginx --image registrykn.szymonmusial.eu.org/nginx --namespace default

docker pull registrykn.szymonmusial.eu.org/nginx:latest
```