#!/bin/sh

export IF=eth0;

while :
do
    echo "`date +%s`,`cat /sys/class/net/$IF/statistics/rx_bytes`,`cat /sys/class/net/$IF/statistics/rx_packets`,`cat /sys/class/net/$IF/statistics/tx_packets`,`cat /sys/class/net/$IF/statistics/tx_bytes`" | tee -a log.csv
    sleep 1
done

