hostA - 172.17.85.21
hostB - 172.17.85.22

```bash
modprobe ipip
ip tunnel add ipiptun mode ipip local 172.17.85.22 remote 172.17.85.21 ttl 64 dev enp7s0
ip addr add dev ipiptun 10.0.1.2 peer 10.0.1.1/32
ip link set dev ipiptun up


modprobe ipip
ip tunnel add ipiptun<z kąd do kąd> mode ipip local <adresip lokalny> remote <adresip zdalnej maszyny> ttl 64 dev <interfejs sieciowy maszyny lokalnej>
ip addr add dev ipiptun <wirtualny adres ten, z kąd do kąd> peer <wirtualny adres docelowej maszyny<do kąd z kąd> + maska>
ip link set dev ipiptun up


for i in `ls /proc/sys/net/ipv4/conf/*/forwarding`
do
echo 1 >$i
done

for i in `ls /proc/sys/net/ipv4/conf/*/rp_filter`
do
echo 0 >$i
done

sudo ip route add 10.240.0.0/24 via 10.0.1.2

```
32 tj z 3 na 2

# Na 3

ip tunnel add ipiptun13 mode ipip local 172.17.85.23 remote 172.17.85.21 ttl 64 dev enp7s0
ip addr add dev ipiptun13 10.0.1.13 peer 10.0.1.31/32
ip link set dev ipiptun13 up

ip tunnel add ipiptun23 mode ipip local 172.17.85.23 remote 172.17.85.22 ttl 64 dev enp7s0
ip addr add dev ipiptun23 10.0.1.23 peer 10.0.1.32/32
ip link set dev ipiptun23 up

sudo ip route add 10.250.0.0/24 via 10.0.1.31
sudo ip route add 10.240.0.0/24 via 10.0.1.32

# Na 2

### 12 ???
ip tunnel add ipiptun12 mode ipip local 172.17.85.22 remote 172.17.85.21 ttl 64 dev enp7s0
ip addr add dev ipiptun12 10.0.1.12 peer 10.0.1.21/32
ip link set dev ipiptun12 up

ip tunnel add ipiptun32 mode ipip local 172.17.85.22 remote 172.17.85.23 ttl 64 dev enp7s0
ip addr add dev ipiptun32 10.0.1.32 peer 10.0.1.23/32
ip link set dev ipiptun32 up

# Na 1

### 21 ???

ip tunnel add ipiptun21 mode ipip local 172.17.85.21 remote 172.17.85.22 ttl 64 dev enp6s0
ip addr add dev ipiptun21 10.0.1.21 peer 10.0.1.12/32
ip link set dev ipiptun21 up

ip tunnel add ipiptun31 mode ipip local 172.17.85.21 remote 172.17.85.23 ttl 64 dev enp6s0
ip addr add dev ipiptun31 10.0.1.31 peer 10.0.1.13/32
ip link set dev ipiptun31 up
