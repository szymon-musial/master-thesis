## Install

```sh
kubectl apply -f  https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.1/aio/deploy/recommended.yaml     
```

## Proxy URL
### http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy

```sh
kubectl proxy
```
## Metrics

```sh
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.6.4/components.yaml
```

### For docker desktop
[Problem](https://github.com/kubernetes-sigs/metrics-server/issues/196)
[Helm value](https://github.com/prometheus-community/helm-charts/blob/kube-prometheus-stack-48.3.1/charts/prometheus-node-exporter/values.yaml#L300)
```log
scraper.go:140] "Failed to scrape node" err="Get \"https://192.168.65.4:10250/metrics/resource\": x509: cannot validate certificate for 192.168.65.4 because it doesn't contain any IP SANs" node="docker-desktop"
```
Adding `--kubelet-insecure-tls` argument

```sh
kubectl patch deployment metrics-server -n kube-system --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]'
```

## Dashboard installation
```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
```

## Apply account
```sh
kubectl apply -f .\service-account.yaml
```

## Print dashboard admin token
after created [admin-user](./service-account.yaml)
```sh
kubectl create token admin-user -n kubernetes-dashboard
```