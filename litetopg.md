```bash
sqlite3 ismop2.db
sqlite> .headers on
sqlite> .mode csv
sqlite> .output data.csv
sqlite> SELECT * from weather;
sqlite> .quit
```


### status

```sql
select * from pg_stat_progress_copy \watch 1
```

```bash
\copy "Weather" ("Id","Status","Direction","RainfallCurrent","Temperature","RainfallHour","Humidity","Speed","Time", "Sync","StationID")  from ./data.csv delimiter ',' CSV HEADER;
```

## pump

sqlite

```sql
select "id","ASP_Level1_PV","ASP_Level_PV","ASP_Level_SP","ASP_Level2_PV","powerUsage","pump_1","pump_2","sync","stationID","time" from pumpControl;
```

pg

```bash
\copy "PumpControl" ("Id","ASP_Level1_PV","ASP_Level_PV","ASP_Level_SP","ASP_Level2_PV","PowerUsage","pump_1","pump_2","Sync","StationID","Time")  from ./pompa.csv delimiter ',' CSV HEADER;
```

# neosentio

### bez headera

sqlite

```sql
select "id","sensorID","measurement","sync","mField","rField","line","hType","hCondition","hLength","pStatus","pType","stationID","time" from neosentio;
```


pg

```bash
\copy "neosentio" ("Id","sensorID","measurement","sync","mField","rField","line","hType","hCondition","hLength","pStatus","pType","stationID","Time")  from ./neo.csv00 delimiter ',';
```

split --number=l/4 --numeric-suffixes ./neo.csv neo.csv

