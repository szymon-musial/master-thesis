apt install -y nfs-server

mkdir /data

cat <<EOF >> /etc/exports
/data *(rw,no_subtree_check,no_root_squash)
EOF

systemctl enable --now nfs-server

exportfs -ar