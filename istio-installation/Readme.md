# Istio install

## CRD
```sh
helm upgrade --install istio-base istio/base --version 1.18.1 --namespace istio-system  --create-namespace --set defaultRevision=1.18.1
```
## Istiod (control plane)
```sh
helm upgrade --install istiod istio/istiod --version 1.18.1 --namespace istio-system -f istio-istiod-values.yaml --wait
```
## Istio ingress gateway
```sh
helm upgrade --install istio-ingressgateway istio/gateway --version 1.18.1 --namespace istio-system --create-namespace
```
### Or for node port

```sh
helm upgrade --install istio-ingressgateway istio/gateway --version 1.18.1 -f istio-gateway-nodeport-values.yaml --namespace istio-system --create-namespace
```

# Prometheus

```sh
~# kubectl get prometheus  -o yaml -n observability | grep -A2 serviceMonitorSelector
    serviceMonitorSelector:
      matchLabels:
        release: kube-prometheus-stack
```

In file [istio-metrics-scrape.yaml](./istio-metrics-scrape.yaml) metadata.name.labels.release must be equal as above output


```sh
kubectl apply -f .\istio-metrics-scrape.yaml
```

```sh
kubectl port-forward services/kube-prometheus-stack-prometheus 9090:9090 -n observability
```

## Sidecar Injection
```sh
kubectl label namespace default istio-injection=enabled 
```