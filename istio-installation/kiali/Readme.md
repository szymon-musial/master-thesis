Kiali needs jeager api, but i have grafana query, so install grafana tempo query

```sh
kubectl apply -f .\tempo-query-jeager-api-ui.yaml
```

```sh
kubectl port-forward services/grafana-query 16686:16686 -n observability
```

## Kiali

# CRD
```sh
helm repo add kiali https://kiali.org/helm-charts
helm repo update
helm upgrade --install kiali-operator kiali/kiali-operator --version 1.72.0 --set cr.create=true --set cr.namespace=istio-system --namespace istio-system --create-namespace
```

# kiali app

```sh
kubectl apply -f .\kiali-server-via-operator.yaml
```

## Web UI

http://localhost:8001/api/v1/namespaces/istio-system/services/kiali:20001/proxy/



## uninstall
```sh
kubectl delete kiali --all --all-namespaces
helm uninstall kiali-operator --namespace istio-system 
kubectl delete crd kialis.kiali.io
```