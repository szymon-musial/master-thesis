using CloudNative.CloudEvents;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using shared;
using sync.Services;
using Wolverine;

public class Handlers(Logger<Handlers> logger, IServiceProvider serviceProvider) : IWolverineHandler
{
    public async Task<RoutedToEndpointMessage<CloudEvent>?> Handle(CloudEvent cloudEvent)
    {
        logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");

        var svcByCEType = serviceProvider.GetKeyedService<ICETypeHandler>(cloudEvent.GetRootType());
        ArgumentNullException.ThrowIfNull(svcByCEType, $"Not found service for type {cloudEvent.Type}");

        var res = await svcByCEType.ProcessCE(cloudEvent);
        return res;
    }
}