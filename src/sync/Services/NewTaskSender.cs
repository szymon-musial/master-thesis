using CloudNative.CloudEvents;
using Configuration;
using shared;
using Wolverine;

namespace sync.Services;

public class NewTaskSender(AppConfiguration appConfiguration) : ICETypeHandler
{
    public Task<RoutedToEndpointMessage<CloudEvent>?> ProcessCE(CloudEvent cloudEvent)
    {
        cloudEvent.Source = new Uri($"urn:edge/{appConfiguration.County}/{appConfiguration.StationId}");
        
        if(!string.IsNullOrEmpty(appConfiguration.ForceTaskTarget))
        {
            cloudEvent.SetTarget(new Uri($"urn:{appConfiguration.ForceTaskTarget}"));
        }
        else
        {
            cloudEvent.SetTarget(new Uri("urn:cloud"));
        }
        return Task.FromResult(cloudEvent.ToKafkaTopicDestination(appConfiguration.KNTopic) ?? null);
    }
}