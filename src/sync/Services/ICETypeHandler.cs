using CloudNative.CloudEvents;
using Wolverine;

namespace sync.Services;

public interface ICETypeHandler
{
    public Task<RoutedToEndpointMessage<CloudEvent>?> ProcessCE(CloudEvent cloudEvent);
}