using System.Text.Json;
using CloudNative.CloudEvents;
using Configuration;
using Microsoft.Extensions.Logging;
using shared;
using shared.Entities;
using sync.Persistence;
using Wolverine;

namespace sync.Services;

public class MeasurementsSender(AppConfiguration appConfiguration, MeasurementsDatabaseContext databaseContext, ILogger<MeasurementsSender> logger) : ICETypeHandler
{
    public async Task<RoutedToEndpointMessage<CloudEvent>?> ProcessCE(CloudEvent cloudEvent)
    {
        var entity = GetEntityBasedOnCEType(cloudEvent.Type, cloudEvent.Data.ToString());
        ArgumentNullException.ThrowIfNull(entity, nameof(entity));
        databaseContext.Add(entity);

        entity.Id = 0;

        logger.LogInformation($"Saved {await databaseContext.SaveChangesAsync()} items");

        cloudEvent.Source = new Uri($"urn:edge/{appConfiguration.County}/{appConfiguration.StationId}");

        return cloudEvent.ToKafkaTopicDestination(appConfiguration.KNTopic);
    }

    public static Entity? GetEntityBasedOnCEType(string cetype, string ceRawData)
    => cetype switch
    {
        "WeatherEntity" => JsonSerializer.Deserialize<WeatherEntity>(ceRawData),
        "PumpControlEntity" => JsonSerializer.Deserialize<PumpControlEntity>(ceRawData),
        "EmbankmentEntity" => JsonSerializer.Deserialize<EmbankmentEntity>(ceRawData),
        _ => throw new ArgumentOutOfRangeException(nameof(cetype), $"Not expected cetype value: {cetype}"),
    };
}