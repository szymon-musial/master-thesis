using CloudNative.CloudEvents;
using shared;
using Wolverine;

namespace sync.Services;

public class SolvedTaskSender() : ICETypeHandler
{
    public const string Key = "SolvedTask";
    public Task<RoutedToEndpointMessage<CloudEvent>?> ProcessCE(CloudEvent cloudEvent)
        => Task.FromResult(cloudEvent.ToMqttTopicDestination("results") ?? null);

}