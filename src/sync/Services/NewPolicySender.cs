using CloudNative.CloudEvents;
using shared;
using Wolverine;

namespace sync.Services;

public class NewPolicySender() : ICETypeHandler
{
    public Task<RoutedToEndpointMessage<CloudEvent>?> ProcessCE(CloudEvent cloudEvent)
        => Task.FromResult(cloudEvent.ToMqttTopicDestination("policy") ?? null);
}