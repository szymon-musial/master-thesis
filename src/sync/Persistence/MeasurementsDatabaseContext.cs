using Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using shared.Entities;

namespace sync.Persistence;

public class MeasurementsDatabaseContext(AppConfiguration appConfiguration) : DbContext()
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite($"Data Source= {appConfiguration.SyncDbPath}");
        base.OnConfiguring(optionsBuilder);
    }
    public DbSet<WeatherEntity> Weather { get; set; }
    public DbSet<PumpControlEntity> PumpControl { get; set; }
    public DbSet<EmbankmentEntity> Embankment { get; set; }
}

public class AppDesignTimeDbContextFactor : IDesignTimeDbContextFactory<MeasurementsDatabaseContext>
{
    public MeasurementsDatabaseContext CreateDbContext(string[] args)
        => new(new AppConfiguration());

}
