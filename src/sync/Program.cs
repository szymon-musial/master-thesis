﻿using Microsoft.Extensions.Hosting;
using Oakton;
using Wolverine;
using Wolverine.MQTT;
using Microsoft.Extensions.Configuration;
using Configuration;
using shared.Interop;
using CloudNative.CloudEvents;
using Microsoft.Extensions.DependencyInjection;
using Wolverine.Kafka;
using sync.Services;
using sync.Persistence;
using Microsoft.EntityFrameworkCore;
using shared.Dto.NewTasks;
using shared;
using shared.Dto;

return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
        conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
        .AddUserSecrets<Program>()
        .AddCommandLine(args)
    )
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));
        services.AddSingleton(appConfiguration);

        if(!appConfiguration.SkipMigration)
        {
            new MeasurementsDatabaseContext(appConfiguration).Database.Migrate();
            Console.WriteLine("Migration apply");
        }
        services.AddDbContext<MeasurementsDatabaseContext>();

        // service provider
        services.AddSingleton(sp => sp);
        
        services.AddKeyedTransient<ICETypeHandler, MeasurementsSender>("WeatherEntity");
        services.AddKeyedTransient<ICETypeHandler, MeasurementsSender>("PumpControlEntity");
        services.AddKeyedTransient<ICETypeHandler, MeasurementsSender>("EmbankmentEntity");

        services.AddKeyedTransient<ICETypeHandler, NewPolicySender>(NewPolicy.Key);
        services.AddKeyedTransient<ICETypeHandler, NewPolicySender>(NewTask.PolicyKey); // jako polityka bo policy enf sb to zapisze
        services.AddKeyedTransient<ICETypeHandler, NewPolicySender>(NewDispatchPolicy.Key); // nowy dispatch na polityki

        services.AddKeyedTransient<ICETypeHandler, NewTaskSender>(NewTask.Key);
        services.AddKeyedTransient<ICETypeHandler, SolvedTaskSender>(SolvedTaskSender.Key);
    })
    .UseWolverine((hostBuilderContext, opts) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));

        opts.UseMqtt(builder =>
        {
            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(appConfiguration.MqttServer, appConfiguration.MqttPort);
                });
        });
        opts.ListenToMqttTopic("raw/#")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.ListenToMqttTopic($"{Destinations.Remote_Tasks.ToString().ToLower()}/#")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.PublishAllMessages().ToMqttTopic("policy") // mqtt destination + filling on mapper
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.PublishAllMessages().ToMqttTopic("results") // mqtt destination + filling on mapper
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        Console.WriteLine($"Station ID {appConfiguration.StationId}");

        opts.UseKafka(appConfiguration.BootstrapServers)
            .AutoProvision();

        opts.ListenToKafkaTopic(appConfiguration.StationId)
            .UseInterop(new MyCloudEventApacheKafkaEnvelopeMapper(appConfiguration));

        opts.PublishMessage<CloudEvent>().ToKafkaTopic(appConfiguration.KNTopic)
            .UseDurableOutbox()
            .UseInterop(new MyCloudEventApacheKafkaEnvelopeMapper(appConfiguration));
    })
    .RunOaktonCommands(args);
