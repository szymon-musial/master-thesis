﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace sync.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "neosentio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    sensorID = table.Column<string>(type: "TEXT", nullable: false),
                    measurement = table.Column<int>(type: "INTEGER", nullable: false),
                    sync = table.Column<bool>(type: "INTEGER", nullable: false),
                    mField = table.Column<int>(type: "INTEGER", nullable: false),
                    rField = table.Column<int>(type: "INTEGER", nullable: false),
                    line = table.Column<int>(type: "INTEGER", nullable: false),
                    hType = table.Column<int>(type: "INTEGER", nullable: false),
                    hCondition = table.Column<int>(type: "INTEGER", nullable: false),
                    hLength = table.Column<int>(type: "INTEGER", nullable: false),
                    pStatus = table.Column<int>(type: "INTEGER", nullable: false),
                    pType = table.Column<int>(type: "INTEGER", nullable: false),
                    stationID = table.Column<string>(type: "TEXT", nullable: false),
                    Time = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_neosentio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PumpControl",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ASP_Level1_PV = table.Column<float>(type: "REAL", nullable: false),
                    ASP_Level_PV = table.Column<float>(type: "REAL", nullable: false),
                    ASP_Level_SP = table.Column<float>(type: "REAL", nullable: false),
                    ASP_Level2_PV = table.Column<float>(type: "REAL", nullable: false),
                    PowerUsage = table.Column<int>(type: "INTEGER", nullable: false),
                    pump_1 = table.Column<int>(type: "INTEGER", nullable: false),
                    pump_2 = table.Column<int>(type: "INTEGER", nullable: false),
                    Sync = table.Column<bool>(type: "INTEGER", nullable: false),
                    StationID = table.Column<string>(type: "TEXT", nullable: false),
                    Time = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PumpControl", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Weather",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Status = table.Column<int>(type: "INTEGER", nullable: false),
                    Direction = table.Column<int>(type: "INTEGER", nullable: false),
                    RainfallCurrent = table.Column<int>(type: "INTEGER", nullable: false),
                    Temperature = table.Column<int>(type: "INTEGER", nullable: false),
                    RainfallHour = table.Column<int>(type: "INTEGER", nullable: false),
                    Humidity = table.Column<int>(type: "INTEGER", nullable: false),
                    Speed = table.Column<int>(type: "INTEGER", nullable: false),
                    Sync = table.Column<bool>(type: "INTEGER", nullable: false),
                    StationID = table.Column<string>(type: "TEXT", nullable: false),
                    Time = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weather", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "neosentio");

            migrationBuilder.DropTable(
                name: "PumpControl");

            migrationBuilder.DropTable(
                name: "Weather");
        }
    }
}
