using CloudEvents;
using CloudNative.CloudEvents.SystemTextJson;
using Configuration;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Services;
using Wolverine;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix);
var appConfiguration = builder.Configuration.Get<AppConfiguration>()
    ?? throw new ArgumentNullException(nameof(AppConfiguration));
builder.Services.AddSingleton(appConfiguration);

builder.Services.AddControllers(opts => opts.InputFormatters.Insert(0, new CloudEventJsonInputFormatter(new JsonEventFormatter())));
builder.Services.AddDbContext<MeasurementsDatabaseContext>();
new MeasurementsDatabaseContext(appConfiguration).Database.Migrate();
builder.Services.AddTransient<DbSaverService>();

var app = builder.Build();

app.Use(async (context, next) =>
{
    // POST request comes from kn kafka brokers are without content type
    // https://github.com/knative/eventing-contrib/issues/1206
    if(string.IsNullOrEmpty(context.Request.Headers.ContentType))
    {
        context.Request.Headers.ContentType = "application/json";
    }
    await next(context);
});

app.MapControllers();

app.Run();
