﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace cloud_db_saver.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "neosentio",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    sensorID = table.Column<string>(type: "text", nullable: false),
                    measurement = table.Column<int>(type: "integer", nullable: false),
                    sync = table.Column<bool>(type: "boolean", nullable: false),
                    mField = table.Column<int>(type: "integer", nullable: false),
                    rField = table.Column<int>(type: "integer", nullable: false),
                    line = table.Column<int>(type: "integer", nullable: false),
                    hType = table.Column<int>(type: "integer", nullable: false),
                    hCondition = table.Column<int>(type: "integer", nullable: false),
                    hLength = table.Column<int>(type: "integer", nullable: false),
                    pStatus = table.Column<int>(type: "integer", nullable: false),
                    pType = table.Column<int>(type: "integer", nullable: false),
                    stationID = table.Column<string>(type: "text", nullable: false),
                    Time = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_neosentio", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PumpControl",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ASP_Level1_PV = table.Column<float>(type: "real", nullable: false),
                    ASP_Level_PV = table.Column<float>(type: "real", nullable: false),
                    ASP_Level_SP = table.Column<float>(type: "real", nullable: false),
                    ASP_Level2_PV = table.Column<float>(type: "real", nullable: false),
                    PowerUsage = table.Column<int>(type: "integer", nullable: false),
                    pump_1 = table.Column<int>(type: "integer", nullable: false),
                    pump_2 = table.Column<int>(type: "integer", nullable: false),
                    Sync = table.Column<bool>(type: "boolean", nullable: false),
                    StationID = table.Column<string>(type: "text", nullable: false),
                    Time = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PumpControl", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Weather",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Status = table.Column<int>(type: "integer", nullable: false),
                    Direction = table.Column<int>(type: "integer", nullable: false),
                    RainfallCurrent = table.Column<int>(type: "integer", nullable: false),
                    Temperature = table.Column<int>(type: "integer", nullable: false),
                    RainfallHour = table.Column<int>(type: "integer", nullable: false),
                    Humidity = table.Column<int>(type: "integer", nullable: false),
                    Speed = table.Column<int>(type: "integer", nullable: false),
                    Sync = table.Column<bool>(type: "boolean", nullable: false),
                    StationID = table.Column<string>(type: "text", nullable: false),
                    Time = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Weather", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "neosentio");

            migrationBuilder.DropTable(
                name: "PumpControl");

            migrationBuilder.DropTable(
                name: "Weather");
        }
    }
}
