using CloudNative.CloudEvents;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace Controllers;

public class CloudEventsController(ILogger<CloudEventsController> logger, DbSaverService dbSaver) : ControllerBase
{
    [HttpPost("/")]
    public async Task<AcceptedResult> CeEventEndpointAsync([FromBody] CloudEvent cloudEvent)
    {
        logger.LogInformation($"Recived CE {cloudEvent.Type} {cloudEvent.Id}");
        var _ = await dbSaver.ProcessCE(cloudEvent);
        return Accepted();
    }
}
