using System.Text.Json;
using CloudNative.CloudEvents;
using Persistence;
using shared.Entities;

namespace Services;

public class DbSaverService(MeasurementsDatabaseContext databaseContext)
{
    public async Task<int> ProcessCE(CloudEvent cloudEvent)
    {
        var entity = GetEntityBasedOnCEType(cloudEvent.Type, cloudEvent.Data.ToString());
        ArgumentNullException.ThrowIfNull(entity, nameof(entity));
        entity.Id = 0;

        await databaseContext.AddAsync(entity);
        return await databaseContext.SaveChangesAsync();
    }

    public static Entity? GetEntityBasedOnCEType(string cetype, string ceRawData)
    => cetype switch
    {
        "WeatherEntity" => JsonSerializer.Deserialize<WeatherEntity>(ceRawData),
        "PumpControlEntity" => JsonSerializer.Deserialize<PumpControlEntity>(ceRawData),
        "EmbankmentEntity" => JsonSerializer.Deserialize<EmbankmentEntity>(ceRawData),
        _ => throw new ArgumentOutOfRangeException(nameof(cetype), $"Not expected cetype value: {cetype}"),
    };

}
