using System.Text.RegularExpressions;
using Configuration;

public class SystemStats(AppConfiguration appConfiguration)
{
    /// <summary>
    /// </summary>
    /// <param name="TotalUsedMemory">Total used memory = MemTotal - MemAvailable??????????????</param>
    /// <param name="MemTotal">Total usable RAM</param>
    /// <param name="MemFree">Free RAM, the memory which is not used for anything at all</param>
    /// <param name="MemAvailable">Available RAM, the amount of memory available for allocation to any process</param>

    public record SystemStatsDto(double MemTotal, double MemFree, double MemAvailable)
    {
        public double TotalUsedMemory => MemTotal -  MemAvailable;

        public double MemTotalMb => MemTotal / 1024;
        public double MemFreeMb => MemFree / 1024;
        public double MemAvailableMb => MemAvailable / 1024;
        public double MemUsedPercentage => TotalUsedMemory / MemTotal * 100;

    }

    public SystemStatsDto GetMemStats()
    {
        string memoryStatsPath = appConfiguration.ProcMeminfoPath;
        if (File.Exists(memoryStatsPath))
        {
            var allLines = File.ReadAllLines(memoryStatsPath);
            var memStartingLines = allLines.Where(x => x.StartsWith("Mem"));

            return new SystemStatsDto(
                MemTotal: GetValueFormProcMemInfo(nameof(SystemStatsDto.MemTotal), memStartingLines),
                MemFree: GetValueFormProcMemInfo(nameof(SystemStatsDto.MemFree), memStartingLines),
                MemAvailable: GetValueFormProcMemInfo(nameof(SystemStatsDto.MemAvailable), memStartingLines)
            );
        }
        throw new FileNotFoundException(memoryStatsPath);
    }

    private static double GetValueFormProcMemInfo(string fieldName, IEnumerable<string> memStartingLines)
    {
        var regex = new Regex($"{fieldName}:\\s+(?<val>\\d+)\\s?kB");

        return memStartingLines
            .Where(l => regex.Match(l).Success)
            .Select(i => regex.Match(i).Groups["val"].Value)
            .Select(v => double.Parse(v))
            .First();
    }

}