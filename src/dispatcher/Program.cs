﻿using Configuration;
using Microsoft.Extensions.Hosting;
using Oakton;
using Wolverine;
using Wolverine.MQTT;
using Microsoft.Extensions.Configuration;
using shared.Interop;
using shared;
using Microsoft.Extensions.DependencyInjection;

return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
        conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
        .AddUserSecrets<Program>()
        .AddCommandLine(args)
    )
    .UseWolverine((context, opts) =>
    {
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var mqttServer = context.Configuration.Get<AppConfiguration>()!.MqttServer;

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(mqttServer);
                });
        });
        opts.ListenToMqttTopic("cqrs/#")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        foreach (var destinations in Enum.GetValues(typeof(Destinations)))
        {
            opts.PublishAllMessages().ToMqttTopic(destinations.ToString().ToLower()) // mqtt destination + filling on mapper
                .UseInterop(new MyCloudEventMqttEnvelopeMapper());            
        }
    })
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));
        services.AddSingleton(appConfiguration);
        
        services.AddSingleton<DispatchRules>();
        services.AddSingleton<SystemStats>();
    })
    .RunOaktonCommands(args);

