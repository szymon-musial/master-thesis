using System.Text.Json;
using CloudNative.CloudEvents;
using Microsoft.Extensions.Logging;
using shared;
using shared.Dto;
using shared.Dto.NewTasks;
using Wolverine;

public class Handlers(ILogger<Handlers> logger, DispatchRules dispatchRules, SystemStats systemStats) : IWolverineHandler
{
    public readonly string[] SupportedRootTypes = [ NewTask.Key, NewDispatchPolicy.Key ];
    public RoutedToEndpointMessage<CloudEvent>? Handle(CloudEvent cloudEvent)
    {
        if(!SupportedRootTypes.Contains(cloudEvent.GetRootType()))
        {
            return null;
        }

        logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");

        if(cloudEvent.GetRootType() == NewTask.Key)
        {
            return ProcessNewTask(cloudEvent);
        }

        if(cloudEvent.GetRootType() == NewDispatchPolicy.Key)
        {
            ProcessNewDispatchPolicy(cloudEvent);
        }

        return null;
    }

    private void ProcessNewDispatchPolicy(CloudEvent cloudEvent)
    {
        var newDispatchPolicy = JsonSerializer.Deserialize<NewDispatchPolicy>(cloudEvent.Data.ToString());
        dispatchRules.Rules[newDispatchPolicy.Field] = newDispatchPolicy.Value;
    }

    public RoutedToEndpointMessage<CloudEvent>? ProcessNewTask(CloudEvent cloudEvent)
    {
        cloudEvent.Source = new Uri("urn:dispatcher");
        var dest = ScheduleDestination(cloudEvent);
        return cloudEvent.ToMqttTopicDestination(dest.ToString().ToLower());
    }

    public Destinations? GetDestinationByResources(string resourceName)
    {
        var ramRuleStr = dispatchRules.Rules.GetValueOrDefault(resourceName);
        if(ramRuleStr is not null && double.TryParse(ramRuleStr, out var ramRule))
        {
            var stats = systemStats.GetMemStats().MemUsedPercentage;            
            var dest = (stats > ramRule) ? Destinations.Remote_Tasks : Destinations.Local_Tasks;
            logger.LogInformation($"Stats {stats}, rule {ramRule} -> {dest}");
            return dest;
        }
        return null;
    }

    public static Destinations GetRandomDestination()
    {
        Array values = Enum.GetValues(typeof(Destinations));
        Random random = new Random();
        return (Destinations)values.GetValue(random.Next(values.Length));
    }

    internal Destinations ScheduleDestination(CloudEvent cloudEvent)
        => GetDestinationByResources(nameof(SystemStats.SystemStatsDto.MemUsedPercentage)) ?? GetRandomDestination();
}