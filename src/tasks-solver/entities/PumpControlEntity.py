from sqlalchemy import Column, Integer, String, Boolean, Float, BigInteger
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class PumpControlEntity(Base):
    __tablename__ = 'PumpControl'
    
    Id = Column(Integer, primary_key=True, autoincrement=True)
    Time = Column(BigInteger, nullable=False)

    @property
    def Timestamp(self):
        return datetime.datetime.fromtimestamp(self.Time)

    AspLevel1PV = Column(Float, nullable=False, name="ASP_Level1_PV")
    AspLevelPV = Column(Float, nullable=False, name="ASP_Level_PV")
    AspLevelSP = Column(Float, nullable=False, name="ASP_Level_SP")
    AspLevel2PV = Column(Float, nullable=False, name="ASP_Level2_PV")
    PowerUsage = Column(Integer, nullable=False)
    Pump1 = Column(Integer, nullable=False, name="pump_1")
    Pump2 = Column(Integer, nullable=False, name="pump_2")
    Sync = Column(Boolean, nullable=False)
    StationID = Column(String, nullable=False)
