from sqlalchemy import Column, Integer, String, Boolean, BigInteger
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class WeatherEntity(Base):
    __tablename__ = 'Weather'

    Id = Column(Integer, primary_key=True, autoincrement=True)
    Time = Column(BigInteger, nullable=False)

    @property
    def Timestamp(self):
        return datetime.datetime.fromtimestamp(self.Time)

    Status = Column(Integer, nullable=False)
    Direction = Column(Integer, nullable=False)
    RainfallCurrent = Column(Integer, nullable=False)
    Temperature = Column(Integer, nullable=False)
    RainfallHour = Column(Integer, nullable=False)
    Humidity = Column(Integer, nullable=False)
    Speed = Column(Integer, nullable=False)
    Sync = Column(Boolean, nullable=False)
    StationID = Column(String, nullable=False)
