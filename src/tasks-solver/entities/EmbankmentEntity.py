from sqlalchemy import Column, Integer, String, Boolean, BigInteger
from sqlalchemy.ext.declarative import declarative_base
import datetime

Base = declarative_base()

class EmbankmentEntity(Base):
    __tablename__ = 'neosentio'
    
    Id = Column(Integer, primary_key=True, autoincrement=True)
    Time = Column(BigInteger, nullable=False)

    @property
    def Timestamp(self):
        return datetime.datetime.fromtimestamp(self.Time)

    SensorID = Column(String, nullable=False, name="sensorID")
    Measurement = Column(Integer, nullable=False, name="measurement")
    Sync = Column(Boolean, nullable=False, name="sync")
    MField = Column(Integer, nullable=False, name="mField")
    RField = Column(Integer, nullable=False, name="rField")
    Line = Column(Integer, nullable=False, name="line")
    HType = Column(Integer, nullable=False, name="hType")
    HCondition = Column(Integer, nullable=False, name="hCondition")
    HLength = Column(Integer, nullable=False, name="hLength")
    PStatus = Column(Integer, nullable=False, name="pStatus")
    PType = Column(Integer, nullable=False, name="pType")
    StationID = Column(String, nullable=False, name="stationID")
