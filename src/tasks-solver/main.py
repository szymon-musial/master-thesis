# import paho.mqtt.client as mqtt
import logging.handlers
import os
from cloudevents.http import from_http, to_structured
from cloudevents.conversion import to_json, to_binary
from cloudevents.http.event import CloudEvent
import logging
import datetime
from flask import Flask, request, Response, g
import time
from TaskProcess import TaskProcess
from RFTaskProcess import RFTaskProcess

import gc

# mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id=f"tasks-solver{os.getpid()}")

# def on_connect(client, userdata, flags, reason_code, properties):
#     logging.info(f"Connected with result code {reason_code}")
#     client.subscribe("cqrs/#")

# def on_message(client, userdata, msg):
#     cloudEvent = from_json(msg.payload)
#     # print(f"Received CE {cloudEvent['type']} {cloudEvent['id']}")
#     process_ce(cloudEvent, 'NewTask', TaskProcess.Handle)
    # json_ce = to_json(new_event)
    # mqttc.publish(f"results/{ce['type']}/{ce['id']}", json_ce)

def set_source_and_target(ce: CloudEvent, new_source = "urn:tasks-solver"):
    ce['target'] = ce['source']
    ce['source'] = new_source

def process_ce(ce: CloudEvent, expected_ce_type: str, callback):
    try:
        if ce['type'] == expected_ce_type:
            resp, typ = callback(ce)
            if resp is not None:
                new_attributes = {
                    **ce._attributes,
                    'time':  datetime.datetime.now().astimezone().isoformat(),
                    'type': f'SolvedTask|{typ}'
                }
                ce = CloudEvent(new_attributes, resp)
                set_source_and_target(ce)
                return ce
                
                # json_ce = to_json(new_event)
                # mqttc.publish(f"results/{ce['type']}/{ce['id']}", json_ce)
    except Exception as error:
        logging.error(f"Error with processing {ce}")
        logging.error(type(error).__name__, "-", error)


app = Flask(__name__)

@app.before_request
def before_request():
    g.start = time.time()

@app.route("/", methods=["POST"])
def ping_ponger():
    cloudEvent = from_http(request.headers, request.get_data())

    # you can access cloudevent fields as seen below
    logging.info(
        f"Found {cloudEvent['id']} from {cloudEvent['source']} with type "
        f"{cloudEvent['type']} and specversion {cloudEvent['specversion']}"
    )
    
    new_event = None
    
    tmp = process_ce(cloudEvent, 'NewTask|Arima', TaskProcess.Handle)
    if tmp is not None:
        new_event = tmp

    tmp = process_ce(cloudEvent, 'NewTask|RF', RFTaskProcess.Handle)
    if tmp is not None:
        new_event = tmp
    
    if new_event is None:
        return Response(None, status=202, mimetype='application/json')

    exec_time_s = time.time() - g.start
    new_event['execsec'] = exec_time_s
    
    headers, body_byte = to_binary(new_event)
    #headers, body_byte = to_structured(new_event)

    print(f"Garbage collector: collected {gc.collect()} objects.")
    return Response(body_byte, status=200, headers=headers)
   # body, 200, headers

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 7000)))


if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    logging.handlers = gunicorn_logger.handlers
    logging.basicConfig(level=gunicorn_logger.level)


# if __name__ == "__main__":

#     logging.basicConfig(level=logging.DEBUG)

#     mqttc.on_connect = on_connect
#     mqttc.on_message = on_message

#     broker_address = os.environ.get("broker", "localhost")
#     mqttc.connect(broker_address, 1883, 60)

#     mqttc.loop_forever()
