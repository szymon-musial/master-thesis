# import paho.mqtt.client as mqtt
import json
import logging
import os
from cloudevents.http.event import CloudEvent
from sqlalchemy import create_engine, desc

from sqlalchemy.orm import sessionmaker

from entities.WeatherEntity import *
from entities.EmbankmentEntity import *
from entities.PumpControlEntity import *

from statsmodels.tsa.arima.model import ARIMA, ARIMAResultsWrapper

class TaskProcess:
    def Handle(ce: CloudEvent):
        data = ce.data
        logging.info(f"Incoming task {data}")
        
        connection_string = os.environ.get('CONN_STR', 'sqlite:////home/szymon/sync.db')
        # connection_string = os.environ.get('CONN_STR', 'postgresql+psycopg2://postgres:root@localhost/postgres')
        
        engine = create_engine(connection_string, echo=True, pool_pre_ping=True)
        Session = sessionmaker(bind=engine)
        session = Session()
        
        limit = None
        if 'Limit' in data and data['Limit'] is not None:
            limit = int(data['Limit'])
        
        embankments = (
            session.query(EmbankmentEntity)
            .filter(EmbankmentEntity.Time >= int(data['Time']))
            .filter(EmbankmentEntity.Time <= int(data['TimeNow']))
            .order_by(desc(EmbankmentEntity.Time))
            .limit(limit)
        )
        
        logging.info(f"sql count {embankments.count()}")
        if embankments.count() == 0:
            return
        
        embankment_data = [embankment.Measurement for embankment in embankments]
        embankment_data.reverse()  # ARIMA expects the data in chronological order
               
        model = ARIMA(embankment_data, order=(data['Order_p'], data['Order_d'], data['Order_q']))
        model_fit: ARIMAResultsWrapper = model.fit()
        
        stats = list(zip(model_fit.param_names, model_fit.params))
        stats.append(('mse', model_fit.mse))
        forecasts = model_fit.forecasts[0][:50]
        
        res = {
            "type": "arima",
            "statistics" : stats,
            "forecasts": forecasts.tolist()
        }
        
        logging.info(model_fit.summary())
        
        return res, 'Arima'
