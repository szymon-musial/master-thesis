import logging
import os
from cloudevents.http.event import CloudEvent
from sqlalchemy import create_engine, desc

from sqlalchemy.orm import sessionmaker

from entities.WeatherEntity import *
from entities.EmbankmentEntity import *
from entities.PumpControlEntity import *

import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from pandas import DataFrame
from pandas import concat
from sklearn.ensemble import RandomForestRegressor

# https://machinelearningmastery.com/random-forest-for-time-series-forecasting/
class RFTaskProcess:
    def Handle(ce: CloudEvent):
        data = ce.data
        logging.info(f"Incoming task {data}")
        
        connection_string = os.environ.get('CONN_STR', 'sqlite:////home/szymon/sync.db')
        # connection_string = os.environ.get('CONN_STR', 'postgresql+psycopg2://postgres:root@localhost/postgres')
        
        engine = create_engine(connection_string, echo=True, pool_pre_ping=True)
        Session = sessionmaker(bind=engine)
        session = Session()
        
        limit = None
        if 'Limit' in data and data['Limit'] is not None:
            limit = int(data['Limit'])
            
        n_estimators = 1000
        if 'n_estimators' in data and data['n_estimators'] is not None:
            n_estimators = int(data['n_estimators'])
            
        
        n_in = 6
        if 'N_in' in data and data['N_in'] is not None:
            n_in = int(data['N_in'])
        
        embankments = (
            session.query(EmbankmentEntity)
            .filter(EmbankmentEntity.Time >= int(data['Time']))
            .filter(EmbankmentEntity.Time <= int(data['TimeNow']))
            .order_by(desc(EmbankmentEntity.Time))
            .limit(limit)
        )
        
        # Konwersja zapytania SQLAlchemy do DataFrame
        df = pd.read_sql(embankments.statement, engine, columns=['measurement'])
        
        series = df[["measurement", "Time"]]
        series.set_index('Time', inplace=True)
        
        values = series.values
        # transform the time series data into supervised learning
        train = RFTaskProcess.series_to_supervised(values, n_in=n_in)
        # split into input and output columns
        trainX, trainy = train[:, :-1], train[:, -1]
        # fit model
        model = RandomForestRegressor(n_estimators=n_estimators)
        model.fit(trainX, trainy)
        # construct an input for a new prediction
        #row = values[-6:].flatten()
        # make a one-step prediction
       
        forecast_count = 100
        inp = values[-(forecast_count + n_in):]       
        forecast_vect = RFTaskProcess.series_to_supervised(inp, n_in=n_in-1)

        
        #yhat = model.predict(asarray([row]))
        yhat = model.predict(forecast_vect)        
        logging.info('Input: %s, Predicted: %s' % (inp.flatten()[:10], yhat[:10]))
        
        res = {
            "type": "RandomForestRegressor",
            "forecasts": yhat.tolist()
        }
        
        return res, 'RF'


    def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
        n_vars = 1 if type(data) is list else data.shape[1]
        df = DataFrame(data)
        cols = list()
        # input sequence (t-n, ... t-1)
        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
        # forecast sequence (t, t+1, ... t+n)
        for i in range(0, n_out):
            cols.append(df.shift(-i))
        # put it all together
        agg = concat(cols, axis=1)
        # drop rows with NaN values
        if dropnan:
            agg.dropna(inplace=True)
        return agg.values