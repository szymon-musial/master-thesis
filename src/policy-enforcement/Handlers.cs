using CloudNative.CloudEvents;
using shared.Dto;
using shared.Dto.NewTasks;
using Wolverine;

public class Handlers(Logger<Handlers> logger, NewPolicyProcess newPolicyProcess, NewTaskProcess newTaskProcess, NewDispatchPolicyProcess newDispatchPolicyProcess) : IWolverineHandler
{
    public async Task HandleAsync(CloudEvent cloudEvent)
    {
        logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");

        if(cloudEvent.Type == NewPolicy.Key)
        {
            await newPolicyProcess.HandleAsync(cloudEvent);
            return;
        }

        if(cloudEvent.Type == NewTask.Key || cloudEvent.Type == NewTask.PolicyKey)
        {
            await newTaskProcess.HandleAsync(cloudEvent);
            return;
        }

        if(cloudEvent.Type == NewDispatchPolicy.Key)
        {
            await newDispatchPolicyProcess.HandleAsync(cloudEvent);
            return;
        }

        throw new ArgumentOutOfRangeException(nameof(cloudEvent.Type));
    }
}