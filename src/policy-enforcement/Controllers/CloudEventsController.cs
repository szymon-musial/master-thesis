using CloudNative.CloudEvents;
using Microsoft.AspNetCore.Mvc;
using shared.Dto;
using shared.Dto.NewTasks;

namespace Controllers;

public class CloudEventsController(ILogger<CloudEventsController> logger,
    NewPolicyProcess newPolicyProcess, NewTaskProcess newTaskProcess, NewDispatchPolicyProcess newDispatchPolicyProcess) : ControllerBase
{
    [HttpPost("/")]
    public async Task<ActionResult> CeEventEndpointAsync([FromBody] CloudEvent cloudEvent)
    {
        ArgumentNullException.ThrowIfNull(cloudEvent, nameof(cloudEvent));
        logger.LogInformation($"Recived CE {cloudEvent.Type} {cloudEvent.Id}");

        if(cloudEvent.Type == NewPolicy.Key)
        {
            await newPolicyProcess.HandleAsync(cloudEvent);
            return Ok();
        }

        if(cloudEvent.Type == NewTask.Key || cloudEvent.Type == NewTask.PolicyKey)
        {
            await newTaskProcess.HandleAsync(cloudEvent);
            return Ok();
        }

        if(cloudEvent.Type == NewDispatchPolicy.Key)
        {
            await newDispatchPolicyProcess.HandleAsync(cloudEvent);
            return Ok();
        }

        throw new ArgumentOutOfRangeException(nameof(cloudEvent.Type));
    }
}
