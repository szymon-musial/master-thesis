using CloudEvents;
using CloudNative.CloudEvents;
using CloudNative.CloudEvents.SystemTextJson;
using Configuration;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Quartz;
using shared.Interop;
using Wolverine;
using Wolverine.MQTT;


using (var db = new QuartzDatabaseContext())
{
    await db.Database.MigrateAsync();
}

var builder = WebApplication.CreateBuilder(args);

builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix);
var appConfiguration = builder.Configuration.Get<AppConfiguration>()
    ?? throw new ArgumentNullException(nameof(AppConfiguration));
builder.Services.AddSingleton(appConfiguration);

builder.Host.UseWolverine(opts =>
    {
        opts.UseMqtt(builder =>
        {
            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(appConfiguration.MqttServer, appConfiguration.MqttPort);
                });
        });
        opts.ListenToMqttTopic("policy/#")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.PublishMessagesToMqttTopic<CloudEvent>(c => $"cqrs/{c.Type}/{c.Id}")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());
    });

builder.Services.AddControllers(opts => opts.InputFormatters.Insert(0, new CloudEventJsonInputFormatter(new JsonEventFormatter())));

builder.Services.AddTransient<NewPolicyProcess>();
builder.Services.AddTransient<NewTaskProcess>();

builder.Services.AddQuartz(q =>
{
    q.UseMicrosoftDependencyInjectionJobFactory();
    q.UsePersistentStore(x =>
    {
        x.UseMicrosoftSQLite(QuartzDatabaseContext.ConnectionString);
        // this requires Quartz.Serialization.Json NuGet package
        x.UseJsonSerializer();
    });
});
builder.Services.AddQuartzHostedService(opt => opt.WaitForJobsToComplete = true);

var app = builder.Build();

app.MapControllers();

app.Run();
