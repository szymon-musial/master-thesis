using System.Text.Json;
using CloudNative.CloudEvents;
using policy_enforcement.Jobs;
using Quartz;
using shared.Dto;
using shared.Dto.DeviceSpecs;

public class NewPolicyProcess(ISchedulerFactory schedulerFactory)
{
    public async Task HandleAsync(CloudEvent cloudEvent)
    {
        //logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");
        var policy = JsonSerializer.Deserialize<NewPolicy<DeviceSpecsCls>>(cloudEvent.Data.ToString());
        await PutJobAsync(
            policy.Id,
            policy.Cron,
            cloudEvent.Data.ToString(),
            MapToDevSpec(policy.Dev.DevType)
        );
    }

    private string MapToDevSpec(string devType) => devType.ToLower() switch
    {
        I2CDeviceSpecs.Key => nameof(I2CDeviceSpecs),
        ModBusSpecs.Key => nameof(ModBusSpecs),
        _ => throw new ArgumentOutOfRangeException(nameof(devType), $"Not expected devType value: {devType}"),
    };

    public async Task PutJobAsync(string id, string cronExpression, string cloudEventData, string devSpecClass)
    {
        var jobKey = new JobKey(id, nameof(NewPolicyProcess));
        var triggerKeyKey = new TriggerKey(id, nameof(NewPolicyProcess));

        IJobDetail job = JobBuilder.Create<ScheduledPolicyJob>()
            .WithIdentity(jobKey)
            .UsingJobData("data", cloudEventData)
            .UsingJobData("devSpecClass", devSpecClass)
            .Build();

        ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity(triggerKeyKey)
            .WithCronSchedule(cronExpression)
            .StartNow()
            .Build();

        var scheduler = await schedulerFactory.GetScheduler();
        await scheduler.UnscheduleJob(triggerKeyKey);
        await scheduler.ScheduleJob(job, trigger);
    }

}