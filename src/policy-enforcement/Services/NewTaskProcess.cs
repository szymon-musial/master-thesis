using System.Text.Json;
using CloudNative.CloudEvents;
using policy_enforcement.Jobs;
using Quartz;
using shared.Dto.NewTasks;

public class NewTaskProcess(ISchedulerFactory schedulerFactory)
{
    public async Task HandleAsync(CloudEvent cloudEvent)
    {
        //logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");
        var task = JsonSerializer.Deserialize<NewTask>(cloudEvent.Data.ToString());
        await PutJobAsync(task);
    }

    public async Task PutJobAsync(NewTask newTask)
    {
        var jobKey = new JobKey(newTask.Id, nameof(NewTaskProcess));
        var triggerKeyKey = new TriggerKey(newTask.Id, nameof(NewTaskProcess));

        IJobDetail job = JobBuilder.Create<ScheduledTaskJob>()
            .WithIdentity(jobKey)
            .UsingJobData("task", JsonSerializer.Serialize(newTask))
            .Build();

        ITrigger trigger = TriggerBuilder.Create()
            .WithIdentity(triggerKeyKey)
            .WithCronSchedule(newTask.Cron)
            .StartNow()
            .Build();

        var scheduler = await schedulerFactory.GetScheduler();
        await scheduler.UnscheduleJob(triggerKeyKey);
        await scheduler.ScheduleJob(job, trigger);
    }
}