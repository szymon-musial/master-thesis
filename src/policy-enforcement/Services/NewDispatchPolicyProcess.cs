using CloudNative.CloudEvents;
using Wolverine;

public class NewDispatchPolicyProcess(IMessageBus messageBus)
{
    public async Task HandleAsync(CloudEvent cloudEvent)
    {
        await messageBus.PublishAsync(cloudEvent);
    }
}