using AppAny.Quartz.EntityFrameworkCore.Migrations;
using AppAny.Quartz.EntityFrameworkCore.Migrations.SQLite;
using Microsoft.EntityFrameworkCore;

namespace Persistence;

public class QuartzDatabaseContext : DbContext
{
    public static readonly string ConnectionString = "Data Source= Quartz.db";

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(ConnectionString);
        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Adds Quartz.NET SQLite schema to EntityFrameworkCore
        modelBuilder.AddQuartz(builder => builder.UseSqlite());
    }
}