using System.Text.Json;
using CloudNative.CloudEvents;
using Quartz;
using shared.Dto;
using shared.Dto.DeviceSpecs;
using Wolverine;

namespace policy_enforcement.Jobs;

public class ScheduledPolicyJob(ILogger<ScheduledPolicyJob> logger, IMessageBus messageBus) : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        JobKey key = context.JobDetail.Key;
        logger.LogInformation($"Executing at {DateTime.UtcNow} (Key: {key.Name}, Group: {key.Group})");

        JobDataMap dataMap = context.JobDetail.JobDataMap;
        var cloudEventData = dataMap.GetString("data");
        var devSpecClass = dataMap.GetString("devSpecClass");
        var devSpecType = GetNewPolicyTypeForDevSpecName(devSpecClass);

        var ce = new CloudEvent()
        {
            Id = Guid.NewGuid().ToString(),
            Type = $"{nameof(NewPolicy)}|{devSpecClass}",
            Source = new Uri("urn:policy-enforcement"),
            Data = JsonSerializer.Deserialize(cloudEventData, devSpecType),
            Time = DateTime.Now,
        };

        await messageBus.PublishAsync(ce);
    }

    private Type GetNewPolicyTypeForDevSpecName(string deviceSpecs) => deviceSpecs switch
    {
        nameof(I2CDeviceSpecs) => typeof(NewPolicy<I2CDeviceSpecs>),
        nameof(ModBusSpecs) => typeof(NewPolicy<ModBusSpecs>),
        _ => throw new ArgumentOutOfRangeException(nameof(deviceSpecs), $"Not expected value: {deviceSpecs}"),
    };
}