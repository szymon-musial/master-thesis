using System.Text.Json;
using CloudNative.CloudEvents;
using Configuration;
using Quartz;
using shared;
using shared.Dto;
using shared.Dto.DeviceSpecs;
using shared.Dto.NewTasks;
using Wolverine;

namespace policy_enforcement.Jobs;

public class ScheduledTaskJob(ILogger<ScheduledTaskJob> logger, IMessageBus messageBus, AppConfiguration appConfiguration) : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        JobKey key = context.JobDetail.Key;
        logger.LogInformation($"Executing at {DateTime.UtcNow} (Key: {key.Name}, Group: {key.Group})");

        JobDataMap dataMap = context.JobDetail.JobDataMap;
        var taskStr = dataMap.GetString("task");

        var task = JsonSerializer.Deserialize<NewTask>(taskStr);

        object data = new { err= "Not supported task"};
        var CE_type = "undefined";


        if (task.TaskName.Equals("arima", StringComparison.OrdinalIgnoreCase))
        {
            var rev = appConfiguration.BackedTime - task.FromNow;
            data = new ArimaTask(task.Id, rev, appConfiguration.BackedTime, task.Limit, task.Order_p ?? 5, task.Order_d ?? 1, task.Order_q ?? 0);
            CE_type = ArimaTask.Key;
        }

        if (task.TaskName.Equals("rf", StringComparison.OrdinalIgnoreCase))
        {
            var rev = appConfiguration.BackedTime - task.FromNow;
            data = new RFTask(task.Id, rev, appConfiguration.BackedTime, task.Limit, task.N_in);
            CE_type = RFTask.Key;
        }

        var ce = new CloudEvent()
        {
            Id = Guid.NewGuid().ToString(),
            Type = CE_type,
            Source = new Uri("urn:policy-enforcement"),
            Data = data,
            Time = DateTime.Now,
        };

        await messageBus.PublishAsync(ce);
    }
}