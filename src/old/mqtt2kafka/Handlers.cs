using System.Text;
using Confluent.Kafka;
using Wolverine;

public class Handlers(ILogger<Handlers> logger, IKafkaProducer<string> kafkaProducer) : IWolverineHandler
{
    public async void Handle(StringContract stringContract)
    {
        logger.LogInformation($"Incoming payload {stringContract.JsonMsg}");

        Headers kafkaHeaders = new()
        {
            { "ce_specversion",  Encoding.UTF8.GetBytes("1.0") },
            { "ce_id", Encoding.UTF8.GetBytes(stringContract.UUID) },
            { "ce_type", Encoding.UTF8.GetBytes(stringContract.Type) },
            { "ce_source",  Encoding.UTF8.GetBytes("/edge/ip/ip_todo") },

            // App specific
            { "ce_solved",  Encoding.UTF8.GetBytes("no") },
        };

        var msg = new Message<Null, string>
        {
            Value = stringContract.JsonMsg,
            Headers = kafkaHeaders,
        };

        await kafkaProducer.ProduceAsync(msg);
    }
}