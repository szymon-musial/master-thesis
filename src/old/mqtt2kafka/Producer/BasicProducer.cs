
using Configuration;
using Confluent.Kafka;

public class BasicProducer : IKafkaProducer<string>
{
    private readonly IProducer<Null, string> _producerBuilder;
    private readonly AppConfiguration _appConfiguration;

    public BasicProducer(AppConfiguration appConfiguration)
    {
        _appConfiguration = appConfiguration;

        var config = new ProducerConfig { BootstrapServers = appConfiguration.BootstrapServers };
        _producerBuilder = new ProducerBuilder<Null, string>(config).Build();
    }

    public Task<DeliveryResult<Null, string>> ProduceAsync(Message<Null, string> message, CancellationToken cancellationToken)
        => _producerBuilder.ProduceAsync(_appConfiguration.Topic, message, cancellationToken);
}