using Confluent.Kafka;

public interface IKafkaProducer<TValue>
{
    public Task<DeliveryResult<Null, TValue>> ProduceAsync(Message<Null, TValue> message, CancellationToken cancellationToken = default);
}