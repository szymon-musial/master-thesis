namespace Configuration;

public sealed record class AppConfiguration(
    string Topic,
    string BootstrapServers
)
{
    public static string AppConfigurationPrefix = "My_";
}