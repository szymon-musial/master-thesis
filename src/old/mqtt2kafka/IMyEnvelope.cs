public interface IMyEnvelope
{
    public enum Destinations
    {
        Local_Tasks,
        Remote_Tasks
    }
    public string Topic { get; }
}