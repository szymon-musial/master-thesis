﻿using Configuration;
using Oakton;
using Wolverine;
using Wolverine.MQTT;

return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
            conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
            .AddUserSecrets<Program>()
            .AddCommandLine(args)
    )
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));
        services.AddSingleton(appConfiguration);
        services.AddTransient<IKafkaProducer<string>, BasicProducer>();
    })    
    .UseWolverine((context, opts) =>
    {
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var mqttServer = "localhost";

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(mqttServer);
                });
        });
        opts.ListenToMqttTopic("remote_tasks/#")
            .UseInterop(new MyMqttEnvelopeMapper());
    })
    .RunOaktonCommands(args);
