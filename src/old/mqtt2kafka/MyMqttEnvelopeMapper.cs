using System.Text;
using MQTTnet;
using Wolverine;
using Wolverine.MQTT;

public class MyMqttEnvelopeMapper : IMqttEnvelopeMapper
{
    internal record TypeAndUUID(string Type, string UUID);
    public void MapIncomingToEnvelope(Envelope envelope, MqttApplicationMessage incoming)
    {
        var TypeAndUUID = GetTypeAndUUID(incoming.Topic);

        var contract = new StringContract(
            Encoding.UTF8.GetString(incoming.PayloadSegment),
            TypeAndUUID.UUID,
            TypeAndUUID.Type
        );
        envelope.TopicName = incoming.Topic;
        envelope.Message = contract;
    }

    internal TypeAndUUID GetTypeAndUUID(string topic)
    {
        var topicArr = topic.Split("/");
        return new TypeAndUUID(topicArr[1], topicArr[2]);
    }

    public void MapEnvelopeToOutgoing(Envelope envelope, MqttApplicationMessage outgoing)
    {
        outgoing.ContentType = envelope.ContentType;
        outgoing.Topic = envelope.TopicName;

        if (envelope.Message is StringContract stringContract)
        {
            outgoing.PayloadSegment = Encoding.UTF8.GetBytes(stringContract.JsonMsg);
            return;
        }
        outgoing.PayloadSegment = envelope.Data ?? [];
    }

    public IEnumerable<string> AllHeaders()
    {
        yield break;
    }
}
