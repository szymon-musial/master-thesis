using System.Text;
using CloudNative.CloudEvents;
using CloudNative.CloudEvents.Http;
using Configuration;
using Microsoft.Extensions.Logging;
using Wolverine;

public class MqttHandlers(ILogger<MqttHandlers> logger, AppConfiguration appConfiguration) : IWolverineHandler
{
    private static HttpClient httpClient = new HttpClient()
    {
        Timeout = TimeSpan.FromMinutes(5),
    };
    public async Task<StringContract> HandleAsync(StringContract stringContract)
    {
        logger.LogInformation($"Incoming mqtt payload {stringContract.JsonMsg}");
        CloudEvent cloudEventRequest = new CloudEvent
        {
            Id = stringContract.UUID,
            Type = stringContract.Type,
            Source = new Uri("/edge/ip/ip_todo"),
            Data = "This is CloudEvent data",
        };
        cloudEventRequest["solved"] = "no";

        var httpContent = cloudEventRequest.ToHttpContent(ContentMode.Structured, new CloudNative.CloudEvents.SystemTextJson.JsonEventFormatter());
        var request = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri(appConfiguration.WorkerUrl),
            Content = httpContent
        };

        var response = await httpClient.SendAsync(request);

        logger.LogInformation($"Received response. Content\n{await response.Content.ReadAsStringAsync()}");

        var cloudEvent = await response.ToCloudEventAsync(new CloudNative.CloudEvents.SystemTextJson.JsonEventFormatter());

        ArgumentNullException.ThrowIfNull(cloudEvent, nameof(cloudEvent));
        ArgumentNullException.ThrowIfNull(cloudEvent.Id, nameof(cloudEvent.Id));
        ArgumentNullException.ThrowIfNull(cloudEvent.Type, nameof(cloudEvent.Type));

        var stringContractResp = new StringContract(cloudEvent.Data?.ToString() ?? "", cloudEvent.Id, cloudEvent.Type)
        {
            Destination = IMyEnvelope.Destinations.Results
        };
        return stringContractResp;
    }
}