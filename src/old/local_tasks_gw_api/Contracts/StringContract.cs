public record StringContract(
    string JsonMsg,
    string UUID,
    string Type
) : IMyEnvelope
{
    public IMyEnvelope.Destinations Destination { get; set; }

    public string Topic => $"{Destination.ToString().ToLower()}/{Type}/{UUID}";
}
