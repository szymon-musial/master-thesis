using System.Net;

namespace Configuration;

public sealed record class AppConfiguration(
    string MqttServer = "localhost",
    int MqttPort = 1883,
    string WorkerUrl = "http://localhost:5500"
)
{
    public static string AppConfigurationPrefix = "My_";
}