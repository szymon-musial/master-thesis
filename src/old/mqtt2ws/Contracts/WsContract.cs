public record WsContract (string Message, Dictionary<string, string> Headers);
