public interface IMyEnvelope
{
    public enum Destinations
    {
        Local_Tasks,
        Remote_Tasks,
        Results,
    }
    public string Topic { get; }
}