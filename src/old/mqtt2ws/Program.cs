﻿using Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Oakton;
using Wolverine;
using Wolverine.MQTT;

return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
            conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
            .AddUserSecrets<Program>()
            .AddCommandLine(args)
    )
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));
        services.AddSingleton(appConfiguration);
        services.AddSingleton<IWsService, WsService>();
        services.AddHostedService<WsServiceBackground>();
    })    
    .UseWolverine((context, opts) =>
    {
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var appConfiguration = context.Configuration.Get<AppConfiguration>()!;

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(appConfiguration.MqttServer, appConfiguration.MqttPort);
                });
        });
        opts.ListenToMqttTopic("remote_tasks/#")
            .UseInterop(new MyMqttEnvelopeMapper());
        
        opts.PublishMessagesToMqttTopic<IMyEnvelope>(c => c.Topic)
            .UseInterop(new MyMqttEnvelopeMapper());

    })
    .RunOaktonCommands(args);

