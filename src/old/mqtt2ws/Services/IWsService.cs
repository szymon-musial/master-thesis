using Websocket.Client;

public interface IWsService
{
    public WebsocketClient Client { get; }
    public bool Connected { get; }
    public void Send(string message);
    public void Send(byte[] bytes);
}

