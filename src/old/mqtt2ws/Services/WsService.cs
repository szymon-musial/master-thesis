using System.Net.WebSockets;
using Configuration;
using Microsoft.Extensions.Logging;
using Websocket.Client;

public class WsService(AppConfiguration appConfiguration, ILogger<WebsocketClient> logger) : IWsService
{
    public WebsocketClient Client { get; } = new WebsocketClient(
        new Uri(appConfiguration.WsUri),
        logger,
        async delegate (Uri uri, CancellationToken token) // from decompilations
        {            
            ClientWebSocket client = new ClientWebSocket();
            client.Options.SetRequestHeader("AppId", appConfiguration.AppId);

            // can throw connection exception
            await client.ConnectAsync(uri, token).ConfigureAwait(continueOnCapturedContext: false);
            return client;
        }
    );

    public bool Connected => 
        Client.NativeClient is not null && 
        Client.NativeClient.State == WebSocketState.Open;
    public void Send(string message) => Client.Send(message);
    public void Send(byte[] bytes) => Client.Send(bytes);
}