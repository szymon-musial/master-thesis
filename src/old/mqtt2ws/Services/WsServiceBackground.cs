using System.Text.Json;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Websocket.Client;
using Wolverine;

public class WsServiceBackground(ILogger<WsServiceBackground> logger, IWsService wsService, IMessageBus messageBus) : IHostedService
{
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        wsService.Client.ReconnectTimeout = TimeSpan.FromMinutes(2);
        wsService.Client.ErrorReconnectTimeout = TimeSpan.FromSeconds(2);
        wsService.Client.LostReconnectTimeout = TimeSpan.FromSeconds(2);

        wsService.Client.ReconnectionHappened.Subscribe(info =>
            logger.LogInformation($"Reconnection happened, type: {info.Type}"));

        wsService.Client.MessageReceived.Subscribe(ProcessIncomingWsMessageAsync);

        await wsService.Client.Start();

    }


    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    public async void ProcessIncomingWsMessageAsync(ResponseMessage responseMessage)
    {
        logger.LogInformation($"Message received: {responseMessage}");

        try
        {
            ArgumentException.ThrowIfNullOrEmpty(responseMessage.Text, nameof(responseMessage.Text));
            var wsContract = JsonSerializer.Deserialize<WsContract>(responseMessage.Text);

            if (wsContract is null)
            {
                throw new ArgumentException(nameof(wsContract));
            }

            string? uuid = wsContract.Headers.GetValueOrDefault("ce_id");
            string? type = wsContract.Headers.GetValueOrDefault("ce_type");

            ArgumentException.ThrowIfNullOrEmpty(uuid, nameof(uuid));
            ArgumentException.ThrowIfNullOrEmpty(type, nameof(type));

            var stringContract = new StringContract(wsContract.Message, uuid, type)
            {
                Destination = IMyEnvelope.Destinations.Results
            };

            await messageBus.PublishAsync(stringContract);

        }
        catch (Exception ex)
        {
            logger.LogError(ex.ToString());
        }
    }
}