using System.Text.Json;
using Microsoft.Extensions.Logging;
using Wolverine;

public class Handlers(ILogger<Handlers> logger, IWsService wsService) : IWolverineHandler
{
    public StringContract? Handle(StringContract stringContract)
    {
        logger.LogInformation($"Incoming payload {stringContract.JsonMsg}");

        if(!wsService.Connected)
        {
            stringContract.Destination = IMyEnvelope.Destinations.Local_Tasks;
            logger.LogWarning($"Closed remote ws, sending {stringContract.UUID} to local");
            return stringContract;
        }

        Dictionary<string, string> headers = new()
        {
            { "ce_specversion",  "1.0" },
            { "ce_id", stringContract.UUID },
            { "ce_type", stringContract.Type },
            { "ce_source",  "/edge/ip/ip_todo" },

            // App specific
            { "ce_solved",  "no" },
        };

        var wsContract = new WsContract(stringContract.JsonMsg, headers);
        wsService.Send(JsonSerializer.SerializeToUtf8Bytes(wsContract));
        return null;
    }
}