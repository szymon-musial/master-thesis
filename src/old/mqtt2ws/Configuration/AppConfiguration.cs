using System.Net;

namespace Configuration;

public sealed record class AppConfiguration(
    string MqttServer = "localhost",
    int MqttPort = 1883,
    string WsUri = "ws://localhost:5000/ws"
)
{
    public static string AppConfigurationPrefix = "My_";
    public string AppId { get; set; } =  Dns.GetHostName();
}