import paho.mqtt.client as mqtt
import json
import logging
import os

class AppSender:
    def __init__(self, broker, port=1883):
        self.broker = broker
        self.port = port
        self.client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id=f"task_generator{os.getpid()}")
        print(f"Broker host: {broker} on port {port}")
        self.client.connect(self.broker, self.port)

    def send(self, message_obj, message_id):

        # Serializacja obiektu wiadomości do JSON-a
        message_json = json.dumps(message_obj.__dict__)

        topic = f"tasks/{message_obj.__class__.__name__.lower()}/{message_id}"
        result = self.client.publish(topic, message_json)
        result.wait_for_publish()

        if result.rc != mqtt.MQTT_ERR_SUCCESS:
            logging.error(f"Nie udało się wysłać wiadomości: '{message_json}' na kanał: '{topic}'")
        else:
            logging.info(f"Wysłano wiadomość: '{message_json}' na kanał: '{topic}'")