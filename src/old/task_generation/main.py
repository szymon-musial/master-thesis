import logging
import uuid
import os
import time

from AppSender import AppSender
from MessageObject import MessageObject

# Przykład użycia klasy AppSender
if __name__ == "__main__":
    # Ustawienia brokera
    broker_address = os.environ.get("broker", "localhost")  # lub adres IP brokera MQTT
    
    # Konfiguracja logowania
    logging.basicConfig(level=logging.DEBUG)
    
    # Tworzenie instancji AppSender
    sender = AppSender(broker_address)
    
    while True:
        # Przykładowa wiadomość i identyfikator
        message_obj = MessageObject("Testowa wiadomość")
        message_id = uuid.uuid4()
        
        # Wysyłanie wiadomości
        time.sleep(float(os.environ.get("delay", 1.0)))
        sender.send(message_obj, message_id)
