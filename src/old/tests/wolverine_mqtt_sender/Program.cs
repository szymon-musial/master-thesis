﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Oakton;
using Wolverine;
using Wolverine.MQTT;

return await Host.CreateDefaultBuilder()
    .UseWolverine((context, opts) =>
    {
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var mqttServer = "localhost";

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(mqttServer);
                });
        });
        opts.PublishAllMessages()
            .ToMqttTopic("app/outgoing");
        
        opts.ListenToMqttTopic("app/#");
    })
    .ConfigureServices(services => services.AddHostedService<TestSender>())
    .RunOaktonCommands(args);
