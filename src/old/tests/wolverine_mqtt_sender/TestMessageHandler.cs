using Wolverine;

public class TestMessageHandler
{
    public void Handle(TestMsg message, Envelope envelope)
    {
        Console.WriteLine("I got a message!");
    }
}