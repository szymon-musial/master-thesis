using Microsoft.Extensions.Hosting;
using Wolverine;

public class TestSender(IMessageBus messageBus) : IHostedService
{
    public async Task StartAsync(CancellationToken cancellationToken)
    {
        await messageBus.SendAsync(new TestMsg());
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
}