﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Config;

await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
        conf.AddEnvironmentVariables(prefix: KNAppConfiguration.AppConfigurationPrefix)
        .AddUserSecrets<Program>()
        .AddCommandLine(args)
    )    
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var c = hostBuilderContext.Configuration;
        var appConfiguration = new KNAppConfiguration(
            RemoteTriggerName: c[nameof(KNAppConfiguration.RemoteTriggerName)] ?? "kn-http-to-kafka-cloud",
            MaxTaskSolverReplicas:int.Parse(c[nameof(KNAppConfiguration.MaxTaskSolverReplicas)] ?? "5"),
            K8ApiHost: c[nameof(KNAppConfiguration.K8ApiHost)] ?? "http://localhost:8001",
            TokenPath: c[nameof(KNAppConfiguration.TokenPath)] ?? @"/var/run/secrets/kubernetes.io/serviceaccount/token",
            PromHost: c[nameof(KNAppConfiguration.PromHost)] ?? @"http://kube-prometheus-stack-prometheus.observability:9090"
            // kubectl create token admin-user -n kubernetes-dashboard > /home/szymon/tmp_token
        );
        services.AddSingleton(appConfiguration);
        
        services.AddSingleton<K8PatchService>();
        services.AddTransient<PrometheusQueryService>();
        services.AddHostedService<DestChangerWatcherBackgroundService>();
        
    })
.RunConsoleAsync();
