#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM docker.io/alpine:3.19 AS base
ENV ASPNETCORE_HTTP_PORTS=80
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine AS build
# https://github.com/dotnet/dotnet-docker/issues/4129
# https://pkgs.alpinelinux.org/contents?file=ld-linux-x86-64.so.2
RUN apk add --no-cache build-base zlib-dev
WORKDIR /src
COPY ["./kn-dispatcher.csproj", "./kn-dispatcher.csproj"]
RUN dotnet restore "./kn-dispatcher.csproj"
COPY . .
RUN dotnet build "./kn-dispatcher.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "./kn-dispatcher.csproj" --runtime linux-musl-x64 -c Release -o /app/publish 

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["/app/kn-dispatcher"]

# Error loading shared library ld-linux-x86-64.so.2: No such file or directory (needed by kn-dispatcher)