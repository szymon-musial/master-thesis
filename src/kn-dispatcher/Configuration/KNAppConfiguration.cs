using System.Net;

namespace Config;

public sealed record class KNAppConfiguration(
    string RemoteTriggerName = "kn-http-to-kafka-cloud",
    int MaxTaskSolverReplicas = 5,
    string K8ApiHost = "http://localhost:8001",
    string TokenPath = @"/var/run/secrets/kubernetes.io/serviceaccount/token",
    string PromHost = @"http://kube-prometheus-stack-prometheus.observability:9090"
)
{
    public static string AppConfigurationPrefix = "My_";
}
