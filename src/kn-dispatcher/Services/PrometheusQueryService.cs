using System.Text.Json;
using Config;

public class PrometheusQueryService(KNAppConfiguration appConfiguration)
{
    private static readonly HttpClient _httpClient = new HttpClient();

    public async Task<int> GetReplicaCountAsync()
    {
        string url = appConfiguration.PromHost + "/api/v1/query?query=kube_deployment_status_replicas{deployment=~\"tasks-solver-.*\"}";

        var response = await _httpClient.GetAsync(url);
        // if (response.IsSuccessStatusCode)
        // {
            var content = await response.Content.ReadAsStringAsync();
            return await ProcessResp(content);

//        }

    }

    public async Task<int> FakeGetReplicaCountAsync()
    {
        string content = """
        {
            "status": "success",
            "data": {
                "resultType": "vector",
                "result": [
                {
                    "metric": {
                    "__name__": "kube_deployment_status_replicas",
                    "container": "kube-state-metrics",
                    "deployment": "tasks-solver-00001-deployment",
                    "endpoint": "http",
                    "instance": "10.233.65.114:8080",
                    "job": "kube-state-metrics",
                    "namespace": "default",
                    "pod": "kube-prometheus-stack-kube-state-metrics-6dcd966b95-ff657",
                    "service": "kube-prometheus-stack-kube-state-metrics"
                    },
                    "value": [1721135789.064, "0"]
                }
                ]
            }
        }
        """;
        return await ProcessResp(content);

    }

    static readonly JsonSerializerOptions options  = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        TypeInfoResolver = PrometheusResponseGenerationContext.Default
    };

    public async Task<int> ProcessResp(string content)
    {
        var prometheusResponse = JsonSerializer.Deserialize<PrometheusResponse>(content,  options);
        // if (prometheusResponse != null && prometheusResponse.Data != null && prometheusResponse.Data.Result.Count > 0)
        // {
        var value = prometheusResponse.Data.Result.OrderBy(r => r.Metric.Deployment).First().Value[1].ToString();
        return int.Parse(value);
        //            }

    }
}
