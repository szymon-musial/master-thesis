using System.Net.Http.Headers;
using System.Text;
using Config;
using Microsoft.Extensions.Logging;

public class K8PatchService(KNAppConfiguration appConfiguration, ILogger<K8PatchService> logger)
{
    const string GROUP = "eventing.knative.dev";
    const string VERSION = "v1";
    const string NAMESPACE = "default";
    const string RESOURCETYPE = "triggers";


    private readonly HttpClient httpClient= new(new HttpClientHandler
    {
        ServerCertificateCustomValidationCallback = (request, cert, chain, errors) =>
        {
            logger.LogInformation("SSL error skipped");
            return true;
        }
    })
    {
        // https://kubernetes.io/docs/reference/using-api/api-concepts/
        BaseAddress = new Uri(appConfiguration.K8ApiHost + $"/apis/{GROUP}/{VERSION}/namespaces/{NAMESPACE}/{RESOURCETYPE}/")
    };

    public async void PathSubscriberInTrigger(string name, string apiVersion = "serving.knative.dev/v1", string kind = "Service")
    {
        var patchJson = $$"""
        {
            "spec": {
                "subscriber": {
                    "ref": {
                        "apiVersion": "{{apiVersion}}",
                        "kind": "{{kind}}",
                        "name": "{{name}}"
                    }
                }
            }
        }
        """;

        var requestMessage = new HttpRequestMessage
        {
            Method = HttpMethod.Patch,
            Content =  new StringContent(patchJson, Encoding.UTF8, "application/merge-patch+json"),
            RequestUri = new Uri(httpClient.BaseAddress + "tasks-solver"),
        };

        var token = GetJWTIfExists();
        if(token is not null)
        {
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }


        var resp = await httpClient.SendAsync(requestMessage);
        logger.LogInformation($"Patch resp: {await resp.Content.ReadAsStringAsync()}");
    }

    public void RouteExternal() => PathSubscriberInTrigger(appConfiguration.RemoteTriggerName, "v1");
    public void RouteInternal() => PathSubscriberInTrigger("tasks-solver", "serving.knative.dev/v1");

    public string? GetJWTIfExists()
    {
        if(!File.Exists(appConfiguration.TokenPath))
        {
            return null;
        }
        using StreamReader streamReader = new(appConfiguration.TokenPath, Encoding.UTF8);
        return streamReader.ReadToEnd();
    }
}