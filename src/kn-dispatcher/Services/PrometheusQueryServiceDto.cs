using System.Text.Json.Serialization;

public record PrometheusResponse(string Status, Data Data);

public record Data(string ResultType, List<Result> Result);

public record Result(Metric Metric, List<object> Value);

public record Metric(
    // string __name__, 
    // string Container, 
    string Deployment //, 
    // string Endpoint, 
    // string Instance, 
    // string Job, 
    // string Namespace, 
    // string Pod, 
    // string Service
);



[JsonSourceGenerationOptions(WriteIndented = true)]
[JsonSerializable(typeof(PrometheusResponse))]
internal partial class PrometheusResponseGenerationContext : JsonSerializerContext;