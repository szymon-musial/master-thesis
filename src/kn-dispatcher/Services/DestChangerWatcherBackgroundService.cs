using System.Diagnostics;
using Config;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class DestChangerWatcherBackgroundService(
    PrometheusQueryService prometheusQueryService,
    ILogger<DestChangerWatcherBackgroundService> logger,
    KNAppConfiguration appConfiguration,
    K8PatchService k8PatchService
    ) : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                int replicaCount = await prometheusQueryService.GetReplicaCountAsync();
                var routeExternal = replicaCount >= appConfiguration.MaxTaskSolverReplicas;
                logger.LogInformation($" Route external {routeExternal}. Current replicas: {replicaCount}");

                if (routeExternal)
                {
                    k8PatchService.RouteExternal();
                }
                else
                {
                    k8PatchService.RouteInternal();
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred: {ex.Message}");
            }
            finally
            {
                logger.LogInformation($"Time from start {DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime()}");
                logger.LogInformation($"CPU time {Process.GetCurrentProcess().TotalProcessorTime} used mem {GC.GetTotalMemory(true)}");
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}
