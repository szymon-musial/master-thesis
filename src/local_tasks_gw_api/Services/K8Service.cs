using Configuration;
using k8s;
using k8s.Models;
using Microsoft.Extensions.Logging;

public class K8Service(IKubernetes client, ILogger<K8Service> logger, AppConfiguration appConfiguration)
{
    public async Task ScaleDeploymentAsync(string deployment, int replicas, bool wait = true, string ns = "default", CancellationToken cancellationToken = default)
    {
        if(appConfiguration.DisableK8)
        {
            return;
        }

        var patch = new V1Patch(new { spec = new { replicas } }, V1Patch.PatchType.MergePatch);
        client.AppsV1.PatchNamespacedDeploymentScale(patch, deployment, ns);

        if (await CheckForDesiredReplicas(deployment, replicas, ns, cancellationToken))
        {
            return;
        }

        if (wait)
        {
            await BlockUntilReachReplicasAsync(deployment, replicas, ns, cancellationToken);
        }
    }

    private async Task BlockUntilReachReplicasAsync(string deployment, int replicas, string ns = "default", CancellationToken cancellationToken = default)
    {
        var deploymentsInNs = client.AppsV1.ListNamespacedDeploymentWithHttpMessagesAsync(namespaceParameter: ns, watch: true, cancellationToken: cancellationToken);
        await foreach (var (_, item) in deploymentsInNs.WatchAsync<V1Deployment, V1DeploymentList>(cancellationToken: cancellationToken))
        {
            if (item.Metadata.Name == deployment)
            {
                logger.LogInformation($"Replicas {item.Metadata.Name} {item.Spec.Replicas}, desired {replicas}");
                if (item.Spec.Replicas == replicas)
                {
                    return;
                }
            }
        }
    }

    private async Task<bool> CheckForDesiredReplicas(string deployment, int replicas, string ns = "default", CancellationToken cancellationToken = default)
    {
        var resp = await client.AppsV1.ReadNamespacedDeploymentScaleAsync(name: deployment, namespaceParameter: ns, cancellationToken: cancellationToken);
        logger.LogInformation($"Replica {deployment} reached desired {replicas}");
        return resp.Spec.Replicas == replicas;
    }
}