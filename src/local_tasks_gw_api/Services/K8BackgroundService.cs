using Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class K8BackgroundService(K8Service k8Service, ILogger<K8BackgroundService> logger, Workers workers, AppConfiguration appConfiguration) : IHostedService
{
    public Task StartAsync(CancellationToken cancellationToken)
    {
        if(appConfiguration.DisableK8)
        {
            return Task.CompletedTask;
        }

        foreach (var worker in workers.AsDict())
        {
            worker.Value.TimerToScaleDown.Elapsed += async (s, e) =>
            {
                await k8Service.ScaleDeploymentAsync(worker.Value.Deployment.ToLower(), 0, cancellationToken: cancellationToken);
                logger.LogInformation($"Trigger fired, {worker.Value.Deployment} scaled to 0");
            };
            worker.Value.TimerToScaleDown.Start();
        }
        return Task.CompletedTask;
    }

    public Task StopAsync(CancellationToken cancellationToken)
        => Task.CompletedTask;

}