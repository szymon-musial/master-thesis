﻿using CloudNative.CloudEvents;
using Configuration;
using k8s;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Oakton;
using Polly;
using Polly.Extensions.Http;
using shared;
using shared.Interop;
using Wolverine;
using Wolverine.MQTT;

return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
            conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
            .AddUserSecrets<Program>()
            .AddCommandLine(args)
            .AddJsonFile("./workers.json")
    )
    .ConfigureServices((hostBuilderContext, services) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));
        services.AddSingleton(appConfiguration);
        
        var workersConfiguration = hostBuilderContext.Configuration.Get<Workers>()
            ?? throw new ArgumentNullException(nameof(Workers));
        services.AddSingleton(workersConfiguration);

        // Load kubernetes configuration
        var kubernetesClientConfig = KubernetesClientConfiguration.BuildConfigFromConfigFile(Environment.GetEnvironmentVariable("KUBECONFIG"));
        // Register Kubernetes client interface as sigleton
        services.AddSingleton<IKubernetes>(_ => new Kubernetes(kubernetesClientConfig));

        services.AddSingleton<K8Service>();

        services.AddHttpClient("polly", c => c.Timeout = Timeout.InfiniteTimeSpan)
        .AddPolicyHandler(GetRetryPolicy());

        if(!appConfiguration.DisableK8)
        {
            services.AddHostedService<K8BackgroundService>();
        }
    })    
    .UseWolverine((context, opts) =>
    {
        opts.DefaultExecutionTimeout = Timeout.InfiniteTimeSpan;
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var appConfiguration = context.Configuration.Get<AppConfiguration>()!;

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(appConfiguration.MqttServer, appConfiguration.MqttPort);
                });
        });
        opts.ListenToMqttTopic($"{Destinations.Local_Tasks.ToString().ToLower()}/#") // można dać tylko na nowe taski
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.PublishMessagesToMqttTopic<CloudEvent>(c => $"results/{c.Type}/{c.Id}")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());
    })
    .RunOaktonCommands(args);


static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
{
    return HttpPolicyExtensions
        .HandleTransientHttpError()
        .OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
        .WaitAndRetryAsync(10, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
}