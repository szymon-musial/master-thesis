using CloudNative.CloudEvents;
using CloudNative.CloudEvents.Http;
using Microsoft.Extensions.Logging;
using Wolverine;
using CloudNative.CloudEvents.SystemTextJson;
using System.Net;
using shared;
using System.Diagnostics;
public class MqttHandlers(ILogger<MqttHandlers> logger, K8Service k8Service, Workers workers, IHttpClientFactory httpClientFactory) : IWolverineHandler
{
    public const string SupportedType = "NewTask";

    public async Task<CloudEvent?> HandleAsync(CloudEvent cloudEventRequest, CancellationToken cancellationToken)
    {
        var stopWatch = Stopwatch.StartNew();

        var rootType = cloudEventRequest.GetRootType();
        if (rootType != SupportedType)
        {
            return null;
        }
        var httpClient = httpClientFactory.CreateClient("polly");

        var worker = workers.AsDict().GetValueOrDefault(cloudEventRequest.Type);
        ArgumentOutOfRangeException.ThrowIfEqual(worker, null, "CloudEvent type out of range");

        worker?.TimerToScaleDown.Stop();

        await k8Service.ScaleDeploymentAsync(worker.Deployment.ToLower(), 1, cancellationToken: cancellationToken);

        var httpContent = cloudEventRequest.ToHttpContent(ContentMode.Binary, new JsonEventFormatter());
        var request = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri(worker.Url),
            Content = httpContent
        };

        var response = await httpClient.SendAsync(request, cancellationToken);

        stopWatch.Stop();
        logger.LogInformation($"Received response after {stopWatch.Elapsed}. Content\n{await response.Content.ReadAsStringAsync(cancellationToken)}");

        worker?.TimerToScaleDown.Start();

        if (response.StatusCode == HttpStatusCode.Accepted)
        {
            return null;
        }

        var cloudEventResp = await response.ToCloudEventAsync(new JsonEventFormatter());

        ArgumentNullException.ThrowIfNull(cloudEventResp, nameof(cloudEventResp));
        ArgumentNullException.ThrowIfNull(cloudEventResp.Id, nameof(cloudEventResp.Id));
        ArgumentNullException.ThrowIfNull(cloudEventResp.Type, nameof(cloudEventResp.Type));

        return cloudEventResp;
    }
}