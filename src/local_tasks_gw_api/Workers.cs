public record Workers(IEnumerable<Worker> workers)
{
    public Dictionary<string, Worker> AsDict()
        => workers
            .OrderBy(x => x.TaskType)
            .DistinctBy(i => i.TaskType)
            .ToDictionary(x => x.TaskType, x => x);
}

public record Worker(string TaskType, int ScaleDownSec, string Url, string Deployment)
{
    public System.Timers.Timer TimerToScaleDown = new()
    {
        Interval = TimeSpan.FromSeconds(ScaleDownSec).TotalMilliseconds,
        AutoReset = false,
        Enabled = true,
    };
};