cd ..

docker build -f ./local_tasks_gw_api/dockerfile -t docker.io/elomelo320/local_tasks_gw_api .    &
docker build -f ./measurements/dockerfile       -t docker.io/elomelo320/measurements .          &
docker build -f ./policy-enforcement/dockerfile -t docker.io/elomelo320/policy-enforcement .    &
docker build -f ./dispatcher/dockerfile         -t docker.io/elomelo320/dispatcher .            &
docker build -f ./sync/dockerfile               -t docker.io/elomelo320/sync .                  &

jobs

docker image ls | grep -e local_tasks_gw_api -e measurements -e policy-enforcement -e dispatcher -e sync
