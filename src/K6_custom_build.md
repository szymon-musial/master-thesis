https://grafana.com/docs/k6/latest/extensions/build-k6-binary-using-docker/


```bash
docker run --rm -u "$(id -u):$(id -g)" -v "${PWD}:/xk6" grafana/xk6 build \
  --with github.com/gpiechnik2/xk6-httpagg \
  --with github.com/grafana/xk6-ts \
  --with github.com/grafana/xk6-dashboard \
  --with github.com/mostafa/xk6-kafka \
  --with github.com/pmalhaire/xk6-mqtt \
  --with github.com/grafana/xk6-sql \
  --with github.com/Juandavi1/xk6-prompt
```