namespace shared.Dto.NewTasks;

public record RFTask(string Id, DateTimeOffset Timestamp, DateTimeOffset TimestampNow, int? Limit,
    int? N_in)
{
    public double Time => Timestamp.ToUnixTimeSeconds();
    public double TimeNow => TimestampNow.ToUnixTimeSeconds();
    public string TaskName => "RF";
    public static readonly string Key = $"{nameof(NewTask)}{Const.CETypeDivider}RF";
}