namespace shared.Dto.NewTasks;

public record NewTask(string Id, string Cron, string TaskName, TimeSpan FromNow, int? Limit,
    int? Order_p, int? Order_d, int? Order_q, // to arima
    int? N_in) // to rf

{
    public const string Key = nameof(NewTask);
    public const string PolicyKey = $"{Key}Policy";
}