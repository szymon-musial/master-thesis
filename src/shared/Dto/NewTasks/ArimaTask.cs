namespace shared.Dto.NewTasks;

public record ArimaTask(string Id, DateTimeOffset Timestamp, DateTimeOffset TimestampNow, int? Limit,
    int Order_p = 5, int Order_d = 1, int Order_q = 0)
{
    public double Time => Timestamp.ToUnixTimeSeconds();
    public double TimeNow => TimestampNow.ToUnixTimeSeconds();

    public string TaskName => "arima";
    public static readonly string Key = $"{nameof(NewTask)}{Const.CETypeDivider}Arima";
}