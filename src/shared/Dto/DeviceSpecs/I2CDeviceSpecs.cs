namespace shared.Dto.DeviceSpecs;

public record I2CDeviceSpecs(string DevType, int DevAddress, int Bitrate) : IDeviceSpecs
{
    public const string Key = "i2c";
}
