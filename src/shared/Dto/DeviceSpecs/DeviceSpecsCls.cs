namespace shared.Dto.DeviceSpecs;

public record DeviceSpecsCls(string DevType) : IDeviceSpecs;