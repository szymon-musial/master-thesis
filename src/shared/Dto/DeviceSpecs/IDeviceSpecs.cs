namespace shared.Dto.DeviceSpecs;

public interface IDeviceSpecs
{
    public string DevType { get; }
}