namespace shared.Dto.DeviceSpecs;

public record ModBusSpecs(string DevType, int Address, int Bitrate) : IDeviceSpecs
{
    public const string Key = "modbus";
}