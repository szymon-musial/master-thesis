using shared.Dto.DeviceSpecs;

namespace shared.Dto;

public record NewPolicy(string Id, string Cron, string MeasurementType, DeviceSpecsCls Dev):
    NewPolicy<DeviceSpecsCls>(Id, Cron, MeasurementType, Dev);

public record NewPolicy<T>(string Id, string Cron, string MeasurementType, T Dev) where T : IDeviceSpecs
{
    public const string Key = nameof(NewPolicy);
}
// Cron https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontriggers.html
