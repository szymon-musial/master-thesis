namespace shared.Dto;

public record NewDispatchPolicy(string Field, string Value)
{
    public const string Key = nameof(NewDispatchPolicy);
}
// Cron https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontriggers.html
