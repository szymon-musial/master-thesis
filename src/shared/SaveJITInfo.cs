using System.Diagnostics;
using System.Runtime;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class SaveJITInfo(ILogger<SaveJITInfo> logger) : BackgroundService
{
    const string subDir = "csvs";
    private string FileName = DateTimeOffset.Now.ToString("yyyy-MM-ddTHH:mm:ssK") + ".csv" ;
    int delayMs = 1000;

    readonly string prefix = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Directory.CreateDirectory(subDir);
        var path = Path.Combine(subDir, $"{prefix}_{FileName}");
        var fs = File.Create(path);
        logger.LogWarning($"Created filename {path}");

        var textWriter = new StreamWriter(fs)
        {
            AutoFlush = true,
        };

        await textWriter.WriteLineAsync("cpu_time_ms,jit_compilation_time_ms,jit_compiled_il_byes, jit_method_count, compilation_time_divied_by_cpu_time");

        while (!stoppingToken.IsCancellationRequested)
        {
            var pt = Process.GetCurrentProcess().TotalProcessorTime;
            await textWriter.WriteLineAsync($"{pt.TotalMilliseconds},{JitInfo.GetCompilationTime(false).TotalMilliseconds},{JitInfo.GetCompiledILBytes(false)},{JitInfo.GetCompiledMethodCount(false)},{JitInfo.GetCompilationTime(false).TotalMilliseconds/pt.TotalMilliseconds}");

            //logger.LogWarning($"Proc time: {pt.TotalMilliseconds} \nJIT Time: {JitInfo.GetCompilationTime(false)}, ILBytes: {JitInfo.GetCompiledILBytes(false)}, MethodCount: {JitInfo.GetCompiledMethodCount(false)}, comp / proc time 0-1: {JitInfo.GetCompilationTime(false).TotalMilliseconds/pt.TotalMilliseconds}");
            await Task.Delay(TimeSpan.FromMilliseconds(delayMs), stoppingToken);
        }

        await textWriter.FlushAsync();
        fs.Close();
    }
}


// https://stackoverflow.com/questions/5768727/measuring-jit-time-of-a-net-application
// https://learn.microsoft.com/en-us/dotnet/framework/debug-trace-profile/performance-counters