using Wolverine;

namespace shared;

public static class MyMessageExtensions
{
    public static RoutedToEndpointMessage<T> ToKafkaTopicDestination<T>(this T message, string topic, DeliveryOptions? options = null)
        => new(new Uri($"kafka://topic/{topic}"), message, options);
    
    public static RoutedToEndpointMessage<T> ToMqttTopicDestination<T>(this T message, string topic, DeliveryOptions? options = null)
        => new(new Uri($"mqtt://topic/{topic}"), message, options);
}