namespace shared.Entities;

public abstract class Entity
{
    public int Id { get; set; }
    public int Time { get; set; }
    public DateTimeOffset Timestamp => DateTimeOffset.FromUnixTimeSeconds(Time);
}