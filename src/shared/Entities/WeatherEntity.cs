namespace shared.Entities;

public class WeatherEntity : Entity
{
    public int Status { get; set; }
    public int Direction { get; set; }
    public int RainfallCurrent { get; set; }
    public int Temperature { get; set; }
    public int RainfallHour { get; set; }
    public int Humidity { get; set; }
    public int Speed { get; set; }
    public bool Sync { get; set; }
    public string StationID { get; set; }
}
