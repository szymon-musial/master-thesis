using System.ComponentModel.DataAnnotations.Schema;

namespace shared.Entities;

[Table("neosentio")]
public class EmbankmentEntity : Entity
{
    [Column("sensorID")]
    public string SensorID { get; set; }

    [Column("measurement")]
    public int Measurement { get; set; }

    [Column("sync")]
    public bool Sync { get; set; }

    [Column("mField")]
    public int MField { get; set; }

    [Column("rField")]
    public int RField { get; set; }

    [Column("line")]
    public int Line { get; set; }

    [Column("hType")]
    public int HType { get; set; }

    [Column("hCondition")]
    public int HCondition { get; set; }

    [Column("hLength")]
    public int HLength { get; set; }

    [Column("pStatus")]
    public int PStatus { get; set; }

    [Column("pType")]
    public int PType { get; set; }

    [Column("stationID")]
    public string StationID { get; set; }
}
