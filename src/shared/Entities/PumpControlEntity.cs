using System.ComponentModel.DataAnnotations.Schema;

namespace shared.Entities;

public class PumpControlEntity : Entity
{
    [Column("ASP_Level1_PV")]
    public float AspLevel1PV { get; set; }
    
    [Column("ASP_Level_PV")]
    public float AspLevelPV { get; set; }
    
    [Column("ASP_Level_SP")]
    public float AspLevelSP { get; set; }
    
    [Column("ASP_Level2_PV")]
    public float AspLevel2PV { get; set; }
    
    public int PowerUsage { get; set; }
    
    [Column("pump_1")]
    public int Pump1 { get; set; }
    
    [Column("pump_2")]
    public int Pump2 { get; set; }

    public bool Sync { get; set; }
    
    public string StationID { get; set; }
}
