using System.Net;
using CloudNative.CloudEvents;

namespace Configuration;

public sealed record class AppConfiguration(
    string MqttServer = "localhost",
    int MqttPort = 1883,
    string BootstrapServers = "localhost:29092",
    string ISMOPPath = "/home/szymon/ismop2.db",
    int BackInTimeMonths = 87,
    string SyncDbPath = "/home/szymon/sync.db",
    string KNTopic = "knative-broker-default-my-kafka-broker",
    string TaskSolverUrl = "http://localhost:7000",
    string PgConnectionString = "Host=localhost;Database=postgres;Username=postgres;Password=root;Include Error Detail=true",
    bool DisableK8 = false,
    bool ForceSendToKnTopic = false,
    string SensorIdRegex = @"^3_[0-1]\d?_.*",
    string ProcMeminfoPath = @"/proc/meminfo",
    string County = "szymon",
    string? ForceTaskTarget = null,
    bool SkipMigration = false,
    bool ForceStructuredCE = false
)
{
    public static string AppConfigurationPrefix = "My_";
    public string StationId { get; init; } = Dns.GetHostName();
    public DateTimeOffset BackedTime => DateTimeOffset.UtcNow.AddMonths(BackInTimeMonths * -1);
    public ContentMode CEContentMode => ForceStructuredCE ? ContentMode.Structured : ContentMode.Binary;
}
