using CloudNative.CloudEvents;

namespace shared;

public static class CloudEventExtensions
{
    public const string targetKeyName = "target";

    public static void SetTarget(this CloudEvent cloudEvent, Uri dest)
        => SetTarget(cloudEvent, dest.ToString());
    
    public static Uri GetTarget(this CloudEvent cloudEvent)
        => new(GetTargetStr(cloudEvent));

    public static string GetRootType(this CloudEvent cloudEvent)
    {
        ArgumentException.ThrowIfNullOrEmpty(cloudEvent.Type);
        return cloudEvent.Type.Split(Const.CETypeDivider)[0];
    }

    private static void SetTarget(this CloudEvent cloudEvent, string target)
    {
        cloudEvent[targetKeyName] = target;
    }
    
    private static string GetTargetStr(this CloudEvent cloudEvent)
    {
        return cloudEvent[targetKeyName] as string;
    }
}
