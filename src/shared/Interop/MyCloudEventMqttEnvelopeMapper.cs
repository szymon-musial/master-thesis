using System.Text;
using CloudNative.CloudEvents;
using CloudNative.CloudEvents.Mqtt;
using CloudNative.CloudEvents.SystemTextJson;
using MQTTnet;
using MQTTnet.Protocol;
using Wolverine;
using Wolverine.MQTT;

namespace shared.Interop;

public class MyCloudEventMqttEnvelopeMapper : IMqttEnvelopeMapper
{
    public void MapIncomingToEnvelope(Envelope envelope, MqttApplicationMessage incoming)
    {
        var ce = incoming.ToCloudEvent(new JsonEventFormatter());

        if(!Guid.TryParse(ce.Id, out var id))
        {
            id = envelope.Id;
        }

        envelope.TopicName = incoming.Topic;
        envelope.Message = ce;
        envelope.Id =  id;
    }

    public void MapEnvelopeToOutgoing(Envelope envelope, MqttApplicationMessage outgoing)
    {
        outgoing.ContentType = envelope.ContentType;
        outgoing.Topic = envelope.TopicName;
        outgoing.QualityOfServiceLevel = MqttQualityOfServiceLevel.AtLeastOnce;

        if (envelope.Message is CloudEvent ce)
        {
            if(string.IsNullOrEmpty(outgoing.Topic))
            {
                outgoing.Topic = TryToFillTopicBasedOnCEType(ce);
            }

            if(string.IsNullOrEmpty(outgoing.Topic))
            {
                outgoing.Topic = TryToFillBasedOnDestinationAndCE(envelope, ce);
            }

            var res = ce.ToMqttApplicationMessage(ContentMode.Structured, new JsonEventFormatter(), null);
            outgoing.PayloadSegment = res.PayloadSegment;
            return;
        }
        outgoing.PayloadSegment = envelope.Data ?? [];
    }

    private static readonly Dictionary<string, string> TypeTopicMap = new()
    {
        { "NewPolicy", "policy" },
        { "SolvedTask", "results" },
    };

    private static string? TryToFillTopicBasedOnCEType(CloudEvent ce)
    {
        if(ce.Type is null)
        {
            return null;
        }
        if(!TypeTopicMap.TryGetValue(ce.GetRootType(), out var rootTopic))
        {
            return null;
        }
        return $"{rootTopic}/{ce.Type}/{ce.Id}";
    }

    private static string? TryToFillBasedOnDestinationAndCE(Envelope envelope, CloudEvent ce)
    {
        if (envelope.Destination is null)
        {
            return null;
        }

        var path = envelope.Destination.GetComponents(UriComponents.Path, UriFormat.UriEscaped);
        return $"{path}/{ce.Type}/{ce.Id}";
    }

    public IEnumerable<string> AllHeaders()
    {
        yield break;
    }
}