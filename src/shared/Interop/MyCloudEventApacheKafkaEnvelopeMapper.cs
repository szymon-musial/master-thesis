using System.Text;
using CloudNative.CloudEvents;
using CloudNative.CloudEvents.Kafka;
using CloudNative.CloudEvents.SystemTextJson;
using Configuration;
using Confluent.Kafka;
using Wolverine;
using Wolverine.Kafka;

public class MyCloudEventApacheKafkaEnvelopeMapper(AppConfiguration appConfiguration) : IKafkaEnvelopeMapper
{
    public IEnumerable<string> AllHeaders() => [];

    public void MapEnvelopeToOutgoing(Envelope envelope, Message<string, string> outgoing)
    {
        if (envelope.Message is CloudEvent ce)
        {
            var res = ce.ToKafkaMessage(appConfiguration.CEContentMode, new JsonEventFormatter());
            outgoing.Headers = res.Headers;
            outgoing.Key = res.Key;
            outgoing.Timestamp = res.Timestamp;
            outgoing.Value = Encoding.UTF8.GetString(res.Value);
            return;
        }
    }

    public void MapIncomingToEnvelope(Envelope envelope, Message<string, string> incoming)
    {
        Message<string?, byte[]> rawKafkaMsg = new(){
            Headers = incoming.Headers,
            Key = incoming.Key,
            Timestamp = incoming.Timestamp,
            Value =  Encoding.UTF8.GetBytes(incoming.Value)
        };

        var ce = rawKafkaMsg.ToCloudEvent(new JsonEventFormatter());

        envelope.MessageType = typeof(CloudEvent).FullName;
        envelope.Message = ce;
    }
}