using Microsoft.EntityFrameworkCore;
using shared.Entities;

namespace shared.Persistence;

public class ISMOPDatabaseContext(DbContextOptions<ISMOPDatabaseContext> options) : DbContext(options)
{
    public DbSet<WeatherEntity> Weather { get; set; }
    public DbSet<PumpControlEntity> PumpControl { get; set; }
    public DbSet<EmbankmentEntity> Embankment { get; set; }
}

// np. pogoda
// Fri Mar 17 2017 21:51:03
// Sun Jun 18 2017 10:31:54