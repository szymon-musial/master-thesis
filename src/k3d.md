```sh
vol=$HOME/k3d/
mkdir -p $vol
k3d cluster create mycluster -p "30000-31000:30000-31000@server:0" --volume $vol:/var/lib/rancher/k3s/storage@all
```

### dash

```sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.7.0/aio/deploy/recommended.yaml
kubectl apply -f ./kubernetes-dashboard/service-account.yaml
kubectl create token admin-user -n kubernetes-dashboard
```

### error

iptables: No chain/target/match error

```sh
systemctl restart docker
```