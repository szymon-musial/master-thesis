using CloudEvents;
using CloudNative.CloudEvents;
using CloudNative.CloudEvents.SystemTextJson;
using Configuration;
using Microsoft.AspNetCore.HttpLogging;
using Wolverine;
using Wolverine.Kafka;

var builder = WebApplication.CreateBuilder(args);

var headersToLog = new List<string> { "Ce-Id", "Ce-Source", "Ce-Specversion", "Ce-Time", "Ce-Type", "X-B3-Parentspanid", "X-B3-Sampled", "X-B3-Spanid", "X-B3-Traceid" };

builder.Configuration
    .AddUserSecrets<Program>()
    .AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix);
var appConfiguration = builder.Configuration.Get<AppConfiguration>()
    ?? throw new ArgumentNullException(nameof(AppConfiguration));
builder.Services.AddSingleton(appConfiguration);

builder.Services.AddHttpLogging(o =>
{
    o.LoggingFields = HttpLoggingFields.All;
    o.CombineLogs = true;
    foreach (var header in headersToLog)
    {
        o.RequestHeaders.Add(header);
    }
});

builder.Services.AddControllers(opts => opts.InputFormatters.Insert(0, new CloudEventJsonInputFormatter(new JsonEventFormatter())));

builder.Host.UseWolverine((hostBuilderContext, opts) =>
    {
        var appConfiguration = hostBuilderContext.Configuration.Get<AppConfiguration>() 
            ?? throw new ArgumentNullException(nameof(AppConfiguration));

        opts.UseKafka(appConfiguration.BootstrapServers)
            .AutoProvision();

        opts.PublishMessage<CloudEvent>().ToKafkaTopics()
            .UseInterop(new MyCloudEventApacheKafkaEnvelopeMapper());
    });
   

var app = builder.Build();

app.Use(async (context, next) =>
{
    // POST request comes from kn kafka brokers are without content type
    // https://github.com/knative/eventing-contrib/issues/1206
    if(string.IsNullOrEmpty(context.Request.Headers.ContentType))
    {
        context.Request.Headers.ContentType = "application/json";
    }
    await next(context);
});

app.UseHttpLogging();

app.MapControllers();

app.Run();
