using CloudNative.CloudEvents;
using Configuration;
using Microsoft.AspNetCore.Mvc;
using shared;
using Wolverine;

namespace Controllers;

public class CloudEventsController(ILogger<CloudEventsController> logger, IMessageBus messageBus, AppConfiguration appConfiguration) : ControllerBase
{
    [HttpPost("/")]
    public async Task<AcceptedResult> CeEventEndpointAsync([FromBody] CloudEvent cloudEvent)
    {
        var target = cloudEvent.GetTarget();
        logger.LogInformation($"Recived CE {cloudEvent.Type} {cloudEvent.Id} from {cloudEvent.Source} to {target}");

        if(appConfiguration.ForceSendToKnTopic)
        {
            await messageBus.BroadcastToTopicAsync(appConfiguration.KNTopic, cloudEvent);
            return Accepted();
        }
        
        ArgumentNullException.ThrowIfNull(target);
        ArgumentOutOfRangeException.ThrowIfNotEqual(target.Segments.Count(), 3);

        if(!target.Segments[0].Contains("edge", StringComparison.OrdinalIgnoreCase))
        {
            throw new ArgumentOutOfRangeException(nameof(cloudEvent.Source.Segments));
        }

        await messageBus.BroadcastToTopicAsync(target.Segments[2], cloudEvent);
        return Accepted();
    }
}
