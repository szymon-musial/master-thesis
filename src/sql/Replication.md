PUBLICATION -> SUBSCRIPTION

### Publikacja - county

```sql
CREATE PUBLICATION szymon FOR ALL TABLES;
```

```sql
CREATE PUBLICATION tomek FOR ALL TABLES;
```


### Subskrypcja - cloud

```sql
CREATE SUBSCRIPTION szymon
    CONNECTION 'host=10.240.0.10 port=32000 user=postgres dbname=szymon password=root'
    PUBLICATION szymon;
```

```sql
CREATE SUBSCRIPTION tomek
    CONNECTION 'host=10.241.0.10 port=32000 user=postgres dbname=tomek password=root'
    PUBLICATION tomek;
```

#### jak się pojawi nowa tabela u publikanta to:

```sql
ALTER SUBSCRIPTION szymon REFRESH PUBLICATION;
```
