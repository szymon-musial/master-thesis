﻿using Microsoft.Extensions.Hosting;
using Oakton;
using Wolverine;
using Wolverine.MQTT;
using Microsoft.Extensions.Configuration;
using Configuration;
using shared.Interop;
using CloudNative.CloudEvents;
using Microsoft.Extensions.DependencyInjection;
using shared.Dto.DeviceSpecs;
using Microsoft.EntityFrameworkCore;
using shared.Persistence;


return await Host.CreateDefaultBuilder()
    .ConfigureAppConfiguration(conf =>
        conf.AddEnvironmentVariables(prefix: AppConfiguration.AppConfigurationPrefix)
        .AddUserSecrets<Program>()
        .AddCommandLine(args)
    )
    .ConfigureServices((context, services) =>
    {
        var sqliteConnString = $"Data Source= {context.Configuration.Get<AppConfiguration>()!.ISMOPPath}";
        services.AddDbContext<ISMOPDatabaseContext>(o => o.UseSqlite(sqliteConnString));
        
        services.AddKeyedTransient<IDevice, I2CDevice>(nameof(I2CDeviceSpecs));
        services.AddKeyedTransient<IDevice, ModBusDevice>(nameof(ModBusSpecs));

        // IServiceProvider
        services.AddSingleton(sp => sp);
    })
    .UseWolverine((context, opts) =>
    {
        // Connect to the MQTT broker
        opts.UseMqtt(builder =>
        {
            var mqttServer = context.Configuration.Get<AppConfiguration>()!.MqttServer;

            builder
                .WithMaxPendingMessages(3)
                .WithClientOptions(client =>
                {
                    client.WithTcpServer(mqttServer);
                });
        });
        opts.ListenToMqttTopic("cqrs/#")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

        opts.PublishMessagesToMqttTopic<CloudEvent>(c => $"raw/{c.Type}/{c.Id}")
            .UseInterop(new MyCloudEventMqttEnvelopeMapper());

    })
    //.ConfigureServices(services => services.AddHostedService<TestSender>())
    .RunOaktonCommands(args);
