using CloudNative.CloudEvents;

public interface IDevice
{
    public Task RequestHandlerAsync(string cloudEventData);
    public Task RequestHandlerAsync(CloudEvent cloudEvent);
}