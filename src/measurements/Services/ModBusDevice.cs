using System.Text.Json;
using CloudNative.CloudEvents;
using Configuration;
using Microsoft.Extensions.Logging;
using shared.Dto;
using shared.Dto.DeviceSpecs;
using shared.Persistence;
using Wolverine;

public class ModBusDevice(IMessageBus messageBus, ISMOPDatabaseContext iSMOPDatabaseContext, AppConfiguration appConfiguration, ILogger<ModBusDevice> logger)
    : Device(messageBus, iSMOPDatabaseContext, appConfiguration, logger), IDevice

{
   public async Task RequestHandlerAsync(string cloudEventData)
   {
      var policy = JsonSerializer.Deserialize<NewPolicy<ModBusSpecs>>(cloudEventData);
      await RunReadings(policy.Dev, policy.MeasurementType).Invoke();
   }
   public Task RequestHandlerAsync(CloudEvent cloudEvent) => RequestHandlerAsync(cloudEvent.Data.ToString());
}