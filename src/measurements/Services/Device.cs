using CloudNative.CloudEvents;
using Configuration;
using shared.Entities;
using Microsoft.Extensions.Logging;
using shared.Dto.DeviceSpecs;
using Wolverine;
using shared.Persistence;
using System.Diagnostics;
using System.Text.RegularExpressions;

public class Device(IMessageBus messageBus, ISMOPDatabaseContext databaseContext, AppConfiguration appConfiguration, ILogger logger)
{
    public async Task SendMeasurementAsync<TEntity>(IEnumerable<TEntity> entities, string uriSource = "urn:measurements") where TEntity : Entity
    {
        Stopwatch stopwatch= Stopwatch.StartNew();
        await Parallel.ForEachAsync(
            entities,
            new ParallelOptions()
            {
                MaxDegreeOfParallelism = 1 // XDDD
            },
            async (entity, token) =>
        {
            await messageBus.PublishAsync(new CloudEvent()
            {
                Id = Guid.NewGuid().ToString(),
                Source = new Uri(uriSource),
                Type = typeof(TEntity).Name,
                Data = entity,
                Time = entity.Timestamp
            });
        });
        stopwatch.Stop();
        if (stopwatch.Elapsed > TimeSpan.FromSeconds(1))
        {
            logger.LogWarning($"Sending Time {stopwatch.Elapsed} on {typeof(TEntity).Name}");
        }
    }

    public Func<Task> RunReadings<TDeviceSpecs>(TDeviceSpecs specs, string measurementType) where TDeviceSpecs : IDeviceSpecs
    => measurementType.ToLower() switch
    {
        "weather" => () => WeatherReadings(specs),
        "pump" => () => PumpReadings(specs),
        "embankment" => () => EmbankmentReadings(specs),
        _ => throw new ArgumentOutOfRangeException(nameof(measurementType), $"Not expected measurementType value: {measurementType}"),
    };

    public IEnumerable<TEntity> GetLastEntities<TEntity>() where TEntity : Entity
        => GetLastEntities(databaseContext.Set<TEntity>());

    public IEnumerable<Entity> GetLastEmbankmentEntities<EmbankmentEntity>()
        => GetLastEntities(
            databaseContext.Embankment
                .Where(i => Regex.IsMatch(i.SensorID, appConfiguration.SensorIdRegex))
            );

    public IEnumerable<TEntity> GetLastEntities<TEntity>(IQueryable<TEntity> startingEntities) where TEntity : Entity
    {
        var unixSec = appConfiguration.BackedTime.ToUnixTimeSeconds();

        var lastTimeRecord = startingEntities
                .Select(i => i.Time)
                .Where(x => x <= unixSec)
                .OrderByDescending(x => x)
                .First();

        logger.LogInformation($"Time {appConfiguration.BackedTime} unix: {unixSec}. Selecting {DateTimeOffset.FromUnixTimeSeconds(lastTimeRecord)} unix {lastTimeRecord}");

        return startingEntities
                .Where(x => x.Time == lastTimeRecord);
    }

    private async Task EmbankmentReadings<TDeviceSpecs>(TDeviceSpecs specs) where TDeviceSpecs : IDeviceSpecs
    {
        await SendMeasurementAsync(GetLastEmbankmentEntities<EmbankmentEntity>(), $"urn:measurements:{specs.DevType}");
    }
    public async Task WeatherReadings<TDeviceSpecs>(TDeviceSpecs specs) where TDeviceSpecs : IDeviceSpecs
    {
        await SendMeasurementAsync(GetLastEntities<WeatherEntity>(), $"urn:measurements:{specs.DevType}");
    }
    public async Task PumpReadings<TDeviceSpecs>(TDeviceSpecs specs) where TDeviceSpecs : IDeviceSpecs
    {
        await SendMeasurementAsync(GetLastEntities<PumpControlEntity>(), $"urn:measurements:{specs.DevType}");
    }
}