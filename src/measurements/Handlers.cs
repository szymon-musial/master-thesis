using CloudNative.CloudEvents;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using shared;
using shared.Dto;
using Wolverine;

public class Handlers(Logger<Handlers> logger, IServiceProvider serviceProvider) : IWolverineHandler
{
    public async Task HandleAsync(CloudEvent cloudEvent)
    {
        logger.LogInformation($"Incoming CE {cloudEvent.Type} {cloudEvent.Id}");
        if(!cloudEvent.Type?.Contains(NewPolicy.Key, StringComparison.OrdinalIgnoreCase) ?? true)
        {
            // Only NewPolicy
            return;
        }
        var types = cloudEvent.Type!.Split(Const.CETypeDivider);

        var deviceService = serviceProvider.GetRequiredKeyedService<IDevice>(types[1]);
        await deviceService.RequestHandlerAsync(cloudEvent);
    }
}